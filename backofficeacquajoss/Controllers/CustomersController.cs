﻿using BackOfficeAcquaJoss.Models;
using Core;
using Core.Db;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class CustomersController : Controller
    {
        private CustomersModel CustomersModel
        {
            get
            {
                CustomersModel model = new CustomersModel();
                model.Customers = null; //Core.Customers.GetClients();
                model.Schools = null; //Core.Customers.GetSchools();
                return model;
            }
        }

        // GET: Customers
        public ActionResult Clients()
        {
            return View(new CustomersModel());
        }

        public ActionResult Schools()
        {
            return View(new CustomersModel());
        }


        #region Telemarketing
        [Route("customers/schools/telemarketing/{data?}")]
        public ActionResult Telemarketing(FormCollection data = null)
        {
            TelemarketingModel model = new TelemarketingModel();
            if (data != null && data.Count > 0)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                foreach (var key in data.AllKeys)
                {
                    var value = data[key];
                    if (value != null && !string.IsNullOrEmpty(value))
                    {
                        parameters.Add(new SqlParameter(key, value));
                    }
                }
                model = new TelemarketingModel(parameters);

            }
            model.Agents = Users.Instance().GetUsersByIdType(UserType.OPERATOR).OrderBy(i => i.User_FullName).ToList();
            model.Schools = Customers.GetSchools();
            return View(model);
        }

        [HttpPost]
        [Route("customers/schools/telemarketing/InsertOrUpdate")]
        public JsonResult InsertOrUpdate(List<TelemarketingsXML> telemarketings)
        {
            List<Telemarketing> result = TelemarketingsXML.InsertOrUpdate(telemarketings);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}