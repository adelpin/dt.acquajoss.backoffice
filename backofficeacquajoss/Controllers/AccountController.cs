﻿using Core;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace BackOfficeAcquaJoss.Controllers
{
    public class AccountController : Controller
    {
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login login)
        {
            if (ModelState.IsValid)
            {
                Users UserProfile = Users.Instance().Loguearse(login.Login_Email, login.Login_Password);
                if (UserProfile != null)
                {
                    FormsAuthenticationTicket formAuthTicket = null;

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(UserProfile);

                    formAuthTicket = new FormsAuthenticationTicket(1, UserProfile.User_Email, DateTime.Now, DateTime.Now.AddMinutes(30), false, userData);
                    string encformAuthTicket = FormsAuthentication.Encrypt(formAuthTicket);
                    HttpCookie formAuthCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encformAuthTicket);
                    Response.Cookies.Add(formAuthCookie);
                    
                    Session["logged"] = true;
                    var urlBack = HomeController.UrlBack;
                    if (urlBack != null)
                    {
                        if (!string.IsNullOrEmpty(urlBack))
                        {
                            HomeController.UrlBack = null;
                            return Redirect(urlBack);
                        }
                    }
                }
                else
                {
                    Session.Remove("logged");
                    ModelState.AddModelError("error_login", "Incorrect email or password");
                }
            }
            return RedirectToAction("Index", "Home");
        }
        
        public ActionResult Logout()
        {
            HomeController.UrlBack = null;
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}