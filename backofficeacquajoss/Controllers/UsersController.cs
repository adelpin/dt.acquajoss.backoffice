﻿using BackOfficeAcquaJoss.Models;
using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class UsersController : Controller
    {
        public ActionResult Index(bool Show = false)
        {


            ViewBag.HotelList = GetUserHotelList();

            ViewBag.Option = "insert";
            ViewBag.ShowInfo = Show;



            return View(new UsersModel());
        }

        private List<SelectListItem> GetUserHotelList()
        {
            List<HotelTm> HotelList = Repository.GetHotels();
            List<SelectListItem> Lista = new List<SelectListItem>();
            Lista = HotelList.Select(i => new SelectListItem() { Text = i.Description, Value = i.Id }).ToList();
            Lista.Insert(0, (new SelectListItem { Text = "", Value = "0" }));

            return Lista;
        }

        public ActionResult Update(string id)
        {
            ViewBag.Option = "update";
            UsersModel model = UsersModel.Instance();

            if (!string.IsNullOrEmpty(id))
            {
                List<Users> Users = model.Users.Where(x => x.User_Id == Convert.ToInt32(id)).ToList();
                model.Users = Users;

                Users User = Users.First();

                if (User.User_Type == "hoteloperator")
                {
                    //get the hotel associated
                    string HotelId = Repository.GetHotelUser(User.User_Id);
                    model.UserAndLogin.HotelUserId = HotelId;
                    ViewBag.HotelUser = HotelId;
                }
            }

            ViewBag.HotelList = GetUserHotelList();

            model.UserAndLogin.User = string.IsNullOrEmpty(id) ? Global.UserProfile : Users.Instance().GetUser(int.Parse(id));
            ViewBag.ShowInfo = true;
            return View("~/Views/Users/Index.cshtml", model);
        }

        [HttpPost]
        public ActionResult InsertOrUpdate(UserAndLogin userAndLogin)
        {
            string option = userAndLogin.User.User_Id == 0 ? "insert" : "update";
            userAndLogin.Login.Login_Type = "backoffice";

            int User_Id = userAndLogin.User.User_Id;
            string HotelUserId = userAndLogin.HotelUserId;

            int res = Users.Instance().InsertOrUpdate(userAndLogin);

            //update change id:
            if (res == 0)
            {
                res = Repository.GetLastUserId();
            }

            if (userAndLogin.User.User_Id == 0)
            {
                userAndLogin.User.User_Id = res;
                User_Id = res;
            }

            //hoteluser
            if (!string.IsNullOrEmpty(userAndLogin.HotelUserId) && userAndLogin.HotelUserId != "0")
            {
                Repository.InsUpdateHotelUser(User_Id, HotelUserId);

            }

            TempData["Action"] = option;
            TempData["Status"] = (res != 0);

            UsersModel model = new UsersModel();
            model.UserAndLogin.User = userAndLogin.User;
            model.UserAndLogin.Login = userAndLogin.Login;

            model.UserAndLogin.User.User_CreationDate = DateTime.Now;
            ViewBag.ShowInfo = true;
            return View("~/Views/Users/Index.cshtml", model);

            //return RedirectToAction("Update", new { id = userAndLogin.User.User_Id });
        }

        public ActionResult Data(string id)
        {
            ViewBag.Option = "data";
            UsersModel model = UsersModel.Instance();

            if (!string.IsNullOrEmpty(id))
            {
                model.Users = model.Users.Where(x => x.User_Id == Convert.ToInt32(id)).ToList();
            }

            model.UserAndLogin.User = string.IsNullOrEmpty(id) ? Global.UserProfile : Users.Instance().GetUser(int.Parse(id));
            ViewBag.ShowInfo = true;
            return View("~/Views/Users/Index.cshtml", model);
        }

        [HttpPost]
        public int ChangeStatus(int user_id, bool user_status)
        {
            return Users.Instance().ChangeStatus(user_id, user_status);
        }
    }
}