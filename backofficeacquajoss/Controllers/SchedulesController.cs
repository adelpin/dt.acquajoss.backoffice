﻿using BackOfficeAcquaJoss.Models;
using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class SchedulesController : Controller
    {
        // GET: Schedules
        public ActionResult Index()
        {
            SchedulesModel m = new SchedulesModel();
            m.AllProducts = Products.Instance(Global.Lang).GetProducts(Global.Lang);
            return View(m);
        }
    }
}