﻿using BackOfficeAcquaJoss.Models;
using Core;
using Core.Db;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Web.Mvc;
using System.Xml;

namespace BackOfficeAcquaJoss.Controllers
{
    public class ReservationsController : Controller
    {
        
        // GET: Reservations
        public ActionResult New()
        {
            BackOfficeAcquaJoss.Models.ReservationsModel Res = ReservationsModel.Instance();

            return View(Res);
        }

        public ActionResult Schools()
        {
            return View(new ReservationsModel());
        }

        public ActionResult Groups()
        {
            return View(new ReservationsModel());
        }

        public ActionResult Cancel()
        {
            return View();
        }

        public ActionResult EmitInvoice(string ReservationId)
        {

            //common with aquafelix
            Repository.BackOfficeInvoiceEmission(ReservationId);

            return Json("");
        }


        public void InvoiceEmission(string ReservationId, bool Folder = false)
        {
            //common with aquafelix
            Repository.BackOfficeInvoiceEmission(ReservationId);
        }


        public ActionResult EmitAll()
        {
           
            for (int i = 1; i <= 10; i++)
            {
                string block = "000000" + i.ToString();
                block = block.Substring(block.Length - 6, 6);

                string reservationId = "";

                //o0 entrambe 1fatt 2autofatt
                string choice = "0";
                reservationId = Repository.GetReservationIdByInvoice("W-19-" + block);


                if (choice == "1" || choice == "0")
                {
                    if (reservationId != "")
                    {
                        InvoiceEmission(reservationId, true);
                    }
                }

               
            }

            return Json("");
        }



        public void InvoiceEmission(string ReservationId)
        {

            BankPayment BankPay = Repository.GetBankPayment(Convert.ToInt32(ReservationId));
            string Payment = BankPay.PaymentType.ToString();

            Shops Shop = Impostazioni.GetAcquistoOp(ReservationId);
            string pp = "2";
            if (Payment == "1") { pp = "BB"; }
            if (Payment == "2") { pp = "CC"; }
            Repository.SendElectronicInvoice(Shop, Repository.GetBuyer(ReservationId), ReservationId, pp);
            
        }

        public ActionResult ChangeEmission(string ReservationId, string State, string Payment)
        {
           
            string OldStatus = Repository.GetReservationState(ReservationId);

            SqlConnection Conn = new SqlConnection();
            Conn.ConnectionString = Impostazioni.Conexion;
            Conn.Open();
            Repository.ChangeReservationStatus(ReservationId, State, Conn);
            Log.Write("Backoffice Reservation: " + ReservationId + " cambiato da: " + OldStatus + " a: " + State);
            string StatusId = Repository.GetPaymentStateId(State);

            string Msg = "Backoffice: " + State;

            Repository.UpdatePaymentsState(StatusId, ReservationId, Msg);
            Repository.ChangeReservationPayment(ReservationId, Payment, Conn);

            ///////////////////////
            //delete Zoho crm:
            if (State != "OK")
            {
                Repository.DeleteZoHoCrmSale(ReservationId);
            }

            Shops Shop = Impostazioni.GetAcquistoOp(ReservationId);



            if (Shop.ShoppingStateHotel != null)
            {
                //if parco + hotel update the hotel allotments
                BackOfficeChangeHotelAllotment(ReservationId, OldStatus, State, Shop.ShoppingStateHotel);
            }

            //invoice
            if (Payment == "1" && State == "OK")
            {
                //bank transfer emit invoice if ok
                InfoBuyer Buyer = Repository.GetBuyer(ReservationId);

                if (Buyer.Invoice == "Y")
                {
                    //check if still emitted
                    string Invoice = Repository.GetInvoice(ReservationId);

                    if (Invoice == "")
                    {
                        Repository.SendElectronicInvoice(Shop, Buyer, ReservationId, "BB");
                    }
                }
            }


            Conn.Close();

            return Json("");
        }


        public void BackOfficeChangeHotelAllotment(string ReservationId, string OldStatus, string NewStatus, ShoppingStateHotel ShoppingStateHotel)
        {
            
            SqlConnection Conn = new SqlConnection(Impostazioni.Conexion);
            Conn.Open();

            SqlTransaction Trans = Conn.BeginTransaction();

            //update hotel allotment
            try
            {
                Log.Write("Backoffice Reservation id: " + ReservationId + " cambio stato hotel allotment da: " + OldStatus + " a:" + NewStatus);

                for (int i = 0; i < ShoppingStateHotel.Nrooms; i++)
                {
                    //Repository.BackofficeChangeHotelAllotments(OldStatus, NewStatus, ShoppingStateHotel, i, Conn, Trans);
                }

                Trans.Commit();
                Repository.CloseTransaction(Conn, Trans);
           
            }
            catch (Exception ex)
            {
                Log.Write("Backoffice BackofficeChangeHotelAllotments " + ex.Message);

                Trans.Rollback();
                Repository.CloseTransaction(Conn, Trans);
            }
        }

        public ActionResult Summary(int id)
        {
            ReservationModel model = new ReservationModel(id);
            BankPayment Pagamenti = Repository.GetBankPayment(id);
            ViewBag.TipoPag = Repository.DecodePaymentType(Pagamenti.PaymentType);
            ViewBag.Pagamenti = Pagamenti;
            ViewBag.PagId = Pagamenti.PaymentType;
            ViewBag.TicketsEmitted = Repository.TicketsEmitted(id);
            ViewBag.PaymentState = Repository.GetPaymentState();
            ViewBag.PaymentType = Repository.GetPaymentType();

            Session["BackOfficeDay"] = Impostazioni.GetReservationDay(id.ToString());

            return View(model);
        }

        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult CommentInsert(ReservationLogs commnet)
        {
            if (_InsertOrUpdate(commnet))
            {
                return Json("OK");
            }
            return Json("KO");
        }

        private Boolean _InsertOrUpdate(ReservationLogs comment)
        {
            string option = comment.Id == 0 ? "insert" : "update";
            
            int res = ReservationLogs.Instance().InsertOrUpdate(comment);
            if (comment.Id == 0) comment.Id = res;

            return (res != 0);
        }

        //public ActionResult Checkout(int id)
        //{
        //    ReservationModel model = new ReservationModel(id);
        //    BankPayment Pagamenti = Repository.GetBankPayment(id);
        //    ViewBag.Pagamenti = Pagamenti;


        //    string BankShopId = Impostazioni.CreaBankTransId();
        //    InfoBuyer Buyer = new InfoBuyer() {
        //        Name = model.reservation.Reservation_Customer.Customer_Name,
        //        Surname = model.reservation.Reservation_Customer.Customer_LastName,
        //        Email = model.reservation.Reservation_Customer.Customer_Email,                
        //    };
        //    string TotPrice = model.reservation.Reservation_Total.ToString();
        //    string BuyerName = model.reservation.Reservation_Customer.Customer_FullName;
        //    InfoAcquisto InfoA = Impostazioni.GetAcquisto(Buyer, TotPrice, "");
        //    InfoBancaSella InfoBs = BancaSellaPayment(InfoA, BankShopId, TotPrice, BuyerName, Buyer.Email);

        //    return View(model);
        //}

        public ActionResult Resend(string ReservationId)
        {
           
            Shops Shop = Impostazioni.GetAcquistoOp(ReservationId);
            InfoBuyer Buyer = new InfoBuyer();


            TmTransazioni Tm = Repository.GetTmasterOperations(ReservationId);

            int TipoAcquisto = 0;

            if (Shop.ShoppingState.DayNow != "")
            {
                TipoAcquisto = 1;
                Buyer = Shop.ShoppingState.Buyer;
                Tm.Totale = Shop.ShoppingState.Price;
            }

            if (Shop.ShoppingStateHotel.DayArrive != "")
            {
                TipoAcquisto = 2;
                Buyer = Shop.ShoppingStateHotel.Buyer;
                Tm.Totale = Shop.ShoppingStateHotel.Price;
            }

            if (Shop.ShoppingStateSubscriptions.TotPrice != "0")
            {
                TipoAcquisto = 3;
                Buyer = Shop.ShoppingStateSubscriptions.Buyer;
                Tm.Totale = Shop.ShoppingStateSubscriptions.TotPrice;
            }

            if (TipoAcquisto == 0)
            {
                Log.Write("Errore acquisto " + ReservationId + " backoffice Resend: tipo acquisto non individuato");
                return Json("Errore");
            }

            string InfoP = "Reinvio biglietti acquisto: " + ReservationId + " sig.: " + Buyer.Name + " " + Buyer.Surname + " email: " + Buyer.Email;
            InfoP += " Totale: " + Tm.Totale;

            Log.Write(InfoP);
          
            Purchase Acquisto = new Purchase();
            Acquisto.Purchase_Id = ReservationId;

            //right way to get barcode emitted before dd/mm/yyyy hh:mm:ss
            Reservations Res = Repository.GetReservation(ReservationId);
            DateTime newDt = (DateTime)Res.Reservation_CreationDate;
            string Barcode = TMasterClass.CreaBarcode(ReservationId);

            Acquisto.Purchase_date = newDt;
            Acquisto.Purchase_Type_Id = TipoAcquisto;

            string Ret = Mail.CreazioneEdInvioMailCliente(Acquisto, Tm, Buyer, Barcode, true);

            return Json("");
        
        }

        public ActionResult Emission(string ReservationId, int Payment)
        {
            string Esito = Repository.BoTicketEmission(ReservationId, Payment, Global.UserProfile);

            return Json(Esito);
        }



        public JsonResult VerifyEmission(string ReservationId)
        {
            int Ntck = Repository.TicketsEmitted(Convert.ToInt32(ReservationId));

            if (Ntck > 0)
            {
                Log.Write("Backoffice VerifyEmission biglietti già emessi per ReservationId: " + ReservationId);
                return Json("KO");
            }

            return Json("OK");
        }


        public JsonResult TestTmaster()
        {          
            //tmaster
            string UrlT = Impostazioni.TmEndPoint;
            if (WsGenerici.TestConnection(UrlT, false) == false)
            {
                Log.Write("Test connessione Tmaster fallito");
                return Json("KO");
            }

            return Json("OK");
        }



        public ActionResult Search(FormCollection data = null)
        {
          
            SearcReservationsModel model = new SearcReservationsModel();
            if (data != null && data.Count > 0)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                foreach (var key in data.AllKeys)
                {
                    var value = data[key];
                    if (value != null && !string.IsNullOrEmpty(value)) {

                        if (key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin")
                        {
                            value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                        }
                        parameters.Add(new SqlParameter(key, value));
                    }
                }
                model = new SearcReservationsModel(parameters);
                
            }

            ViewBag.PaymentList = Repository.GetPaymentState();

            ViewBag.Invoice = "";
            if (Request.Form["Invoice"] != null)
            {
                string ss = Request.Form["Invoice"].ToString();
                if (ss != "")
                { ViewBag.Invoice = Convert.ToInt16(ss); }
            }


            return View(model);
        }

        
        [HttpPost]
        public string GetHotels(int product_idType)
        {
            return CRUD.ToSerializeObject(Products.Instance(Global.Lang).GetProducts(Global.Lang));
        }

        [HttpPost]
        public string GetHotel(int hotel_idProduct)
        {
            return CRUD.ToSerializeObject(Hotels.Instance().GetHotel(hotel_idProduct));
        }

        public ActionResult DownloadInvoice(string ReservationId)
        {

            string Title = "";
            string InvoiceXml = Repository.BackOfficeInvoiceDownload(ReservationId, ref Title);
            MemoryStream stream = Repository.DownloadInvoice(InvoiceXml);
            return File(stream, "text/xml", Title);

        }

        public ActionResult DownloadFile(string ReservationId)
        {
            string DownloadFileName = "";
            MemoryStream stream = Repository.DownloadFile(ReservationId, ref DownloadFileName);
            return File(stream, "application/pdf", DownloadFileName);

        }

        //private InfoBancaSella BancaSellaPayment(InfoAcquisto InfoA, string BankShopId, string TotPrice, string BuyerName, string Email)
        //{
        //    //true bancasella data:         
        //    string BsShopLogin = Impostazioni.BsShopLogin;
        //    string AuthUrl = Impostazioni.BsUrl;

        //    InfoBancaSella InfoBS = Impostazioni.GetInfoBancaSella(InfoA, BsShopLogin, BankShopId, AuthUrl);


        //    string Webservice = "on";
        //    string crypt = "";

        //    if (Webservice == "on")
        //    {
        //        //webrequest
        //        string Ws = Banks.BancaSellaRequest(BsShopLogin, TotPrice, BankShopId, BuyerName, Email);
        //        crypt = Banks.BancasSellaReadWs(Ws);
        //    }
        //    else
        //    {
        //        //api
        //        //crypt = Banche.PagaBancaSella(InfoBS);
        //    }

        //    InfoBS.XMLOUT = crypt;

        //    XmlDocument XmlReturn = new XmlDocument();

        //    XmlReturn.LoadXml(InfoBS.XMLOUT);
        //    XmlNode ThisNode = XmlReturn.SelectSingleNode("//GestPayCryptDecrypt/ErrorCode");
        //    InfoBS.ErrorCode = ThisNode.InnerText;
        //    if (InfoBS.ErrorCode == "0")
        //    {
        //        XmlNode ThisNode2 = XmlReturn.SelectSingleNode("//GestPayCryptDecrypt/CryptDecryptString");
        //        InfoBS.EncryptedString = ThisNode2.InnerText;
        //    }
        //    else
        //    {
        //        //Put error handle code HERE
        //        ThisNode = XmlReturn.SelectSingleNode("//GestPayCryptDecrypt/ErrorDescription");
        //        InfoBS.ErrorDescription = ThisNode.InnerText;
        //        InfoBS.ErrorClass = "on";
        //    }

        //    //no iframe
        //    //var percorso = AuthUrl + "?a=" + ShopLogin + "&b=" + crypt;
        //    //return Redirect(percorso);

        //    //iframe:
        //    ViewBag.BsIframeJs = Impostazioni.BsIframeJs;
        //    return InfoBS;
        //}

    }

}