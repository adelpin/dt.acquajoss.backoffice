﻿using Core;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class HomeController : Controller
    {
        public static string UrlBack { get; set; }

        // GET: Home
        public ActionResult Index()
        {
            Impostazioni.SiteSettings = Repository.GetSiteSettings();
            Impostazioni.ManageDateSettings();

            //automatic reset for hotel allotment pending
            Repository.UpdateHotelPending();

            return View();
        }
    }
}