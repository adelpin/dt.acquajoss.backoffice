﻿using BackOfficeAcquaJoss.Models;
using Core;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Core.Db;

using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using EventsModel = BackOfficeAcquaJoss.Models.EventsModel;
using FaqsModel = BackOfficeAcquaJoss.Models.FaqsModel;
using ActivitiesModel = BackOfficeAcquaJoss.Models.ActivitiesModel;
using PartnersModel = BackOfficeAcquaJoss.Models.PartnersModel;
using NewsModel = BackOfficeAcquaJoss.Models.NewsModel;
using ConvenzioniModel = BackOfficeAcquaJoss.Models.ConvenzioniModel;

namespace BackOfficeAcquaJoss.Controllers
{
    public class AdminController : Controller
    {
        #region Reports
        public ActionResult Reports(FormCollection data = null)
        {
            SearcReservationsModel model = new SearcReservationsModel();
            if (data != null && data.Count > 0)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                foreach (var key in data.AllKeys)
                {
                    var value = data[key];
                    if (value != null && !string.IsNullOrEmpty(value))
                    {

                        if (key == "Reservation_Ini" || key == "Reservation_Fin")
                        {
                            value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                        }
                        parameters.Add(new SqlParameter(key, value));
                    }
                }
                model = new SearcReservationsModel(parameters);
           
            }




            ////////////////////////////////////////////////////////
            //reports
            List<Reservations> customers = new List<Reservations>();

            Reservations res = new Reservations();
            res.Reservation_Id = 1234;
            res.Reservation_Status = "1";
            res.Reservation_CreationDate = DateTime.Now;

            customers.Add(res);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            //reportViewer.Width = Unit.Percentage(900);
            //reportViewer.Height = Unit.Percentage(900);

            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\AnalisiEconomica.rdlc";

            ReportDataSource RDS = new ReportDataSource("DataSet1", customers);

            reportViewer.LocalReport.DataSources.Add(RDS);
            reportViewer.LocalReport.Refresh();

            ViewBag.ReportViewer = reportViewer;

            return View(model);
        }

        #endregion Reports

        #region SiteSettings
        public ActionResult Settings()
        {
            ExtendedSettings Model = Repository.GetAllSettings();

            return View(Model);
        }

        public JsonResult UpdateSiteSettings(SiteSettings Info1, BasePriceSettings Info3 , DateSettings Info4)
        {
            Repository.UpdateSiteSettings(Info1);
            Repository.UpdateBasePriceSettings(Info3);
            Repository.UpdateBaseDateSettings(Info4);
            return Json("OK");
        }
        #endregion 

        #region Checkbank
        public ActionResult CheckBank()
        {
            int DayBefore = 0;

            if (!string.IsNullOrEmpty(Impostazioni.BsCheckDaysBefore))
            {
                DayBefore = Convert.ToInt32(Impostazioni.BsCheckDaysBefore);
            }

            List<string> ResFinal = Repository.GetReservationsKoList(Impostazioni.BsShopLogin, Impostazioni.BsCheckEndPoint, DayBefore);

            ViewBag.Elenco = ResFinal;
            return View();
        }
        #endregion

        #region Activities




        public ActionResult Activities()
        {
            return View(new ActivitiesModel());
        }

        public ActionResult InsertOrUpdateCategory(Categories caregory)
        {
            TempData["Action"] = caregory.Category_Id == 0 ? "insert" : "update";
            TempData["Status"] = Categories.Instance().InsertOrUpdate(caregory) != 0;
            return RedirectToAction("Activities");
        }

        public void InsertOrUpadteActivity(Core.Activities activity)
        {
            bool success = false;
            bool newActivity = activity.Activity_Id == 0;
            try
            {
                string folderGallery = "/gallery";
                string rootPath = "Content/images/activities/" + (activity.Activity_Title.ToLower().Replace(" ", "-"));
                int[] categories = Request.Form["Activity_CategoriesNew"].Split(',').Select(int.Parse).ToArray();
                List<string> gallery = string.IsNullOrEmpty(Request.Form["Activity_GalleryNew"]) ? new List<string>() : Request.Form["Activity_GalleryNew"].Split(',').ToList();

                List<HttpPostedFileBase> galleryList = new List<HttpPostedFileBase>();
                if (Request.Files.Count > 0)
                {
                    foreach (string fileKey in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[fileKey];
                        string pathFile = fileKey.Contains("Activity_Gallery") ? folderGallery : string.Empty;
                        if (fileKey.Contains("Activity_Gallery")) galleryList.Add(file);
                        else activity.GetType().GetProperty(fileKey).SetValue(activity, rootPath + "/" + file.FileName, null);

                        success = (new BackOfficeAcquaJoss.Models.FileUpload().Upload(file, new Files()
                        {
                            Name = file.FileName,
                            Size = file.ContentLength,
                            Folder = rootPath + pathFile,
                        }) == 1);
                    }
                }
                int response = Core.Activities.Instance().InsertOrUpdate(activity);
                success = (response != 0);
                activity.Activity_Id = activity.Activity_Id == 0 ? response : activity.Activity_Id;

                foreach (int Category_Id in categories)
                {
                    if (!activity.Activity_Categories.Contains(Category_Id))
                    {
                        ActivityCategory.Instance().Insert(new ActivityCategory()
                        {
                            ActivityCategory_IdActivity = activity.Activity_Id,
                            ActivityCategory_IdCategory = Category_Id
                        });
                    }
                }

                foreach (int Category_Id in activity.Activity_Categories)
                {
                    if (!categories.Contains(Category_Id))
                    {
                        ActivityCategory.Instance().Delete(new ActivityCategory()
                        {
                            ActivityCategory_IdActivity = activity.Activity_Id,
                            ActivityCategory_IdCategory = Category_Id
                        });
                    }
                }

                foreach (HttpPostedFileBase file in galleryList)
                {
                    string image = rootPath + folderGallery + "/" + file.FileName;
                    ActivityGallery.Instance().InsertOrUpdate(new ActivityGallery()
                    {
                        ActivityGallery_IdActivity = activity.Activity_Id,
                        ActivityGallery_Image = image,
                        ActivityGallery_Index = activity.Activity_Gallery.Count + 1,
                    });
                    gallery.Add(image);
                }

                foreach (string fileGallery in activity.Activity_Gallery.Select(a => a.ActivityGallery_Image))
                {
                    if (!gallery.Contains(fileGallery))
                    {
                        ActivityGallery activityGallery = activity.Activity_Gallery.Find(a => a.ActivityGallery_Image.Equals(fileGallery));
                        ActivityGallery.Instance().Delete(activityGallery.ActivityGallery_Id);
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
                Console.WriteLine(ex.Message);
            }
            TempData["Action"] = newActivity ? "insert" : "update";
            TempData["Status"] = success;
        }

        /***/
        public ActionResult ActivitySchedule(string id = null)
        {
            Core.ActivitySchedule s = new ActivitySchedule();
            ActivityScheduleModel m = new ActivityScheduleModel
            {
                ActivitySchedule = string.IsNullOrEmpty(id) ? s : s.GetActivitySchedule(int.Parse(id))
            };
            return View(m);
        }

        [HttpPost]
        public ActionResult ActivityScheduleInsertOrUpdate(ActivitySchedule activitySchedule)
        {
            string option = activitySchedule.Id == 0 ? "insert" : "update";
            int res = Core.ActivitySchedule.Instance().InsertOrUpdate(activitySchedule);
            if (activitySchedule.Id == 0) activitySchedule.Id = res;
            TempData["Action"] = option;
            TempData["Status"] = (res != 0);
            return RedirectToAction("ActivitySchedule", new { id = activitySchedule.Id });
        }

        [HttpPost]
        public int ChangeStatusActivitySchedule(int Id, bool Status)
        {
            return Core.ActivitySchedule.Instance().ChangeStatus(Id, Status);
        }

        [HttpPost]
        public JsonResult InsertUpdateActivitiesCalendarSchedule(Core.ActivitiesCalendarSchedule activityCalendar)
        {


            int response = ActivitiesCalendarSchedule.Instance().InsertOrUpdate(activityCalendar);

            if (activityCalendar.ActivitiesCalendarSchedule_id == 0)
            {
                activityCalendar.ActivitiesCalendarSchedule_id = response;
            }
            return Json(activityCalendar, JsonRequestBehavior.AllowGet);


        }
        #endregion Activities

        #region Partners
        public ActionResult Partners()
        {
            return View(new PartnersModel());
        }

        public ActionResult InsertOrUpadtePartner(Partners partner) {
            bool success = false;
            if (Request.Files.Count > 0)
            {
                string folderPath = "Content/images/partners";
                HttpPostedFileBase file = Request.Files[0];
                success = (new BackOfficeAcquaJoss.Models.FileUpload().Upload(file, new Files()
                {
                    Name = file.FileName,
                    Size = file.ContentLength,
                    Folder = folderPath,
                }) == 1);
                partner.Partner_Image = folderPath + "/" + file.FileName;
            }
            if (success || partner.Partner_Id != 0)
            {
                success = (Core.Partners.Instance().InsertOrUpdate(partner) != 0);
            }
            TempData["Action"] = partner.Partner_Id == 0 ? "insert" : "update";
            TempData["Status"] = success;
            return RedirectToAction("Partners");
        }

        public ActionResult DeletePartner(int id)
        {
            bool status = Core.Partners.Instance().Delete(id);
            TempData["Action"] = "delete";
            TempData["Msn"] = status ? "The partner was successfully removed" : "An unexpected error occurred, try later";
            TempData["Status"] = status;

            Partners partner = Core.Partners.Instance().GetPartner(id);
            if (partner != null)
            {
                string pathString = Path.Combine(Server.MapPath(@"\"), partner.Partner_Image);
                if (System.IO.File.Exists(pathString))
                {
                    System.IO.File.Delete(pathString);
                }
            }
            return RedirectToAction("Partners");
        }
        #endregion Partners

        #region TMASTER
        public ActionResult TMaster(int? idParkOperational = null)
        {
            if (idParkOperational == null)
            {
                //current season
                idParkOperational = Repository.GetCurrentParkOperational();
            }

            return View(new TmasterMapModel(idParkOperational));
        }

        public JsonResult TmasterDelete(string Id)
        {
            Repository.DeleteTmaster(Id);
            return Json("");
        }

        public ActionResult InsertOrUpadteTMaster(TmasterMap tmasterMap)
        {
            //decimal price correct managed:
            string Price = Request.QueryString["Price"];
            tmasterMap.Price = (decimal)Repository.ConvertiPrezzo(Price);

            TempData["Action"] = tmasterMap.Id == 0 ? "insert" : "update";
            TempData["Status"] = TmasterMap.Instance().InsertOrUpdate(tmasterMap) != 0;
            return RedirectToAction("TMaster", new { idParkOperational = tmasterMap.ParkOperational_Id });
        }

        [HttpPost]
        public int InsertOrUpadteTMasterAjax(TmasterMap tmasterMap)
        {
            return TmasterMap.Instance().InsertOrUpdate(tmasterMap);
        }
        #endregion TMARSTER

        #region TMASTERPRODUCT
        public ActionResult TMasterMapeo(int? idParkOperational = null, int? idProductSearch = null, bool Delete = false)
        {
            if (idParkOperational == null)
            {
                //current season
                idParkOperational = Repository.GetCurrentParkOperational();
            }

            TMasterMapeoModel Tm = new TMasterMapeoModel(idParkOperational);

            List<ProductsTickets> Lista = new List<ProductsTickets>();
            if (idProductSearch != null)
            {
                Lista = Tm.ProductsTickets.Where(x => x.ProductTicket_IdProduct == idProductSearch).ToList();
                //filterd by season too              
                Lista = Lista.Where(x => x.TmasterMap.ParkOperational_Id == idParkOperational).ToList();
            }

            List<Products> ListaDD = Core.Products.Instance().GetProducts();

            List<string> MainProducts = Repository.GetMainProducts();


            ListaDD = ListaDD.Where(x => MainProducts.Contains(x.Product_Type)).ToList();
            ListaDD = ListaDD.OrderBy(x => x.Product_Key).ToList();

            ViewBag.ListaDD = ListaDD;
            ViewBag.ProductTickets = Lista;

            return View(Tm);
        }




        public ActionResult InsertOrUpadteTMasterMapeo(ProductsTickets productsTicket)
        {
            string idParkOperational = HttpUtility.ParseQueryString(Request.UrlReferrer.Query).Get("idParkOperational");
            string ProductSearch = HttpUtility.ParseQueryString(Request.UrlReferrer.Query).Get("idProductSearch");

            int? idProductSearch = null;

            if (!string.IsNullOrEmpty(ProductSearch))
            {
                idProductSearch = Convert.ToInt32(ProductSearch);
            }

            TempData["Action"] = productsTicket.ProductTicket_Id == 0 ? "insert" : "update";
            TempData["Status"] = ProductsTickets.Instance().InsertOrUpdate(productsTicket) != 0;
            return RedirectToAction("TMasterMapeo", new { idParkOperational = (string.IsNullOrEmpty(idParkOperational) ? null : idParkOperational), idProductSearch });
        }

        [HttpPost]
        public int InsertOrUpadteTMasterMapeoAjax(ProductsTickets productsTicket)
        {
            return ProductsTickets.Instance().InsertOrUpdate(productsTicket);
        }

        [HttpPost]
        public int DeleteTMasterMapeoAjax(int ProductId)
        {
            return ProductsTickets.Instance().DeleteProductTicket(ProductId);
        }



        [HttpPost]
        public int CheckUniqueTMasterMapeoAjax(ProductsTickets productsTicket)
        {
            return ProductsTickets.Instance().GetProductTickets(productsTicket).ProductTicket_Id;
        }
        #endregion TMASTERPRODUCT

        #region Events
        public ActionResult Events()
        {
            return View(new EventsModel());
        }

        [ValidateInput(false)]
        public void InsertOrUpdateEvent(Core.Events evento)
        {
            bool success = false;
            bool newEvent = evento.Event_Id == 0;
            try
            {
                string folderGallery = "/gallery";
                string rootPath = "Content/images/events/"; //+ (evento.Event_Title.ToLower().Replace(" ", "-"));

                List<HttpPostedFileBase> galleryList = new List<HttpPostedFileBase>();
                if (Request.Files.Count > 0)
                {
                    foreach (string fileKey in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[fileKey];
                        string pathFile = fileKey.Contains("Event_Gallery") ? folderGallery : string.Empty;
                        if (fileKey.Contains("Event_Gallery")) galleryList.Add(file);
                        else evento.GetType().GetProperty(fileKey).SetValue(evento, rootPath + "/" + file.FileName, null);

                        success = (new BackOfficeAcquaJoss.Models.FileUpload().Upload(file, new Files()
                        {
                            Name = file.FileName,
                            Size = file.ContentLength,
                            Folder = rootPath + pathFile,
                        }) == 1);
                    }
                }
                int response = Core.Events.Instance().InsertOrUpdate(evento);
                success = (response != 0);
                evento.Event_Id = evento.Event_Id == 0 ? response : evento.Event_Id;
            }
            catch (Exception ex)
            {
                success = false;
                Console.WriteLine(ex.Message);
            }
            TempData["Action"] = newEvent ? "insert" : "update";
            TempData["Status"] = success;
        }

        public JsonResult DeleteEvent(string Id)
        {
            Repository.DeleteEvent(Id);

            return Json("");
        }

        /* EventsInstagrams */

        [HttpPost]
        public string EventInstagramAdd(EventsInstagrams eventInstagram)
        {
            return JsonConvert.SerializeObject(new { Status = EventsInstagrams.Instance().InsertOrUpdate(eventInstagram) });
        }

        [HttpPost]
        public ActionResult EventInstagramDelete(EventsInstagrams eventInstagram)
        {
            EventsInstagrams.Instance().Delete(eventInstagram.EventInstagram_Identifier);
            return null;
        }

        /* Start EventsFaqs */
        [HttpPost]
        public ActionResult GetEventsFaqsHTML(int event_Id)
        {
            EventsModel m = new EventsModel();
            m.Event = Core.Events.Instance().GetEvent(event_Id) != null ? Core.Events.Instance().GetEvent(event_Id) : new Core.Events();
            return PartialView("~/Views/Admin/EventsFaqs/EventsFaqs.cshtml", m);
        }

        [HttpPost]
        public ActionResult GetEventsFaqsJson(int event_Id)
        {
            var list = EventsFaqs.Instance().GetEventsFaqs(event_Id);
            return Json(list);
        }

        [HttpPost]
        public int ChangeStatusEventFaq(int eventfaq_id, bool eventfaq_status)
        {
            return EventsFaqs.Instance().ChangeStatus(eventfaq_id, eventfaq_status);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertOrUpdateEventFaq(EventsFaqs eventFaq)
        {

            if (_InsertOrUpdateEventFaq(eventFaq).EventFaq.EventsFaqs_Id != 0)
            {
                return Json(eventFaq);
            }
            return Json(null);
        }

        private EventsModel _InsertOrUpdateEventFaq(EventsFaqs eventFaq)
        {
            string option = eventFaq.EventsFaqs_Id == 0 ? "insert" : "update";

            EventsModel model = new EventsModel();
            model.Event = Core.Events.Instance().GetEvent(eventFaq.EventsFaqs_IdEvent);
            //int eventsFaqs_OrderMax = model.EventsFaqs.Max(f => f.EventsFaqs_Order);
            eventFaq.EventsFaqs_Order = model.Event.Event_Faqs.Count + 1;
            int res = EventsFaqs.Instance().InsertOrUpdate(eventFaq);
            if (eventFaq.EventsFaqs_Id == 0) eventFaq.EventsFaqs_Id = res;

            model.EventFaq = eventFaq;
            return model;
        }

        /* End EventsFaqs */

        //[HttpPost]
        //public string GetEventFaqs(EventsInstagrams eventInstagram)
        //{
        //    return JsonConvert.SerializeObject(new { Status = EventsInstagrams.Instance().InsertOrUpdate(eventInstagram) });
        //}
        #endregion

        #region Faqs

        private FaqsModel FaqsModel
        {
            get
            {
                FaqsModel m = new FaqsModel
                {
                    Faqs = null,
                    faqs = Faqs.Instance().GetFaqs(),
                };
                return m;
            }
        }
        public ActionResult Faq()
        {

            return View(FaqsModel);
        }

        public ActionResult FaqUpdate(string id = null)
        {
            FaqsModel m = FaqsModel;
            if (id != null)
            {
                m.Faqs = Faqs.Instance().GetFaq(int.Parse(id));
            }
            return View("~/Views/Admin/Faq.cshtml", m);
        }

        [HttpPost]
        public ActionResult GetFaqsJson(int faq_Id)
        {
            var list = Faqs.Instance().GetFaq(faq_Id);
            return Json(list);
        }

        [HttpPost]
        public int ChangeStatusFaq(int faqs_id, bool faqs_status)
        {
            return Faqs.Instance().ChangeStatus(faqs_id, faqs_status);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertOrUpdateFaq(Faqs Faq)
        {

            if (_InsertOrUpdateFaq(Faq).Faqs.Faqs_Id != 0)
            {
                return Json(Faq);
            }
            return Json(null);
        }

        private FaqsModel _InsertOrUpdateFaq(Faqs Faq)
        {
            string option = Faq.Faqs_Id == 0 ? "insert" : "update";

            FaqsModel model = new FaqsModel();
            //int eventsFaqs_OrderMax = model.EventsFaqs.Max(f => f.EventsFaqs_Order);
            Faq.Faqs_Order = Faqs.Instance().GetFaqs().Select(f => f.Faqs_Order).Max() + 1;
            int res = Faqs.Instance().InsertOrUpdate(Faq);
            if (Faq.Faqs_Id == 0) Faq.Faqs_Id = res;

            model.Faqs = Faq;
            return model;
        }


        #endregion Faqs

        #region News
        public ActionResult News()
        {
            return View(new NewsModel());
        }

        [ValidateInput(false)]
        public void InsertOrUpdateNews(Core.News news)
        {
            bool success = false;
            bool newNews = news.News_Id == 0;
            try
            {
                string folderGallery = "/gallery";
                string rootPath = "Content/images/news"; //+ (evento.Event_Title.ToLower().Replace(" ", "-"));

                List<HttpPostedFileBase> galleryList = new List<HttpPostedFileBase>();
                if (Request.Files.Count > 0)
                {
                    foreach (string fileKey in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[fileKey];
                        string pathFile = fileKey.Contains("News_Gallery") ? folderGallery : string.Empty;
                        if (fileKey.Contains("News_Gallery")) galleryList.Add(file);
                        else news.GetType().GetProperty(fileKey).SetValue(news, rootPath + "/" + file.FileName, null);

                        success = (new BackOfficeAcquaJoss.Models.FileUpload().Upload(file, new Files()
                        {
                            Name = file.FileName,
                            Size = file.ContentLength,
                            Folder = rootPath + pathFile,
                        }) == 1);
                    }
                }
                int response = Core.News.Instance().InsertOrUpdate(news);
                success = (response != 0);
                news.News_Id = news.News_Id == 0 ? response : news.News_Id;
            }
            catch (Exception ex)
            {
                success = false;
                Console.WriteLine(ex.Message);
            }
            TempData["Action"] = newNews ? "insert" : "update";
            TempData["Status"] = success;
        }
        #endregion

        #region Home
        public ActionResult HotelInfo()
        {
            HotelInfo HotelInfo = Repository.GetHotelInfo();
            return View(HotelInfo);
        }


        public JsonResult UpdateHotelInfo(string Id, string Description)
        {
            Repository.UpdateHotelInfo(Id, Description);

            return Json("");
        }



        public ActionResult Home(string id = "1") {

            AquaHomeModel model = new AquaHomeModel(int.Parse(id));
           
            return View(model);
        }
        [ValidateInput(false)]
        public void InsertOrUpdateHome(Core.AquaHome home)
        {
            bool success = false;
            bool newHome = home.Home_Id == 0;
            try
            {
                string folderGallery = "/gallery";
                string rootPath = "Content/images/home"; //+ (evento.Event_Title.ToLower().Replace(" ", "-"));

                List<HttpPostedFileBase> galleryList = new List<HttpPostedFileBase>();
                if (Request.Files.Count > 0)
                {
                    foreach (string fileKey in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[fileKey];
                        string pathFile = fileKey.Contains("News_Gallery") ? folderGallery : string.Empty;
                        if (fileKey.Contains("News_Gallery")) galleryList.Add(file);
                        else home.GetType().GetProperty(fileKey).SetValue(home, rootPath + "/" + file.FileName, null);

                        success = (new BackOfficeAcquaJoss.Models.FileUpload().Upload(file, new Files()
                        {
                            Name = file.FileName,
                            Size = file.ContentLength,
                            Folder = rootPath + pathFile,
                        }) == 1);
                    }
                }
                int response = Core.AquaHome.Instance().InsertOrUpdate(home);
                success = (response != 0);
                home.Home_Id = home.Home_Id == 0 ? response : home.Home_Id;
            }
            catch (Exception ex)
            {
                success = false;
                Console.WriteLine(ex.Message);
            }
            TempData["Action"] = newHome ? "insert" : "update";
            TempData["Status"] = success;
        }
        #endregion

        #region Customers
        //private CustomersModel CustomersModel
        //{
        //    get
        //    {
        //        CustomersModel model = new CustomersModel();
        //        model.Customers = Core.Customers.GetClients();
        //        model.Schools = Core.Customers.GetSchools();
        //        return model;
        //    }
        //}

        //public ActionResult Customers()
        //{
        //    return View(new CustomersModel());
        //}

        #endregion

        #region HotelUsers
        public ActionResult HotelUsers()
        {
            return View(new HotelUsersModel());
        }

        [HttpPost]
        [Route("admin/hotelusers/InsertOrUpdate")]
        public JsonResult InsertOrUpdate(List<HotelUsersXML> hotelusers)
        {
            List<HotelUsers> result = HotelUsersXML.InsertOrUpdate(hotelusers);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Convenzioni
        public ActionResult Convenzioni(string id = "1")
        {

            ConvenzioniModel model = new ConvenzioniModel(int.Parse(id));

            return View(model);
        }
        [ValidateInput(false)]
        public void InsertOrUpdateConvenzioni(Core.Convenzion home)
        {
            bool success = false;
            bool newHome = home.Convenzioni_Id == 0;
            try
            {
                string folderGallery = "/gallery";
                string rootPath = "Content/images/convenzioni"; //+ (evento.Event_Title.ToLower().Replace(" ", "-"));

                List<HttpPostedFileBase> galleryList = new List<HttpPostedFileBase>();
                if (Request.Files.Count > 0)
                {
                    foreach (string fileKey in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[fileKey];
                        string pathFile = fileKey.Contains("News_Gallery") ? folderGallery : string.Empty;
                        if (fileKey.Contains("News_Gallery")) galleryList.Add(file);
                        else home.GetType().GetProperty(fileKey).SetValue(home, rootPath + "/" + file.FileName, null);

                        success = (new BackOfficeAcquaJoss.Models.FileUpload().Upload(file, new Files()
                        {
                            Name = file.FileName,
                            Size = file.ContentLength,
                            Folder = rootPath + pathFile,
                        }) == 1);
                    }
                }
                int response = Core.Convenzion.Instance().InsertOrUpdate(home);
                success = (response != 0);
                home.Convenzioni_Id = home.Convenzioni_Id == 0 ? response : home.Convenzioni_Id;
            }
            catch (Exception ex)
            {
                success = false;
                Console.WriteLine(ex.Message);
            }
            TempData["Action"] = newHome ? "insert" : "update";
            TempData["Status"] = success;
        }
        #endregion
    }
}