﻿using BackOfficeAcquaJoss.Models;
using Core;
using Core.Db;
using Core.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;

namespace BackOfficeAcquaJoss.Controllers
{
    public class AjaxController : Controller
    {
        private static string Conexion = Settings.Default.DT_BackOffice;

        [HttpPost]
        public ViewResult GetHotels(string lang, int rooms, int adults, int kids)
        {
            HotelsMoldel model = new HotelsMoldel(lang, rooms, adults, kids);
            return View("~/Views/Products/ProductData/_Hotels.cshtml", model);
        }

        [HttpPost]
        public string GetDynamicProductRate(int idProduct, DateTime? date = null, string media = SaleChannel.INTERNETSALES)
        {
            SqlCommand command = new SqlCommand("spBackOfficeGetDynamicProductRate");
            command.Parameters.Add(new SqlParameter("@IdProduct", idProduct));
            if (date.HasValue) command.Parameters.Add(new SqlParameter("@Date", date.Value.ToString(CRUD.DATEFORMAT)));
            command.Parameters.Add(new SqlParameter("@Channel", media));
            string response = CRUD.ExecuteCommandQuery(Conexion, command);
            return date.HasValue ? CRUD.ToObject(response) : response;
        }

        [HttpPost]
        public string GetProductRate(int idProduct, DateTime? date = null, string media = SaleChannel.INTERNETSALES)
        {
            SqlCommand command = new SqlCommand("spBackOfficeGetProductRate");
            command.Parameters.Add(new SqlParameter("@IdProduct", idProduct));
            if(date.HasValue) command.Parameters.Add(new SqlParameter("@Date", date.Value.ToString(CRUD.DATEFORMAT)));
            command.Parameters.Add(new SqlParameter("@Channel", media));
            string response = CRUD.ExecuteCommandQuery(Conexion, command);
            return date.HasValue ? CRUD.ToObject(response) : response;
        }

        [HttpPost]
        public string GetProductAlltoment(int idProduct, DateTime? date = null)
        {
            SqlCommand command = new SqlCommand("spBackOfficeGetScheduleAllotment");
            command.Parameters.Add(new SqlParameter("@ProductId", idProduct));
            if (date.HasValue) command.Parameters.Add(new SqlParameter("@SaleDate", date.Value.ToString(CRUD.DATEFORMAT)));
            string response = CRUD.ExecuteCommandQuery(Conexion, command);
            return date.HasValue ? CRUD.ToSerializeObject(response) : response;
        }

        [HttpPost]
        public string Reservation(CheckoutReservation checkoutReservation) {
            try
            {
                XmlDocument document = new XmlDocument();
                XmlElement reservaDet = document.CreateElement("Reservations");

                CheckoutReservations reservations = checkoutReservation.reservations;
                int idx = 0;
                if (reservations.admissions != null && reservations.admissions.Count > 0)
                {
                    foreach (Admission a in reservations.admissions)
                    {
                        XmlElement admision = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = "admission";
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = a.idProduct.ToString();
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Date");
                        attribute.Value = a.date.ToString(CRUD.DATEFORMAT);
                        admision.Attributes.Append(attribute);
                        
                        attribute = document.CreateAttribute("Schedule");
                        attribute.Value = a.schedule;
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = a.quantity.ToString();
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Coupon");
                        attribute.Value = a.coupon.ToString();
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = a.price.ToString("0.00", new CultureInfo("en"));
                        admision.Attributes.Append(attribute);
                        
                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = a.currency_code;
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = a.discountCoupon != null ? a.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (a.discountCoupon != null ? a.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = a.subtotal.ToString("0.00", new CultureInfo("en"));
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = a.total.ToString("0.00", new CultureInfo("en"));
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        admision.Attributes.Append(attribute);

                        reservaDet.AppendChild(admision);
                    }
                }

                if (reservations.dynamics != null && reservations.dynamics.Count > 0)
                {
                    foreach (Dynamic a in reservations.dynamics)
                    {
                        XmlElement dynamic = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = "dynamic";
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = a.idProduct.ToString();
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Date");
                        attribute.Value = a.date.ToString(CRUD.DATEFORMAT);
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Schedule");
                        attribute.Value = a.schedule;
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = a.quantity.ToString();
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Coupon");
                        attribute.Value = a.coupon;
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = a.price.ToString("0.00", new CultureInfo("en"));
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = a.currency_code;
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = a.discountCoupon != null ? a.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (a.discountCoupon != null ? a.discountCoupon.DiscountCoupon_Value : 0).ToString("0.00", new CultureInfo("en"));
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = a.subtotal.ToString("0.00", new CultureInfo("en"));
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = a.total.ToString("0.00", new CultureInfo("en"));
                        dynamic.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        dynamic.Attributes.Append(attribute);

                        reservaDet.AppendChild(dynamic);
                    }
                }

                if (reservations.schools != null && reservations.schools.Count > 0)
                {
                    foreach (School s in reservations.schools)
                    {
                        XmlElement admision = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = ProductIdentifier.School;
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = s.idProduct.ToString();
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Date");
                        attribute.Value = s.date.ToString(CRUD.DATEFORMAT);
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Schedule");
                        attribute.Value = s.schedule;
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = s.quantity.ToString();
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Coupon");
                        attribute.Value = s.coupon.ToString();
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = s.price.ToString("0.00", new CultureInfo("en"));
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = s.currency_code;
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = s.discountCoupon != null ? s.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (s.discountCoupon != null ? s.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = s.subtotal.ToString("0.00", new CultureInfo("en"));
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = s.total.ToString("0.00", new CultureInfo("en"));
                        admision.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        admision.Attributes.Append(attribute);

                        reservaDet.AppendChild(admision);
                    }
                }

                if (reservations.extras != null && reservations.extras.Count > 0)
                {
                    foreach (Models.Extras e in reservations.extras)
                    {
                        XmlElement extra = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = "extra";
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = e.idProduct.ToString();
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Date");
                        attribute.Value = e.date.ToString(CRUD.DATEFORMAT);
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = e.quantity.ToString();
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = e.price.ToString("0.00", new CultureInfo("en"));
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = e.currency_code;
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = e.discountCoupon != null ? e.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (e.discountCoupon != null ? e.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = e.subtotal.ToString("0.00", new CultureInfo("en"));
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = e.total.ToString("0.00", new CultureInfo("en"));
                        extra.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        extra.Attributes.Append(attribute);

                        reservaDet.AppendChild(extra);
                    }
                }

                if (reservations.activities != null && reservations.activities.Count > 0)
                {
                    foreach (Models.Activities a in reservations.activities)
                    {
                        XmlElement activity = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = "activity";
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = a.idProduct.ToString();
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Date");
                        attribute.Value = a.date.ToString(CRUD.DATEFORMAT);
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = a.quantity.ToString();
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = a.price.ToString("0.00", new CultureInfo("en"));
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = a.currency_code;
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Schedule");
                        attribute.Value = a.schedule;
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = a.discountCoupon != null ? a.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (a.discountCoupon != null ? a.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = a.subtotal.ToString("0.00", new CultureInfo("en"));
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = a.total.ToString("0.00", new CultureInfo("en"));
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        activity.Attributes.Append(attribute);

                        reservaDet.AppendChild(activity);
                    }
                }

                if (reservations.activitiesschools != null && reservations.activitiesschools.Count > 0)
                {
                    foreach (Models.ActivitiesSchools a in reservations.activitiesschools)
                    {
                        XmlElement activity = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = ProductIdentifier.ActivitySchool;
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = a.idProduct.ToString();
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Date");
                        attribute.Value = a.date.ToString(CRUD.DATEFORMAT);
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = a.quantity.ToString();
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = a.price.ToString("0.00", new CultureInfo("en"));
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = a.currency_code;
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Schedule");
                        attribute.Value = a.schedule;
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = a.discountCoupon != null ? a.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (a.discountCoupon != null ? a.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = a.subtotal.ToString("0.00", new CultureInfo("en"));
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = a.total.ToString("0.00", new CultureInfo("en"));
                        activity.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        activity.Attributes.Append(attribute);

                        reservaDet.AppendChild(activity);
                    }
                }

                if (reservations.menus != null && reservations.menus.Count > 0)
                {
                    foreach (Menu m in reservations.menus)
                    {
                        XmlElement menu = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = "menu";
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = m.idProduct.ToString();
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Date");
                        attribute.Value = m.date.ToString(CRUD.DATEFORMAT);
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = m.quantity.ToString();
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = m.price.ToString("0.00", new CultureInfo("en"));
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = m.currency_code;
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = m.discountCoupon != null ? m.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (m.discountCoupon != null ? m.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = m.subtotal.ToString("0.00", new CultureInfo("en"));
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = m.total.ToString("0.00", new CultureInfo("en"));
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        menu.Attributes.Append(attribute);

                        reservaDet.AppendChild(menu);
                    }
                }

                if (reservations.menusschools != null && reservations.menusschools.Count > 0)
                {
                    foreach (MenuSchools m in reservations.menusschools)
                    {
                        XmlElement menu = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = ProductIdentifier.MenuSchool;
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = m.idProduct.ToString();
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Date");
                        attribute.Value = m.date.ToString(CRUD.DATEFORMAT);
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = m.quantity.ToString();
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = m.price.ToString("0.00", new CultureInfo("en"));
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = m.currency_code;
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = m.discountCoupon != null ? m.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (m.discountCoupon != null ? m.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = m.subtotal.ToString("0.00", new CultureInfo("en"));
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = m.total.ToString("0.00", new CultureInfo("en"));
                        menu.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        menu.Attributes.Append(attribute);

                        reservaDet.AppendChild(menu);
                    }
                }

                if (reservations.gadgets != null && reservations.gadgets.Count > 0)
                {
                    foreach (Gadget g in reservations.gadgets)
                    {
                        XmlElement gadget = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = "gadget";
                        gadget.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = g.idProduct.ToString();
                        gadget.Attributes.Append(attribute);
                        
                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = g.quantity.ToString();
                        gadget.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = g.discountCoupon != null ? g.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        gadget.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (g.discountCoupon != null ? g.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        gadget.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = g.subtotal.ToString("0.00", new CultureInfo("en"));
                        gadget.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = g.total.ToString("0.00", new CultureInfo("en"));
                        gadget.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        gadget.Attributes.Append(attribute);

                        reservaDet.AppendChild(gadget);
                    }
                }
                               
                if (reservations.hotels != null && reservations.hotels.Count > 0) {
                    foreach (Models.Hotel h in reservations.hotels)
                    {
                        XmlElement hotel = document.CreateElement("Item");
                        XmlAttribute attribute = document.CreateAttribute("Type");
                        attribute.Value = "hotel";
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("IdProduct");
                        attribute.Value = h.idProduct.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Adults");
                        attribute.Value = h.adults.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Kids");
                        attribute.Value = h.kids.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Infants");
                        attribute.Value = h.infants.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Rooms");
                        attribute.Value = h.rooms.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Days");
                        attribute.Value = h.days.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Quantity");
                        attribute.Value = h.quantity.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Price");
                        attribute.Value = h.price.ToString("0.00", new CultureInfo("en"));
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CouponId");
                        attribute.Value = h.discountCoupon != null ? h.discountCoupon.DiscountCoupon_Id.ToString() : string.Empty;
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Discount");
                        attribute.Value = (h.discountCoupon != null ? h.discount_value : 0).ToString("0.00", new CultureInfo("en"));
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Subtotal");
                        attribute.Value = h.subtotal.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Total");
                        attribute.Value = h.total.ToString("0.00", new CultureInfo("en"));
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Arrival");
                        attribute.Value = h.arrival.ToString(CRUD.DATEFORMAT);
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Departure");
                        attribute.Value = h.departure.ToString(CRUD.DATEFORMAT);
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("CurrencyCode");
                        attribute.Value = h.currency_code;
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Tickets");
                        attribute.Value = h.tickets.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("Nights");
                        attribute.Value = h.nights.ToString();
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("DatesOpened");
                        attribute.Value = string.Join(",", h.datesOpened.Select(d => d.ToString(CRUD.DATEFORMAT)).ToArray());
                        hotel.Attributes.Append(attribute);

                        attribute = document.CreateAttribute("DatesSelected");
                        attribute.Value = string.Join(",", h.datesSelected.Select(d => d.ToString(CRUD.DATEFORMAT)).ToArray());
                        hotel.Attributes.Append(attribute);


                        int i = 0;
                        foreach(RoomType rt in h.roomsTypes)
                        {
                            XmlElement roomsType = document.CreateElement("RoomType");
                            XmlAttribute adultsAttr = document.CreateAttribute("Adults");
                            adultsAttr.Value = rt.adults.ToString();
                            roomsType.Attributes.Append(adultsAttr);

                            XmlAttribute kidsAtrr = document.CreateAttribute("Kids");
                            kidsAtrr.Value = rt.kids.ToString();
                            roomsType.Attributes.Append(kidsAtrr);

                            XmlAttribute infantsAttr = document.CreateAttribute("Infants");
                            infantsAttr.Value = rt.infants.ToString();
                            roomsType.Attributes.Append(infantsAttr);

                            XmlAttribute typeAttr = document.CreateAttribute("RoomType");
                            typeAttr.Value = rt.roomType;
                            roomsType.Attributes.Append(typeAttr);

                            attribute = document.CreateAttribute("Idx");
                            attribute.Value = (i++).ToString();
                            roomsType.Attributes.Append(attribute);

                            hotel.AppendChild(roomsType);
                        }
                        
                        attribute = document.CreateAttribute("Idx");
                        attribute.Value = (idx++).ToString();
                        hotel.Attributes.Append(attribute);

                        reservaDet.AppendChild(hotel);
                    }
                }

                document.AppendChild(reservaDet);

                using (SqlConnection conn = new SqlConnection(Conexion))
                {
                    SqlCommand command = new SqlCommand("spBackOfficeReservationCreate", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    //RESERVATION HEADER
                    command.Parameters.Add(new SqlParameter("@Reservation_IdSeller", Global.UserProfile.User_Id));
                    command.Parameters.Add(new SqlParameter("@Reservation_IdSalesSource", checkoutReservation.reservation.Reservation_IdSalesSource));
                    command.Parameters.Add(new SqlParameter("@Reservation_Subtotal", checkoutReservation.reservation.Reservation_Subtotal));
                    command.Parameters.Add(new SqlParameter("@Reservation_Total", checkoutReservation.reservation.Reservation_Total));
                    command.Parameters.Add(new SqlParameter("@Reservation_CurrencyCode", checkoutReservation.reservation.Reservation_CurrencyCode));

                    //CUSTOMER DATA
                    Customers customer = checkoutReservation.customer;
                    command.Parameters.Add(new SqlParameter("@Customer_Id", customer.Customer_Id));
                    command.Parameters.Add(new SqlParameter("@Customer_Name", string.IsNullOrEmpty(customer.Customer_Name) ? string.Empty : customer.Customer_Name.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_LastName", string.IsNullOrEmpty(customer.Customer_LastName) ? string.Empty : customer.Customer_LastName.Trim().ToUpper()));

                    command.Parameters.Add(new SqlParameter("@Customer_Company_Name", string.IsNullOrEmpty(customer.Customer_Company_Name) ? string.Empty : customer.Customer_Company_Name.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_Direction", string.IsNullOrEmpty(customer.Customer_Direction) ? string.Empty : customer.Customer_Direction.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_SchoolCategory", string.IsNullOrEmpty(customer.Customer_SchoolCategory) ? string.Empty : customer.Customer_SchoolCategory.Trim().ToUpper()));

                    command.Parameters.Add(new SqlParameter("@Customer_Sex", string.IsNullOrEmpty(customer.Customer_Sex) ? string.Empty : customer.Customer_Sex.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_Email", string.IsNullOrEmpty(customer.Customer_Email) ? string.Empty : customer.Customer_Email.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_CellPhone", string.IsNullOrEmpty(customer.Customer_CellPhone) ? string.Empty : customer.Customer_CellPhone.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_Phone", string.IsNullOrEmpty(customer.Customer_Phone) ? string.Empty : customer.Customer_Phone.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_CountryCode", string.IsNullOrEmpty(customer.Customer_CountryCode) ? string.Empty : customer.Customer_CountryCode.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_City", string.IsNullOrEmpty(customer.Customer_City) ? string.Empty : customer.Customer_City.Trim().ToUpper()));
                    command.Parameters.Add(new SqlParameter("@Customer_ZipCode", string.IsNullOrEmpty(customer.Customer_ZipCode) ? string.Empty : customer.Customer_ZipCode.Trim().ToUpper()));

                    //RESERVATIONS ITEMS
                    command.Parameters.Add(new SqlParameter("@XMLItems", document.InnerXml));

                    //Add IdReservation the output parameter to the command object
                    SqlParameter idReservation = new SqlParameter();
                    idReservation.ParameterName = "@IdReservation";
                    idReservation.SqlDbType = SqlDbType.Int;
                    idReservation.Direction = ParameterDirection.Output;
                    command.Parameters.Add(idReservation);
                    
                    SqlParameter idCustomer = new SqlParameter();
                    idCustomer.ParameterName = "@IdCustomer";
                    idCustomer.SqlDbType = SqlDbType.Int;
                    idCustomer.Direction = ParameterDirection.Output;
                    command.Parameters.Add(idCustomer);
                    
                    SqlParameter status = new SqlParameter();
                    status.ParameterName = "@Status";
                    status.SqlDbType = SqlDbType.Bit;
                    status.Direction = ParameterDirection.Output;
                    command.Parameters.Add(status);

                    command.Connection.Open();
                    using (SqlDataReader rdr = command.ExecuteReader())
                    {
                        return CRUD.ToObject(CRUD.ToSerializeObject(
                            new
                            {
                                idReserva = idReservation.Value,
                                idCliente = idCustomer.Value,
                                status = status.Value
                            }
                        ));
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("NewReservation ERROR EXCEPTION");
                System.Diagnostics.Debug.WriteLine(ex.Message);
                Log.Write("NEW RESERVATION:");
                Log.Write("ERROR (" + ex.Source + "): " + ex.Message);
                Log.Write("LINE: " + ex.StackTrace);
            }
            return null;
        }


        #region Post Discount Coupons
        [HttpPost]
        public string GetDiscountCouponAvailable(DateTime? date = null, int? product_Id = null)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            if (date.HasValue)
            {
                parameters.Add(new SqlParameter("@SwimDate", date.Value.ToString(CRUD.DATEFORMAT)));
                if (product_Id.HasValue)
                {
                    parameters.Add(new SqlParameter("@ProductID", product_Id.Value));
                }
                string result = JsonConvert.SerializeObject(DiscountCoupon.Instance().GetDiscountCouponsAvailable(parameters));
                return result;
            }

            return null;            
        }
        #endregion

        #region Customers
        [HttpPost]
        public JsonResult GetCustomersClientsJson(string table, string allData)
        {
            List<Customers> list = Customers.GetCustomerOrSchools(table, allData);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetCustomersSchoolsJson(string table, string allData)
        {
            List<Customers> list = Customers.GetCustomerOrSchools(table, allData);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSchoolsLocation(string allData)
        {
            return Json(Customers.GetLocations(allData), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public int ChangeCustomers(int id, bool status, string column)
        {
            return Customers.Instance().ChangeStatus(id, status, column);
        }

        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult CustomersInsertOrUpdate(Customers customer)
        {
            int res = Customers.Instance().InsertOrUpdate(customer);
            if (customer.Customer_Id == 0) customer.Customer_Id = res;

            return Json(customer);
        }
        #endregion

        #region Telemarketing
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult TelemarketingInsertOrUpdate(Telemarketing telemarketing)
        {
            int res = Telemarketing.Instance().InsertOrUpdate(telemarketing);
            if (telemarketing.Telemarketing_Id == 0) telemarketing.Telemarketing_Id = res;

            return Json(telemarketing);
        }
        #endregion

        #region School Booking Summary
        //[HttpPost]
        public ActionResult Export()
        {
            ReservationModel model = new ReservationModel(2527);
            return View("~/Views/Shared/Reservations/_BookingSummaryXLS.cshtml", model);
        }

        public void ExportToExcel()
        {
            string FileName = "BookingSummary" + DateTime.Now.ToString("mm_dd_yyy_hh_ss_tt") + ".xls";

            ReservationModel model = new ReservationModel(2527);

            string HtmlResult = RenderRazorViewToString("~/Views/Shared/Reservations/_BookingSummaryXLS.cshtml", model);

            byte[] ExcelBytes = Encoding.ASCII.GetBytes(HtmlResult);

            Response.ContentType = "application/ms-excel";
            Response.Charset = "UTF-8";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FileName));
            Response.Write(HtmlResult);
            Response.End();
            Response.Flush();
        }

        protected string RenderRazorViewToString(string viewName, object model)
        {
            if (model != null)
            {
                ViewData.Model = model;
            }
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}