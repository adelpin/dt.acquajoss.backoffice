﻿using BackOfficeAcquaJoss.Models;
using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AddonsModel = BackOfficeAcquaJoss.Models.AddonsModel;

namespace BackOfficeAcquaJoss.Controllers
{
    public class AddonsController : Controller
    {
        private AddonsModel AddonsModel{
            get
            {
                AddonsModel model = new AddonsModel();
                model.Addons = new Products().GetAddons();
                return model;
            }
        }

        private ProductsInfoModel ProductsInfoModel
        {
            get
            {
                ProductsInfo p = new ProductsInfo();
                ProductsInfoModel m = new ProductsInfoModel
                {
                    ProductsInfo = p.GetProductsInfo(Global.Lang),
                    Products = Products.Instance(Global.Lang).GetProducts()
                };
                return m;
            }
        }

        #region Addon
        public ActionResult Addon()
        {
            //addon products
            ViewBag.CurrentParkOperational = Repository.GetCurrentParkOperational();
            ViewBag.parkOperationalActive = Repository.GetParkOpListAllNosubCalendars();

            ViewBag.HighPrice = "0";
            ViewBag.LowPrice = "0";

            return View(AddonsModel);
        }

        public ActionResult Update(string id)
        {
            AddonsModel model = AddonsModel;
            if (id != null)
            {
                model.Addon = Products.Instance(Global.Lang).GetProduct(int.Parse(id));
            }

            int CurrentParkOperational = Repository.GetCurrentParkOperational();

            ViewBag.CurrentParkOperational = CurrentParkOperational;
            ViewBag.parkOperationalActive = Repository.GetParkOpListAllNosubCalendars();

            string HighPrice = "0";
            string LowPrice = "0";
            string HighPriceBase = "0";
            string LowPriceBase = "0";

            if (id != null)
            {

                int PrcId = Repository.GetProductPrices(CurrentParkOperational, Convert.ToInt32(id), ref HighPrice, ref LowPrice, ref HighPriceBase, ref LowPriceBase);
                ViewBag.HighPrice = HighPrice;
                ViewBag.LowPrice = LowPrice;
                ViewBag.HighPriceBase = HighPriceBase;
                ViewBag.LowPriceBase = LowPriceBase;
            }

            return View("~/Views/Addons/Addon.cshtml", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertOrUpdate(Products addon, string HighPrice, string LowPrice, string HighPriceBase, string LowPriceBase, int ParkOp)
        {
            string option = addon.Product_Id == 0 ? "insert" : "update";
            AddonsModel model = AddonsModel;
            int res = Products.Instance(Global.Lang).InsertOrUpdate(addon);
            TempData["Action"] = option;
            TempData["Status"] = (res != 0);
            model.Addon = addon;
            if (res != 0)
            {
                if (addon.Product_Id == 0)
                {
                    addon.Product_Id = res;
                }
                int Product_Id = addon.Product_Id;
                Repository.InsertOrUpdateProductPrices(Product_Id, ParkOp, HighPrice, LowPrice, HighPriceBase, LowPriceBase);

                return RedirectToAction("Update", new { id = addon.Product_Id });
            }
            return RedirectToAction("Addon");
        }

        [HttpPost]
        public int ChangeStatus(int addon_id, bool addon_status)
        {
            return Products.Instance(Global.Lang).ChangeStatus(addon_id, addon_status);
        }
        #endregion

        #region UpdateInfo
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateInfo(Products product, string language = null)
        {
            ProductsInfoModel m = ProductsInfoModel;
            m.Product = product;
            m.ProductInfo = ProductsInfo.Instance().GetProductInfo(product.Product_Id, language ?? Global.Lang);
            if (m.ProductInfo == null)
            {
                m.ProductInfo = new ProductsInfo
                {
                    ProductInfo_IdProduct = product.Product_Id,
                    ProductInfo_LanguageCode = language
                };
                m.ProductsImage = ProductsImage.Instance().GetProductsImage(m.ProductInfo.ProductInfo_Id);
            }
            else
                m.ProductsImage = ProductsImage.Instance().GetProductsImage(m.ProductInfo.ProductInfo_Id);

            m.Hotel = Hotels.Instance().GetHotel(product.Product_Id);
            if (m.Hotel == null)
            {
                m.Hotel = new Hotels
                {
                    Hotel_IdProduct = product.Product_Id
                };
            }

            if (m.ProductsSchedules == null)
            {
                m.ProductsSchedules = new List<ProductsSchedules>();
            }

            //park op active in select list item
            List<ParkOperational> Popen = m.ParkOperationals.Where(x => x.ParkOperational_Status == true).ToList();
            ViewBag.Popen = Popen.Select(i => new SelectListItem() { Text = i.ParkOperational_Code, Value = i.ParkOperational_Id.ToString() });

            ViewBag.Folder = "addons";
            return PartialView("~/Views/Products/ProductData/_ProductInfo.cshtml", m);
        }
        #endregion
    }
}