﻿using BackOfficeAcquaJoss.Models;
using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Sales(FormCollection data = null)
        {
            ReportsModel.ReportsSales m = new ReportsModel.ReportsSales(data);

            return View(m);
        }

        public ActionResult SalesPostalCodes(FormCollection data = null, int page = 0)
        {
            ReportsModel.ReservationSalesPostalCodes Result = new ReportsModel.ReservationSalesPostalCodes(data);

            //must be included the products disabled
            List<Core.ProductBaseInfo> AllProducts = Repository.GetAllProducts().OrderBy(x => x.Key).ToList();
            ViewBag.Products = AllProducts;

            foreach (var key in data.AllKeys)
            {
                if (key == "Product_Id")
                {
                    string val = data[key];

                    ViewBag.product_id = null;

                    if (val != "0")
                    {
                        ViewBag.product_id = val;
                    }

                }
            }

            return View(Result);
        }

        public ActionResult SalesSellers(FormCollection data = null, int page = 0)
        {
            ReportsModel.ReservationSalesSellers Result = new ReportsModel.ReservationSalesSellers(data);

            //must be included the products disabled
            List<Core.ProductBaseInfo> AllProducts = Repository.GetAllProducts().OrderBy(x => x.Key).ToList();
            ViewBag.Products = AllProducts;

            foreach (var key in data.AllKeys)
            {
                if (key == "Product_Id")
                {
                    string val = data[key];

                    ViewBag.product_id = null;

                    if (val != "0")
                    {
                        ViewBag.product_id = val;
                    }

                }
            }

            return View(Result);
        }

        public ActionResult SalesCustomers(FormCollection data = null, int page = 0)
        {
            ReportsModel.ReservationSalesCustomers Result = new ReportsModel.ReservationSalesCustomers(data);

            //must be included the products disabled
            List<Core.ProductBaseInfo> AllProducts = Repository.GetAllProducts().OrderBy(x => x.Key).ToList();
            ViewBag.Products = AllProducts;

            foreach (var key in data.AllKeys)
            {
                if (key == "Product_Id")
                {
                    string val = data[key];

                    ViewBag.product_id = null;

                    if (val != "0")
                    {
                        ViewBag.product_id = val;
                    }

                }
            }

            return View(Result);
        }


        public ActionResult SalesDetails(FormCollection data = null, int page = 0)
        {
            ReportsModel.ReservationSalesDetailsZM Result = new ReportsModel.ReservationSalesDetailsZM(data);

            //must be included the products disabled
            List<Core.ProductBaseInfo> AllProducts = Repository.GetAllProducts().OrderBy(x => x.Key).ToList();
            ViewBag.Products = AllProducts;

            foreach (var key in data.AllKeys)
            {
                if (key == "Product_Id")
                {
                    string val = data[key];

                    ViewBag.product_id = null;

                    if (val != "0")
                    {
                        ViewBag.product_id = val;
                    }

                }
            }

            List<PaymentType> Lp = Repository.GetPaymentType();
            List<SelectListItem> PaymentsType = new List<SelectListItem>();

            foreach (var el in Lp)
            {
                SelectListItem sl = new SelectListItem();
                sl.Value = el.Id.ToString();
                sl.Text = el.Description;
                PaymentsType.Add(sl);
            }

            ViewBag.PaymentType = PaymentsType;

            ViewBag.Seller_Id = "";
            if (Request.Form["Seller_Id"] != null)
            {
                string ss = Request.Form["Seller_Id"].ToString();
                if (ss != "")
                { ViewBag.Seller_Id = Convert.ToInt16(ss); }
            }

            ViewBag.Payment_Id = "";
            if (Request.Form["Payment_Id"] != null)
            {
                string ss = Request.Form["Payment_Id"].ToString();
                if (ss != "")
                { ViewBag.Payment_Id = Convert.ToInt16(ss); }
            }


            ViewBag.Invoice = "";
            if (Request.Form["Invoice"] != null)
            {
                string ss = Request.Form["Invoice"].ToString();
                if (ss != "")
                { ViewBag.Invoice = Convert.ToInt16(ss); }
            }

            return View(Result);
        }


        public ActionResult SalesAddons(FormCollection data = null)
        {
            ReportsModel.ReportsSalesAddons Resultado = new ReportsModel.ReportsSalesAddons(data);

            return View(Resultado);
        }


        public ActionResult SalesDealers(FormCollection data = null)
        {
            ReportsModel.ReportsSalesDealers Resultado = new ReportsModel.ReportsSalesDealers(data);

            return View(Resultado);
        }

        public ActionResult SalesSubscriptionNames(FormCollection data = null)
        {
            ReportsModel.ReportSalesSubscriptionNames Result = new ReportsModel.ReportSalesSubscriptionNames(data);

            //must be included the products disabled
            List<Core.ProductBaseInfo> AllProducts = Repository.GetAllProducts().OrderBy(x => x.Key).ToList();
            ViewBag.Products = AllProducts.Where(x => x.Type == "subscription").ToList();

            KeepSelectedProduct(data);

            return View(Result);
        }

        private void KeepSelectedProduct(FormCollection data)
        {
            foreach (var key in data.AllKeys)
            {
                if (key == "Product_Id")
                {
                    string val = data[key];

                    ViewBag.product_id = null;

                    if (val != "0")
                    {
                        ViewBag.product_id = val;
                    }

                }
            }
        }

    }
}