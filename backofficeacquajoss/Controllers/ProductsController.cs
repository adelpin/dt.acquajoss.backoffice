﻿using BackOfficeAcquaJoss.Models;
using Core;
using Core.Db;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class ProductsController : Controller
    {
        private ProductsModel ProductsModel
        {
            get
            {
                Products P = new Products();
                ProductsModel Model = new ProductsModel
                {
                    Products = P.GetProducts(ProductType.PRODUCT)
                };
                return Model;
            }
        }

        private ProductsInfoModel ProductsInfoModel
        {
            get
            {
                ProductsInfo p = new ProductsInfo();
                ProductsInfoModel m = new ProductsInfoModel
                {
                    ProductsInfo = p.GetProductsInfo(Global.Lang),
                    Products = Products.Instance(Global.Lang).GetProducts(Global.Lang),
                    ProductsAllotments = ProductsAllotment.Instance().GetProductsAllotments()
                };
                return m;
            }
        }

        private ProductsImageModel ProductsImageModel
        {
            get
            {
                ProductsImage p = new ProductsImage();
                ProductsImageModel m = new ProductsImageModel
                {
                    ProductsImage = p.GetProductsImage()
                };
                return m;
            }
        }

        private HotelsModel HotelsModel
        {
            get
            {
                Hotels h = new Hotels();
                HotelsModel m = new HotelsModel
                {
                    Hotel = null
                };
                return m;
            }
        }

        #region Dematerialized

        public JsonResult InsertPriceTravel(string Description, string Price, string Tmaster, int? IdPackage, int idParkOperational)
        {
            int Idp = 0;
            if (IdPackage != null) { Idp = (int)IdPackage; }
            decimal Pr = (decimal)Repository.ConvertiPrezzo(Price);
            decimal Tm = (decimal)Repository.ConvertiPrezzo(Tmaster);
            Repository.InsertPriceTravel(Description, Pr, Tm, Idp, idParkOperational);

            return Json("");
        }

        public JsonResult UpdatePriceTravel(int Id, string Description, string Price, string Tmaster, int? IdPackage, int idParkOperational)
        {
            int Idp = 0;
            if (IdPackage != null) { Idp = (int)IdPackage; }
            decimal Pr = (decimal)Repository.ConvertiPrezzo(Price);
            decimal Tm = (decimal)Repository.ConvertiPrezzo(Tmaster);
            Repository.UpdatePriceTravel(Id, Description, Pr, Tm, Idp, idParkOperational);

            return Json("");
        }

        public JsonResult DeletePriceTravel(int Id)
        {

            Repository.DeletePriceTravel(Id);


            return Json("");
        }

        public ActionResult Dematerialized(int ProdSel = 0, int ParkOp = 0)
        {
            PriceModelTravel Model = new PriceModelTravel();

            Model.ProdSel = ProdSel;

            //no more packages but all the other products
            //Model.TravelPackages = Repository.GetProducts().Where(x => x.Type == "package").ToList();
            List<ProductBaseInfo> HotProducts = Repository.GetProducts().OrderBy(x => x.Key).Where(y => y.Type == "hotel" || y.Key == "EXTRANIGHT1").ToList();

            Model.TravelProducts = HotProducts;

            //default empty
            Model.TravelProducts.Insert(0, new ProductBaseInfo());

            List<ParkOperational> Lista = Repository.GetParkOpListAllNosubCalendars();
            ViewBag.parkOperationalActive = Lista;

            if (ParkOp == 0)
            {
                ParkOp = Lista[0].ParkOperational_Id;
            }

            ViewBag.ParkOp = ParkOp;

            List<PriceTravel> PriceTravelList = Repository.GetPriceTravel(ParkOp);

            Model.PriceTravelList = PriceTravelList;

            return View(Model);
        }

        #endregion

        #region Product
        public JsonResult GetHotelSeasonPrice(int IDHotel, int IdOperational)
        {
            int PriceId = Repository.GetHotelSeasonPrice(IDHotel, IdOperational);

            return Json(PriceId);
        }

        public JsonResult GetProductSeasonPrices(int ParkOperational, int ProductId)
        {

            int PrcId = Repository.GetPriceList(ParkOperational);

            if (PrcId == 0)
            {
                return Json("KO");
            }

            string HighPrice = "0";
            string LowPrice = "0";
            string HighPriceBase = "0";
            string LowPriceBase = "0";

            Repository.GetProductPrices(ParkOperational, ProductId, ref HighPrice, ref LowPrice, ref HighPriceBase, ref LowPriceBase);

            SeasonPrices Prices = new SeasonPrices();

            Prices.HighPrice = HighPrice;
            Prices.LowPrice = LowPrice;

            return Json(Prices);
        }

        public ActionResult Product()
        {
            //main prod
            ViewBag.CurrentParkOperational = Repository.GetCurrentParkOperational();
            ViewBag.parkOperationalActive = Repository.GetParkOpListAll();

            ViewBag.HighPrice = "0";
            ViewBag.LowPrice = "0";

            return View(ProductsModel);
        }

        public JsonResult DeleteScheduleAllotment(string Id)
        {
            if (Repository.DeleteScheduleAllotment(Id) != 0)
            {
                return Json("KO");
            }

            return Json("");
        }

        public ActionResult DynPrices(int? Year = null, int? Month = null)
        {
            if (Year == null)
            {
                //current season
                Year = DateTime.Now.Year;
            }

            List<DynamicPrice> DynamicPrices = new List<DynamicPrice>();

            if (Month != null)
            {
                int Anno = (int)Year;
                int Mese = (int)Month;

                if (Repository.VerifyParkOpen(Anno, Mese) > 0)
                {
                    DynamicPrices = Repository.GetDynamicCalendar(Anno, Mese);

                    //if empty create
                    if (DynamicPrices.Count == 0)
                    {

                        DynamicPrices = Repository.CreateEmptDynamicPrices(Anno, Mese);

                        Repository.InsertDbCalendar(DynamicPrices);

                    }

                }
            }


            //only the days open and active:
            DynamicPrices = DynamicPrices.Where(x => x.Id1 != "0" && x.TimeEntrance != "0").ToList();

            List<SelectListItem> Years = CreateYears();
            ViewBag.Years = Years;

            List<SelectListItem> Months = CreateMonths();
            ViewBag.Months = Months;

            return View(DynamicPrices);
        }

        public JsonResult UpdateDynamicPrice(string Year, string Month, string Index, string Price1, string Price2)
        {
            string Data = Year + "-" + Impostazioni.Adatta(Month) + "-" + Impostazioni.Adatta(Index);
            Price1 = Price1.Replace(",", ".");
            Price2 = Price2.Replace(",", ".");

            Repository.UpdateDynamicPrice(Data, Price1, Price2);

            return Json("OK");
        }

        public JsonResult VerifyParkOpen(int Year, int Month)
        {
            int Nrec = Repository.VerifyParkOpen(Year, Month);

            if (Nrec == 0)
            {
                return Json("KO");
            }

            return Json("OK");
        }

        private List<SelectListItem> CreateYears()
        {
            string yy = DateTime.Now.Year.ToString();
            string yyn = (DateTime.Now.Year + 1).ToString();

            List<SelectListItem> Years = new List<SelectListItem>
            {
                new SelectListItem{ Text="", Value = "0" },
                new SelectListItem{ Text=yy, Value = yy },
                new SelectListItem{ Text=yyn, Value = yyn }
            };

            return Years;
        }

        private List<SelectListItem> CreateMonths()
        {

            List<SelectListItem> Months = new List<SelectListItem>
            {
                new SelectListItem{ Text="", Value = "0" },
                new SelectListItem{ Text="Gennaio", Value = "1" },
                new SelectListItem{ Text="Febbraio", Value = "2" },
                new SelectListItem{ Text="Marzo", Value = "3"},
                new SelectListItem{ Text="Aprile", Value = "4"},
                new SelectListItem{ Text="Maggio", Value = "5"},
                new SelectListItem{ Text="Giugno", Value = "6"},
                new SelectListItem{ Text="Luglio", Value = "7"},
                new SelectListItem{ Text="Agosto", Value = "8"},
                new SelectListItem{ Text="Settembre", Value = "9"},
                new SelectListItem{ Text="Ottobre", Value = "10"},
                new SelectListItem{ Text="Novembre", Value = "11"},
                new SelectListItem{ Text="Dicembre", Value = "12"}
            };

            return Months;
        }


        public ActionResult Prices(string id = null)
        {
            PricesModel m = new PricesModel();
            if (!string.IsNullOrEmpty(id))
            {
                m.PriceListSelected = m.PriceList.Find(p => p.PriceList_Id.Equals(int.Parse(id)));
                m.PriceListSalesChannel = PriceListSalesChannel.Instance().GetSalesChannelByList(int.Parse(id));
            }
            return View(m);
        }

        public ActionResult Update(string id = null)
        {
            //main products view
            ProductsModel m = ProductsModel;
            if (id != null)
            {
                m.Product = Products.Instance(Global.Lang).GetProduct(int.Parse(id));
            }

            int CurrentParkOperational = Repository.GetCurrentParkOperational();

            ViewBag.CurrentParkOperational = CurrentParkOperational;
            ViewBag.parkOperationalActive = Repository.GetParkOpListAll();

            string HighPrice = "0";
            string LowPrice = "0";
            string HighPriceBase = "0";
            string LowPriceBase = "0";

            if (id != null)
            {
                int PrcId = Repository.GetProductPrices(CurrentParkOperational, Convert.ToInt32(id), ref HighPrice, ref LowPrice, ref HighPriceBase, ref LowPriceBase);
                ViewBag.HighPrice = HighPrice;
                ViewBag.LowPrice = LowPrice;
                ViewBag.HighPriceBase = HighPriceBase;
                ViewBag.LowPriceBase = LowPriceBase;
            }

            return View("~/Views/Products/Product.cshtml", m);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertOrUpdate(Products product, string HighPrice, string LowPrice, string HighPriceBase, string LowPriceBase, int ParkOp)
        {

            if (_InsertOrUpdate(product).Product.Product_Id != 0)
            {
                int Product_Id = product.Product_Id;
                Repository.InsertOrUpdateProductPrices(Product_Id, ParkOp, HighPrice, LowPrice, HighPriceBase, LowPriceBase);
                return RedirectToAction("Update", new { id = product.Product_Id });
            }
            return RedirectToAction("Product");
        }


        [HttpPost]
        public int ChangeStatus(int product_id, bool product_status)
        {
            return Products.Instance(Global.Lang).ChangeStatus(product_id, product_status);
        }

        [HttpPost]
        public int ChangePriceStatus(int price_id, bool price_status)
        {
            return Core.Prices.Instance().ChangeStatus(price_id, price_status);
        }

        [HttpPost]
        public int ChangePriceListStatus(int pricelist_id, bool pricelist_status)
        {
            return PriceList.Instance().ChangeStatus(pricelist_id, pricelist_status);
        }

        [HttpPost]
        public int UpdateHotel(Hotels hotel)
        {
            return Hotels.Instance().InsertOrUpdate(hotel);
        }

        [HttpPost]
        public int AddHotel(Products product, Hotels hotel)
        {
            int product_id = _InsertOrUpdate(product).Product.Product_Id;
            if (product_id != 0)
            {
                hotel.Hotel_IdProduct = product_id;
                return Hotels.Instance().InsertOrUpdate(hotel);
            }
            return 0;
        }

        [HttpPost]
        public string GetAddonsWithProductType(int idProduct, int idType)
        {
            return string.Empty; //CRUD.ToSerializeObject(AddOnsProducts.Instance().GetAddonsWithProductType(idProduct, idType));
        }

        [HttpPost]
        public ActionResult InsertOrUpdateListPrice(PriceList priceList, List<string> selectedSalesChannelList)
        {
            TempData["Action"] = priceList.PriceList_Id == 0 ? "insert" : "update";
            int res = PriceList.Instance().InsertOrUpdate(priceList);
            if (priceList.PriceList_Id == 0) priceList.PriceList_Id = res;
            if (res != 0)
            {
                List<string> SalesChannelSaved = priceList.PriceListSalesChannelList.Select(l => l.PriceListSalesChannel_Channel.ToString()).ToList();
                List<string> SalesChannelRemoved = null;
                if (selectedSalesChannelList != null)
                {
                    SalesChannelRemoved = SalesChannelSaved.Except(selectedSalesChannelList).ToList();
                }
                PriceListSalesChannel.Instance().ManageSalesChannel(priceList.PriceList_Id, selectedSalesChannelList, SalesChannelRemoved);
            }
            TempData["Status"] = (res != 0);
            return RedirectToAction("Prices");
        }

        [HttpPost]
        public int InsertOrUpdatePrice(Prices price)
        {
            return Core.Prices.Instance().InsertOrUpdate(price);
        }

        private ProductsModel _InsertOrUpdate(Products product)
        {
            string option = product.Product_Id == 0 ? "insert" : "update";

            ProductsModel model = ProductsModel;
            int res = Products.Instance(Global.Lang).InsertOrUpdate(product);
            if (product.Product_Id == 0) product.Product_Id = res;

            TempData["Action"] = option;
            TempData["Status"] = (res != 0);
            model.Product = product;
            return model;
        }
        #endregion

        #region ProductsAllotments
        [Route("products/allotments/{id?}")]
        public ActionResult ProductsAllotments(string id = null)
        {
            ProductsAllotmentModel m = new ProductsAllotmentModel();
            if (id != null)
            {
                m.ParkOperationalList = ParkOperational.Instance().GetParkOperationalList(new Hashtable() { { "ParkOperational_Status", true } });
                m.ParkCalendarList = ParkCalendar.Instance().GetParkCaledarActives();
                m.ParkSchedulesList = ParkSchedules.Instance().GetParkSchedules(new Hashtable() { { "ParkSchedule_Status", true } });
                m.ProductsAllotments = ProductsAllotment.Instance().GetProductsAllotments(int.Parse(id));
                m.Product = Products.Instance().GetProduct(int.Parse(id));
            }
            return View(m);
        }

        [HttpPost]
        [Route("products/allotments/manage")]
        public int ManageProductsAllotment(ProductsAllotment productsAllotment)
        {
            return ProductsAllotment.Instance().InsertOrUpdate(productsAllotment);
        }

        //public ActionResult HotelsAllotments(string id = null)
        [Route("products/hotels/allotments/{data?}")]
        public ActionResult HotelsAllotments(FormCollection data = null)
        {
            HotelAllotmentModel m = new HotelAllotmentModel();
            List<int> IDProductsHotels = m.Hotels.Select(hotel => hotel.Hotel_IdProduct).ToList();
            m.Prices = Core.Prices.Instance().GetProductsPrices().FindAll(price => IDProductsHotels.Contains(price.Price_IdProduct));
            int CurrentParkOperational = ParkCalendar.Instance().GetParkCaledarActives().OrderBy(i => i.ParkCalendar_CreationDate).FirstOrDefault().ParkCalendar_IdOperational;
            m.ParkOperationalList = ParkOperational.Instance().GetParkOperationalList(new Hashtable() { { "ParkOperational_Status", true }, { "ParkOperational_Id", CurrentParkOperational } }); //ParkOperational_Id
            m.ParkCalendarList = ParkCalendar.Instance().GetParkCalendar(new Hashtable() { { "ParkCalendar_IdOperational", CurrentParkOperational } });//GetParkCaledarActives();
            m.ParkSchedulesList = ParkSchedules.Instance().GetParkSchedules(new Hashtable() { { "ParkSchedule_Status", true } });

            List<SelectListItem> Months = CreateMonths();
            ViewBag.Months = Months;

            return View(m);
        }

        [HttpPost]
        [Route("products/hotels/GetAllotmentsJson")]
        public JsonResult GetAllotmentsJson(string allData)
        {
            string DataSt = Impostazioni.StartCalendarDate;

            DateTime Dt = Impostazioni.ConvertiDataDateFormatTwo(DataSt);


            List<SqlParameter> parameters = CRUD.GetSqlParameters(allData);
            if (parameters != null && parameters.Count > 0)
            {
                string mm = parameters[2].Value.ToString();

                if (mm != "0")
                {
                    string Month = DateTime.Now.Year.ToString() + "-" + Impostazioni.Adatta(mm) + "-01";

                    parameters[2].Value = (object)Month;
                }
                else
                {
                    parameters[2].Value = null;
                }


                List<HotelAllotments> List = HotelAllotments.Instance().GetHotelAllotments(parameters);
                List = List.Where(x => x.HotelAllotment_Date >= Dt).ToList();

                return Json(List, JsonRequestBehavior.AllowGet);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("products/hotels/manage")]
        public int ManageHotelsAllotment(HotelAllotments hotelAllotment)
        {
            //hotelAllotment.HotelAllotment_RoomType = Core.HotelRoomsTypes.Instance().GetHotelRoomType(int.Parse(hotelAllotment.HotelAllotment_RoomType)).HotelRoomType_Identifier;
            return Core.HotelAllotments.Instance().InsertOrUpdate(hotelAllotment);
        }
        #endregion

        #region ProductInfo
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult InsertOrUpdateInfo(ProductsInfo productInfo)
        {
            if (_InsertOrUpdateInfo(productInfo).ProductInfo.ProductInfo_Id != 0)
            {
                return Json(productInfo, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public PartialViewResult UpdateInfo(Products product, string language = null)
        {
            ProductsInfoModel m = ProductsInfoModel;
            m.Product = product;
            m.ProductInfo = ProductsInfo.Instance().GetProductInfo(product.Product_Id, language ?? Global.Lang);
            if (m.ProductInfo == null)
            {
                m.ProductInfo = new ProductsInfo
                {
                    ProductInfo_IdProduct = product.Product_Id,
                    ProductInfo_LanguageCode = language
                };
                m.ProductsImage = ProductsImage.Instance().GetProductsImage(m.ProductInfo.ProductInfo_Id);
                m.ProductsAllotments = ProductsAllotment.Instance().GetProductsAllotments(m.Product.Product_Id);
                m.ProductsSchedules = ProductsSchedules.Instance().GetProductsSchedules(m.Product.Product_Id);
            }
            else
            {
                m.ProductsImage = ProductsImage.Instance().GetProductsImage(m.ProductInfo.ProductInfo_Id);
                m.ProductsAllotments = ProductsAllotment.Instance().GetProductsAllotments(m.Product.Product_Id);
                m.ProductsSchedules = ProductsSchedules.Instance().GetProductsSchedules(m.Product.Product_Id);
            }
                

            m.Hotel = Hotels.Instance().GetHotel(product.Product_Id);
            if (m.Hotel == null)
            {
                m.Hotel = new Hotels
                {
                    Hotel_IdProduct = product.Product_Id
                };
            }

            //park op active in select list item
            List<ParkOperational> Popen = m.ParkOperationals.Where(x => x.ParkOperational_Status == true).ToList();
            ViewBag.Popen = Popen.Select(i => new SelectListItem() { Text = i.ParkOperational_Code, Value = i.ParkOperational_Id.ToString() });

            ViewBag.Folder = "products";
            return PartialView("~/Views/Products/ProductData/_ProductInfo.cshtml", m);
        }

        private ProductsInfoModel _InsertOrUpdateInfo(ProductsInfo productInfo)
        {
            string option = productInfo.ProductInfo_Id == 0 ? "insert" : "update";

            ProductsInfoModel model = ProductsInfoModel;
            int res = ProductsInfo.Instance().InsertOrUpdate(productInfo);
            if (productInfo.ProductInfo_Id == 0) productInfo.ProductInfo_Id = res;

            model.ProductInfo = productInfo;
            return model;
        }
        #endregion

        #region ProductImage
        public JsonResult ImageDelete(int Id)
        {
            Repository.DeleteProductImage(Id);
            return Json("");
        }


        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetProductsImage(int? productinfo_id = null)
        {
            var list = ProductsImage.Instance().GetProductsImage(productinfo_id);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertOrUpdateImage(ProductsImage productImage, HttpPostedFileBase file)
        {
            if (_InsertOrUpdateImage(productImage).ProductImage.ProductImage_Id != 0)
            {
                return RedirectToAction("Update", new { id = productImage.ProductImage_Id });
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public int ChangeStatusImage(int productimage_id, bool productimage_status)
        {
            return ProductsImage.Instance().ChangeStatus(productimage_id, productimage_status);
        }

        [HttpPost]
        public int ChangeTypesImage(int productimage_id, string productimage_type)
        {
            return ProductsImage.Instance().ChangeType(productimage_id, productimage_type);
        }

        private ProductsImageModel _InsertOrUpdateImage(ProductsImage productImage)
        {
            string option = productImage.ProductImage_Id == 0 ? "insert" : "update";

            ProductsImageModel model = ProductsImageModel;
            int res = ProductsImage.Instance().InsertOrUpdate(productImage);
            if (productImage.ProductImage_Id == 0) productImage.ProductImage_Id = res;
            model.ProductImage = productImage;
            return model;
        }
        #endregion

        #region ProductSchedules
        [HttpPost]
        public JsonResult GetProductSchedulesJson(int product_Id)
        {
            var list = ProductsSchedules.Instance().GetProductsSchedules(product_Id);
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult VerifyProductSchedules(int ProdId, int PsId, int AsId)
        {
            bool Found = Repository.VerifyProductSchedules(ProdId, PsId, AsId);

            if (Found == true)
            {
                return Json("KO");
            }

            return Json("OK");
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult InsertOrUpdateSchedule(ProductsSchedules productSchedule)
        {
            if (_InsertOrUpdateInfo(productSchedule).ProductSchedule.Id != 0)
            {
                int DefAct = 0;
                if (!string.IsNullOrEmpty(Request.Form["ActivitySchedule_Id_Default"]))
                {
                    DefAct = Convert.ToInt32(Request.Form["ActivitySchedule_Id_Default"]);
                }

                List<ProductsAllotment> allotments = ProductsAllotment.Instance().CreateAllotments(productSchedule.ParkOperational_Id, productSchedule.ParkSchedule_Id, productSchedule.Product_Id, productSchedule.ActivitySchedule_Id, DefAct);
                return Json(new { ProductsSchedules = productSchedule, ProductsAllotment = (allotments.Count > 0 ? allotments : default(List<ProductsAllotment>)) }, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        private ProductsInfoModel _InsertOrUpdateInfo(ProductsSchedules productSchedule)
        {
            string option = productSchedule.Id == 0 ? "insert" : "update";

            ProductsInfoModel model = ProductsInfoModel;
            List<ProductsSchedules> findByProduct = ProductsSchedules.Instance().GetProductsSchedules().FindAll(i => i.Product_Id == productSchedule.Product_Id);
            List<ProductsSchedules> findByOperational = (findByProduct.Count > 0) ? findByProduct.FindAll(i => i.ParkOperational_Id == productSchedule.ParkOperational_Id) : new List<ProductsSchedules>();
            int? _cPosition = (findByOperational.Count > 0? findByOperational.Max(i => i.Position) : default(int));//.FindAll(i => i.Product_Id == productSchedule.Product_Id && i.ParkOperational_Id == productSchedule.ParkOperational_Id).Max(i => i.Position);
            if (productSchedule.Position == 0) { productSchedule.Position = (_cPosition.HasValue ? _cPosition.Value : 0) + 1; }
            int res = ProductsSchedules.Instance().InsertOrUpdate(productSchedule);
            if (productSchedule.Id == 0) productSchedule.Id = res;

            model.ProductSchedule = productSchedule;
            return model;
        }
        #endregion

        #region UploadImage
        [HttpPost]
        public JsonResult UploadFile(ProductsImage model, HttpPostedFileBase file) //String folder, ProductsImage model, HttpPostedFileBase file
        {
            var result = new { status = false, message = string.Empty, type = string.Empty };

            if (file == null || file.ContentLength == 0)
                return Json(new { status = false, message = string.Empty, type = "error" }, JsonRequestBehavior.AllowGet);

            var type_product = Products.Instance().GetProduct(ProductsInfo.Instance().GetProductInfo(model.ProductImage_IdProductInfo).ProductInfo_IdProduct).Product_Key.ToLower().Replace(' ', '-');

            string folderImage = model.ProductImage_Url;  // type_product + "/";
            string fileName = file.FileName;
            string pathfile = Path.Combine(Server.MapPath("~/" + folderImage), Path.GetFileName(fileName));

            FileUpload fu = new FileUpload();

            model.ProductImage_Url = folderImage + model.Product_ImageType.ImageType_Identifier.ToLower().Replace(' ', '-') + "/" + fileName;

            if (_InsertOrUpdateImage(model).ProductImage.ProductImage_Id != 0)
            {
                fu.Upload(file, new Files { Name = file.FileName, Folder = model.Product_ImageType.ImageType_Identifier.ToLower().Replace(' ', '-'), Path = folderImage });
                return Json(new { status = true, message = "Tu registro se completado con éxito.", type = "success" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = false, message = string.Empty, type = "error" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region HotelInfo
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HotelInfo(Products product)
        {
            HotelsModel m = HotelsModel;
            m.Product = product;
            m.Hotel = Hotels.Instance().GetHotel(null, product.Product_Id);
            if (m.Hotel == null)
            {
                m.Hotel = new Hotels
                {
                    Hotel_IdProduct = product.Product_Id
                };
            }

            return PartialView("~/Views/Products/ProductData/_Hotel.cshtml", m);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertOrUpdateHotel(Hotels hotel)
        {
            int[] services = Request.Form["Hotel_ServicesNew"].Split(',').Select(int.Parse).ToArray();
            if (_InsertOrUpdateHotel(hotel).Hotel.Hotel_Id != 0)
            {
                foreach (int Service_Id in services)
                {
                    if (!hotel.Hotel_Services.Contains(Service_Id))
                    {
                        Core.HotelServices.Instance().Insert(new Core.HotelServices()
                        {
                            HotelService_IdHotel = hotel.Hotel_Id,
                            HotelService_IdCatService = Service_Id
                        });
                    }
                }

                foreach (int Service_Id in hotel.Hotel_Services)
                {
                    if (!services.Contains(Service_Id))
                    {
                        Core.HotelServices.Instance().Delete(new Core.HotelServices()
                        {
                            HotelService_IdHotel = hotel.Hotel_Id,
                            HotelService_IdCatService = Service_Id
                        });
                    }
                }

                return Json(hotel, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        private HotelsModel _InsertOrUpdateHotel(Hotels hotel)
        {
            string option = hotel.Hotel_Id == 0 ? "insert" : "update";

            HotelsModel model = HotelsModel;
            int res = Hotels.Instance().InsertOrUpdate(hotel);
            if (hotel.Hotel_Id == 0) hotel.Hotel_Id = res;

            model.Hotel = hotel;
            return model;
        }

        #endregion
    }
}