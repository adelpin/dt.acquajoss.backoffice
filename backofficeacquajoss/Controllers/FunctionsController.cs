﻿using BackOfficeAcquaJoss.Models;
using Core;
using Core.Db;
using Core.Properties;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class FunctionsController : Controller
    {
        public Users Usuario = Users.Instance();
        private static FunctionsController _instance = null;
        private string conexion = Settings.Default.DT_BackOffice;

        public static FunctionsController instance()
        {
            if (_instance == null)
            {
                _instance = new FunctionsController();
                JsonConvert.DefaultSettings = () => new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented,
                    Error = (serializer, err) =>
                    {
                        err.ErrorContext.Handled = true;
                    }
                };
            }
            return _instance;
        }

        public string GetClientData(string cliente)
        {
            return Customers.GetClients(cliente);
        }

        public string GetSchoolData(string school, string city)
        {
            return Customers.GetSchools(school, city);
        }

        public string GetHotelData(int idProduct) {
            return JsonConvert.SerializeObject(Hotels.Instance().GetHotel(idProduct));
        }

        [HttpPost]
        public PartialViewResult AddBitacoraComent(ReservationLogs log)
        {
            try
            {
                ReservationLogs.Instance().InsertOrUpdate(log);
                ReservationModel model = new ReservationModel(log.Reservation_Id);
                return PartialView("~/Views/Shared/Reservations/_SummaryLogItems.cshtml", model);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpPost]
        public int Sortable(string table, string keyId, string keyValue, int[] data)
        {
            int res = 0;
            int i = 1;
            foreach(int id in data)
            {
                res = CRUD.Update(conexion, table, new Hashtable() {
                    { keyValue, i++ }
                }, new Hashtable() {
                    { keyId, id }
                });
            }
            return res;
        }
    }
}