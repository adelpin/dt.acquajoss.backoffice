﻿using BackOfficeAcquaJoss.Models;
using Core;
using Core.Db;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PromotionsModel = BackOfficeAcquaJoss.Models.PromotionsModel;

namespace BackOfficeAcquaJoss.Controllers
{
    public class PromotionsController : Controller
    {
        #region Banners
        private SlidersModel SlidersModel
        {
            get
            {                
                SlidersModel m = new SlidersModel
                {
                    Slider = null,
                    Sliders = Sliders.Instance().GetSliders(),
                };
                return m;
            }
        }

        public ActionResult Banners()
        {
            return View(SlidersModel);
        }

        public ActionResult BannersUpdate(string id = null)
        {
            SlidersModel m = SlidersModel;
            if (id != null)
            {
                m.Slider = Sliders.Instance().GetSlider(int.Parse(id));
            }
            return View("~/Views/Promotions/Banners.cshtml", m);
        }

        public JsonResult BannersRecordDelete(string id)
        {
            Repository.DeleteSlider(id);
            return Json("");
        }


        [HttpPost]
        public ActionResult BannersInsertOrUpdate(Sliders slider)
        {
            if (_InsertOrUpdateImage(slider).Slider.Slider_Id != 0)
            {
                return Json(slider);
            }
            return Json(null);
        }

        [HttpPost]
        public int BannersChangeStatus(int slider_id, bool slider_status)
        {
            return Sliders.Instance().ChangeStatus(slider_id, slider_status);
        }

        [HttpPost]
        public int BannersChangeStatusCountdown(int slider_id, bool slider_status)
        {
            return Sliders.Instance().ChangeStatusCountdown(slider_id, slider_status);
        }

        private SlidersModel _InsertOrUpdateImage(Sliders sliderImage)
        {
            string option = sliderImage.Slider_Id == 0 ? "insert" : "update";

            SlidersModel model = SlidersModel;
            int res = Sliders.Instance().InsertOrUpdate(sliderImage);
            if (sliderImage.Slider_Id == 0) sliderImage.Slider_Id = res;
            model.Slider = sliderImage;
            return model;
        }
        #endregion

        #region Promotions
        private PromotionsModel PromotionsModel
        {
            get
            {
                PromotionsModel m = new PromotionsModel
                {
                    Promotion = null,
                    Promotions = Promotions.Instance().GetPromotions(),
                };
                return m;
            }
        }

        public ActionResult Deals()
        {
            Session["savedeal"] = "";
            Session["AddonId"] = null;
            List<Products> Lista = GetAddonProducts();

            ViewBag.Products = Lista;
            ViewBag.AddonId = 0;
            ViewBag.Operation = "";
            return View(PromotionsModel);
        }

        public JsonResult ChangeProd(string Code, int AddonId)
        {

            //Repository.SetDiscountCouponProduct(Code, AddonId);

            Session["AddonId"] = AddonId;
            return Json("");
        }


        public JsonResult DealsRecordDelete(string Id)
        {
            Repository.DeleteDeals(Id);
            return Json("");
        }

        public ActionResult DealsSelect(string id = null)
        {

            PromotionsModel m = PromotionsModel;
            List<Products> Lista = GetAddonProducts();
            int AddonId = 0;

            if (Session["savedeal"].ToString() != "on")
            {

                //list of extra addon                           
                if (id != null)
                {
                    m.Promotion = Promotions.Instance().GetPromotion(int.Parse(id));

                    string Code = m.Promotion.PromotionDeal_Code;

                    AddonId = Repository.GetAddonIdByDiscountCode(Code);
                    Session["AddonId"] = AddonId;
                }
            }
            else
            {

                Session["savedeal"] = "";
                Session["AddonId"] = null;
            }


            ViewBag.Products = Lista;
            ViewBag.AddonId = AddonId;



            return View("~/Views/Promotions/Deals.cshtml", m);
        }


        private List<Products> GetAddonProducts()
        {
            List<Products> Lista = PromotionsModel.Products.Where(x => x.Product_Type.Equals("extra") && x.Product_Status == true).ToList();
            return Lista;
        }

        [HttpPost]
        public ActionResult DealsInsertOrUpdate(Promotions Promotion, int AddonId = 0)
        {
            Repository.SetDiscountCouponProduct(Promotion.PromotionDeal_Code, AddonId);

            DateTime dd = Promotion.PromotionDeal_DateStart;
            dd = dd.Date;

            if (_InsertOrUpdateDeals(Promotion).Promotion.PromotionDeal_Id != 0)
            {
                return Json(Promotion);
            }



            return Json(null);
        }

        [ValidateInput(false)]
        public void InsertOrUpdateDeal(Promotions promotion)
        {
            //no timespan
            DateTime DtEvent = (DateTime)promotion.PromotionDeal_DateEnd;
            DtEvent = DtEvent.Date;
            promotion.PromotionDeal_DateEnd = DtEvent;

            Session["savedeal"] = "on";

            int AddonId = 0;

            if (Session["AddonId"] != null)
            {
                AddonId = (int)Session["AddonId"];
            }

            bool success = false;
            bool newDeal = promotion.PromotionDeal_Id == 0;

            try
            {
                string Code = promotion.PromotionDeal_Code;
                Repository.SetDiscountCouponProduct(Code, AddonId);

                if (AddonId != 0)
                {
                    Repository.DeleteDiscountCouponProduct(Code, AddonId);
                    Repository.SetDiscountCouponProduct(Code, AddonId);


                    //delete old, if present
                    Repository.DeleteProductCalendar(AddonId);

                    ProductsCalendar.Instance().InsertOrUpdate(new ProductsCalendar()
                    {
                        Product_Id = AddonId,
                        Product_Date = DtEvent,
                        Product_EventExclude = null
                    });

                }


                string rootPath = "Content/images/promotions/"; // + (promotion.PromotionDeal_Code.ToLower().Replace(" ", "-"))
                if (Request.Files.Count > 0)
                {
                    foreach (string fileKey in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[fileKey];
                        string pathFile = fileKey.Contains("Thumbnail") ? "thumbnail" : "shopcart";

                        //promotion.GetType().GetProperty(fileKey).SetValue(promotion, rootPath + "/" + file.FileName, null);

                        success = (new FileUpload().Upload(file, new Files()
                        {
                            Name = file.FileName,
                            Size = file.ContentLength,
                            Folder = rootPath + pathFile,
                        }) == 1);
                    }
                }

                if (promotion.PromotionDeal_Order == 0)
                    promotion.PromotionDeal_Order = Promotions.Instance().GetPromotions().Select(i => i.PromotionDeal_Order).Max() + 1;

                int response = Promotions.Instance().InsertOrUpdate(promotion);
                success = (response != 0);
                promotion.PromotionDeal_Id = promotion.PromotionDeal_Id == 0 ? response : promotion.PromotionDeal_Id;

                if (Request.Files.Count > 0)
                {
                    foreach (string fileKey in Request.Files)
                    {
                        PromotionsImage.Instance().DeleteByPromotionDeal(promotion.PromotionDeal_Id, fileKey.Replace('_', ' '));

                        HttpPostedFileBase file = Request.Files[fileKey];
                        string pathFile = fileKey.Contains("Thumbnail") ? "thumbnail" : "shopcart";
                        string image = "/" + rootPath + pathFile + "/" + file.FileName;
                        PromotionsImage.Instance().InsertOrUpdate(new PromotionsImage()
                        {
                            PromotionImage_IdPromotionDeal = promotion.PromotionDeal_Id,
                            PromotionImage_Url = image,
                            PromotionImage_ImageType = fileKey.Replace('_', ' '),
                            PromotionImage_Name = promotion.PromotionDeal_Title,
                            PromotionImage_Status = true
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
                Console.WriteLine(ex.Message);
            }

            TempData["Action"] = newDeal ? "insert" : "update";
            TempData["Status"] = success;
        }

        [HttpPost]
        public int DealsChangeStatus(int promotion_Id, bool? promotion_Status = null, bool? promotion_Pack = null)
        {
            return Promotions.Instance().ChangeStatus(promotion_Id, promotion_Status, promotion_Pack);
        }

        private PromotionsModel _InsertOrUpdateDeals(Promotions promotionImage)
        {
            string option = promotionImage.PromotionDeal_Id == 0 ? "insert" : "update";

            PromotionsModel model = PromotionsModel;
            int res = Promotions.Instance().InsertOrUpdate(promotionImage);
            if (promotionImage.PromotionDeal_Id == 0) promotionImage.PromotionDeal_Id = res;
            model.Promotion = promotionImage;
            return model;
        }

        #endregion

        #region Discounts

        public JsonResult DiscountsRecordDelete(string Id)
        {
            string msg = "";

            int Ntck = Repository.NticketsDiscount(Id);

            if (Ntck > 0)
            {
                msg = "in uso su: " + Ntck + " biglietti ";
            }

            int Npro = Repository.NpromoDiscount(Id);

            if (Npro > 0)
            {
                msg += "in uso su: " + Npro + " promozioni ";
            }



            int Nres = Repository.NreservationsDiscount(Id);

            if (Nres > 0)
            {
                msg += "in uso su: " + Nres + " acquisti ";
            }

            Repository.DeleteDiscount(Id);
            return Json(msg);
        }
        private DiscountCouponModel DiscountCouponModel
        {
            set { DiscountCouponModel = value;  }
            get
            {
                DiscountCoupon c = new DiscountCoupon();
                DiscountCouponModel m = new DiscountCouponModel
                {
                    DiscountCoupons = DiscountCoupon.Instance().GetDiscounts(),
                };
                return m;
            }
        }

        public ActionResult Discounts(int ProdSel = 0)
        {
            DiscountCouponModel Model = new DiscountCouponModel() { DiscountCoupons = DiscountCoupon.Instance().GetDiscounts() };
            ViewBag.ProdSel = ProdSel;
            return View(Model);
        }


        public JsonResult UpdateDiscount(int Id, string Identifier, int Product, int Ticket,
            string Channel, int Genere, string Value, string DateStart, string DateEnd,
            int AnticipationDays, int Score, bool Status)
        {

            Repository.UpdateDiscount(Id, Identifier, Product, Ticket,
            Channel, Genere, Value, DateStart, DateEnd,
            AnticipationDays, Score, Status);

            return Json("");
        }


        public JsonResult InsertDiscount(string Identifier, int Product, int Ticket,
            string Channel, int Genere, string Value, string DateStart, string DateEnd,
            int AnticipationDays, int Score, bool Status)
        {

            Repository.InsertDiscount(Identifier, Product, Ticket,
            Channel, Genere, Value, DateStart, DateEnd,
            AnticipationDays, Score, Status);

            return Json("");
        }

        [HttpPost]
        public int DiscountsChangeStatus(int discountcoupon_id, bool discountcoupon_status)
        {
            return DiscountCoupon.Instance().ChangeStatus(discountcoupon_id, discountcoupon_status);
        }

        public PartialViewResult DiscountsList(DiscountCoupon discountCoupon)
        {
            DiscountCouponModel m = new DiscountCouponModel() { DiscountCoupons = DiscountCoupon.Instance().GetDiscounts(discountCoupon) };
            return PartialView("~/Views/Promotions/Discounts/_Rows.cshtml", m);
        }

        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult InsertOrUpdateDiscount(DiscountCoupon discountCoupon, DiscountCoupon lastDiscountCouponSearch)
        {
            if (_InsertOrUpdateDiscounts(discountCoupon).DiscountCoupon.DiscountCoupon_Id != 0)
            {
                return RedirectToAction("DiscountsList", lastDiscountCouponSearch);
            }
            return null;
        }

        private DiscountCouponModel _InsertOrUpdateDiscounts(DiscountCoupon discountCoupon)
        {
            string option = discountCoupon.DiscountCoupon_Id == 0 ? "insert" : "update";

            DiscountCouponModel model = DiscountCouponModel;
            int res = DiscountCoupon.Instance().InsertOrUpdate(discountCoupon);
            if (discountCoupon.DiscountCoupon_Id == 0) discountCoupon.DiscountCoupon_Id = res;

            TempData["Action"] = option;
            TempData["Status"] = (res != 0);
            model.DiscountCoupon = discountCoupon;
            return model;
        }
        #endregion

        #region UploadImage
        [HttpPost]
        public JsonResult UploadFile(Sliders model, HttpPostedFileBase file_desktop, HttpPostedFileBase file_mobile)
        {
            var result = new { status = false, message = string.Empty, type = string.Empty };
            string folderImage = "Content/images/sliders/";

            if (file_desktop != null)
            {
                model.Slider_Image = folderImage + model.Slider_Image_Type.ImageType_Identifier.ToLower().Replace(' ', '-') + "/" + file_desktop.FileName;
                FileUpload fu = new FileUpload();
                fu.Upload(file_desktop, data: new Files() { Name = file_desktop.FileName, Folder = model.Slider_Image_Type.ImageType_Identifier.ToLower().Replace(' ', '-'), Path = folderImage });
            }

            if (file_mobile != null)
            {
                model.Slider_Image_Mobile = folderImage + model.Slider_Image_Type.ImageType_Identifier.ToLower().Replace(' ', '-') + "-mobile/" + file_mobile.FileName;
                FileUpload fu = new FileUpload();
                fu.Upload(file_mobile, data: new Files() { Name = file_mobile.FileName, Folder = model.Slider_Image_Type.ImageType_Identifier.ToLower().Replace(' ', '-') + "-mobile", Path = folderImage });
            }

            if (_InsertOrUpdateImage(model).Slider.Slider_Id != 0)
            {
                return Json(new { status = true, message = "Tu registro se completado con éxito.", type = "success" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = false, message = string.Empty, type = "error" }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}