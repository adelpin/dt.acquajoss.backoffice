﻿using BackOfficeAcquaJoss.Models;
using Core;
using Core.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class ParkController : Controller
    {
        // GET: Park
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Schedules(string id = null)
        {
            ParkSchedules ps = new ParkSchedules();
            ParkSchedulesModel model = new ParkSchedulesModel();
            model.ParkSchedule = string.IsNullOrEmpty(id) ? ps : ps.GetParkSchedule(int.Parse(id));
            model.ParkSchedules = ps.GetParkSchedules();
            return View(model);
        }

        [HttpPost]
        public ActionResult SchedulesInsertOrUpdate(ParkSchedules parkSchedule)
        {
            string option = parkSchedule.ParkSchedule_Id == 0 ? "insert" : "update";
            int res = ParkSchedules.Instance().InsertOrUpdate(parkSchedule);
            if (parkSchedule.ParkSchedule_Id == 0) parkSchedule.ParkSchedule_Id = res;
            TempData["Action"] = option;
            TempData["Status"] = (res != 0);
            return RedirectToAction("schedules", new { id = parkSchedule.ParkSchedule_Id });
        }

        public ActionResult Calendar(string id = null)
        {
            return CalendarView(id, "calendar");
        }

        //For calendar
        public ActionResult Schedule(string id = null)
        {
            return CalendarView(id, "schedule");
        }

        public ActionResult CalendarView(string id, string action)
        {
            ParkOperational p = ParkOperational.Instance();
            ParkCalendarModel m = new ParkCalendarModel();
            m.action = action;
            m.ParkOperational = string.IsNullOrEmpty(id) ? new ParkOperational() : p.GetParkOperational(int.Parse(id));
            m.ParkOperationalList = p.GetParkOperationalList();
            m.CalendarList = string.IsNullOrEmpty(id) ? null : ParkCalendar.Instance().GetParkCalendar(m.ParkOperational.ParkOperational_Id);
            return View("~/Views/Park/Calendar.cshtml", m);
        }

        public ActionResult CalendarSchedules(string idOperational, string schedule, List<DateTime> dates)
        {
            if (!string.IsNullOrEmpty(idOperational) && !string.IsNullOrEmpty(schedule) && dates != null)
            {
                foreach (DateTime date in dates)
                {
                    ParkCalendar parkCalendar = ParkCalendar.Instance().GetParkCalendar(int.Parse(idOperational), date);
                    ParkCalendarSchedule parkSchedule = ParkCalendarSchedule.Instance().GetParkCalendarSchedule(parkCalendar.ParkCalendar_Id, int.Parse(schedule));

                    ParkCalendarSchedule.Instance().InsertOrUpdate(new ParkCalendarSchedule()
                    {
                        ParkCalendarSchedule_IdParkCalendar = parkCalendar.ParkCalendar_Id,
                        ParkCalendarSchedule_IdParkSchedule = int.Parse(schedule),
                        ParkCalendarSchedule_Status = true
                    });

                }
                ParkCalendarSchedule.Destroy();
            }
            return RedirectToAction("Schedule", new { id = idOperational });
        }


        public ActionResult Seasons(string id = null)
        {
            ParkOperational p = ParkOperational.Instance();
            ParkSeasons s = ParkSeasons.Instance();
            ParkSeasonsModel m = new ParkSeasonsModel();
            m.ParkOperational = string.IsNullOrEmpty(id) ? new ParkOperational() : p.GetParkOperational(int.Parse(id));
            m.ParkOperationalList = p.GetParkOperationalList();
            m.ParkSeasonsList = s.GetParkSeasonsList(m.ParkOperational.ParkOperational_Id);
            m.Calendar = string.IsNullOrEmpty(id) ? null : ParkCalendar.Instance().GetParkCalendar(m.ParkOperational.ParkOperational_Id);
            return View(m);
        }

        [HttpPost]
        public int ManageCalendar(int idOperational, List<DateTime> datesAdded, List<DateTime> datesRemoved){
            return ParkCalendar.Instance().ManageCalendar(idOperational, datesAdded, datesRemoved);
        }

        [HttpPost]
        public ActionResult InsertOrUpdateOperational(ParkOperational parkOperational)
        {
            string option = parkOperational.ParkOperational_Id == 0 ? "insert" : "update";
            int res = ParkOperational.Instance().InsertOrUpdate(parkOperational);
            if (option.Equals("insert")) parkOperational.ParkOperational_Id = res;
            TempData["Action"] = option;
            TempData["Status"] = (res != 0);
            return RedirectToAction("calendar", new { id = parkOperational.ParkOperational_Id });
        }

        [HttpPost]
        public int ChangeStatusCalendar(int idOperational, bool status)
        {
            return ParkOperational.Instance().ChangeStatus(idOperational, status);
        }
        [HttpPost]
        public int ChangeStatusSchedule(int idSchedule, bool status)
        {
            return ParkSchedules.Instance().ChangeStatus(idSchedule, status);
        }

        [HttpPost]
        public int ChangeStatusCalendarSchedule(int idParkCalendarSchedule, bool status)
        {
            return ParkCalendarSchedule.Instance().ChangeStatus(idParkCalendarSchedule, status);
        }

        [HttpPost]
        public int ManageSeason(ParkSeasons parkSeason)
        {
            return ParkSeasons.Instance().InsertOrUpdate(parkSeason);
        }
    }
}