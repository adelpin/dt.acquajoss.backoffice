﻿using BackOfficeAcquaJoss.Models;
using Core;
using Core.Db;
using Core.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Controllers
{
    public class SeoController : Controller
    {
        // GET: Seo
        public ActionResult Index()
        {
            return View(new SeoModel());
        }
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertOrUpdate(Seo seo)
        {
            string option = seo.Seo_Id == 0 ? "insert" : "update";
            
            int res = Seo.Instance().InsertOrUpdate(seo);
            if (seo.Seo_Id == 0) seo.Seo_Id = res;

            TempData["Action"] = option;
            TempData["Status"] = (res != 0);

            return RedirectToAction("Index");
        }
        
        public ActionResult Emails()
        {
            return View(new SeoEmailsModel());
        }


        [HttpPost]
        public PartialViewResult FilterEmails(string type, string allData)
        {
            try
            {
                List<SqlParameter> parameters = CRUD.GetSqlParameters(allData);
                parameters.Add(new SqlParameter("type", type));
                string json = CRUD.ExecuteCommandQuery(Settings.Default.DT_BackOffice, "spBackOfficeGetSeoEmails", parameters);
                return PartialView("~/Views/Seo/_EmailsResult.cshtml", new Hashtable() {
                    { "type", type },
                    { "data", json }
                });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}