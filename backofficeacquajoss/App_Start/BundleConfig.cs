﻿using Core;
using System.Web.Optimization;

namespace BackOfficeAcquaJoss.App_Start
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = GlobalSettings.EnProduccion;
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-3.2.1.min.js",
                "~/Scripts/jquery.metisMenu.min.js",
                "~/Scripts/jquery.serializejson.min.js",
                "~/Scripts/jquery.redirect.min.js",
                "~/Scripts/jquery.numeric.min.js",
                "~/Scripts/jquery.slimscroll.min.js",
                "~/Scripts/jquery.nestable.js",
                "~/Scripts/jquery-ui-1.12.1.min.js",
                "~/Scripts/jquery-scrolltofixed-min.js",
                "~/Scripts/toastr.min.js",
                "~/Scripts/sweetalert.min.js",
                "~/Scripts/chosen.jquery.js",
                "~/Scripts/moment-with-locales.min.js",
                "~/Scripts/accounting.min.js",
                "~/Scripts/switchery.js",
                "~/Scripts/clockpicker.js",
                "~/Scripts/dropzone.js",
                "~/Scripts/mixitup.min.js",
                "~/Scripts/footable.min.js",
                "~/Scripts/slick.min.js",
                "~/Scripts/inspinia.js"));

            bundles.Add(new ScriptBundle("~/bundles/ckeditor").Include(
               "~/Scripts/ckeditor/ckeditor.min.js",
               "~/Scripts/ckeditor/ckeditor-jquery-adapter.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/validator.js",
                "~/Scripts/jquery.validate.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/functions").Include(
                "~/Scripts/clipboard.min.js",
                "~/Scripts/icheck.min.js",
                "~/Scripts/functions.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/bootstrap-dialog.min.js",
                "~/Scripts/bootstrap-confirmation.min.js",
                "~/Scripts/bootstrap-show-password.js",
                "~/Scripts/bootstrap-datetimepicker.js",
                "~/Scripts/bootstrap-colorpicker.min.js",
                "~/Scripts/bootstrap-year-calendar.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/animate.css",
                "~/Content/bootstrap.min.css",
                "~/Content/bootstrap-dialog.min.css",
                "~/Content/bootstrap-datetimepicker.min.css",
                "~/Content/bootstrap-colorpicker.min.css",
                "~/Content/bootstrap-year-calendar.min.css",
                "~/Content/font-awesome.min.css",
                "~/Content/toastr.min.css",
                "~/Content/sweetalert.css",
                "~/Content/flag-icon.min.css",
                "~/Content/footable.bootstrap.min.css",
                "~/Content/switchery.css",
                "~/Content/clockpicker.css",
                "~/Content/dropzone.css",
                "~/Content/chosen/bootstrap-chosen.css",
                "~/Content/themes/base/jquery-ui.css",
                "~/Content/slick.css",
                "~/Content/slick-theme.css",
                "~/Content/utilities.css",
                "~/Content/iCheck/custom.css",
                "~/Content/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"));

            bundles.Add(new StyleBundle("~/Content/tables").Include(
                "~/Content/dataTables/dataTables.bootstrap.css",
                "~/Content/dataTables/dataTables.responsive.css",
                "~/Content/dataTables/dataTables.tableTools.css",
                "~/Content/dataTables/buttons.dataTables.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                "~/Scripts/dataTables/jquery.dataTables.js",
                "~/Scripts/dataTables/dataTables.bootstrap.js",
                "~/Scripts/dataTables/dataTables.responsive.js",
                "~/Scripts/dataTables/dataTables.buttons.min.js",
                "~/Scripts/dataTables/jszip.min.js",
                "~/Scripts/dataTables/buttons.html5.min.js"));

            bundles.Add(new StyleBundle("~/Content/theme").Include(
                "~/Content/Inspinia/style.css",
                "~/Content/site.css"));
        }
    }
}