﻿var _bp = null;
$(window).on("load resize", function (e) {
    var bp = breakpoint();
    if (bp != _bp || _bp == null) {
        $("body").attr("size", bp);
        $(document).trigger("breakpoint", [{
            breakpoint: bp,
            isMobile: bp == "xs"
        }]);

        $('.make_static').trigger('detach.ScrollToFixed');
        if (bp != "xs") {
            $('.make_static').scrollToFixed({
                marginTop: function () {
                    if ($(this).is("header")) return 0;
                    else return $('header').length ? $('header').height() + 15 : 15;
                },
                removeOffsets: true,
                limit: function () {
                    return $("footer").length ? $("footer").offset().top - $('.make_static').outerHeight(true) : 0;
                },
                zIndex: 1,
            });
        }
        _bp = bp
    }
});

$(document).ready(function () {
    if (isMobile) {
        $(".datepicker:not(.input-daterange), .input-daterange :text").prop("readonly", true).css("background-color", "transparent");
    }

    accounting.settings = {
        currency: {
            symbol: CultureInfo.NumberFormat.CurrencySymbol,
            format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
            decimal: CultureInfo.NumberFormat.CurrencyDecimalSeparator,  // decimal point separator
            thousand: CultureInfo.NumberFormat.CurrencyGroupSeparator,  // thousands separator
            precision: CultureInfo.NumberFormat.CurrencyDecimalDigits   // decimal places
        },
        number: {
            precision: CultureInfo.NumberFormat.NumberDecimalDigits,
            thousand: CultureInfo.NumberFormat.NumberGroupSeparator,
            decimal: CultureInfo.NumberFormat.NumberDecimalSeparator
        }
    }

    $('.dropdown-submenu > a').on("click", function (e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });

    $(document).datepicker();

    $('[data-info]').each(function () {
        $(this).data("info", JSON.parse($(this).attr("data-info")));
        $(this).removeAttr("data-info");
    });

    $(document).tooltipAll();

    $('[data-filter]').on('click', '.btn', function (e) {
        var $target = $($(this).attr("target"));
        var $childrens = $target.parent().children();
        if (!$childrens.length) $childrens = $($(this).attr("target").replace('=', '!='));
        if ($(this).hasClass("active")) {
            //console.log("if");
            $(this).removeClass("active");
            //console.log($childrens);
            $childrens.show();
            return false;
        } else {
            //console.log("else");
            $childrens.not($target).hide();
            $target.show();
        }
    });

    $(".clockpicker").each(function (i, item) {
        $(this).clockpicker({
            init: function () {
                //console.log("colorpicker initiated");
                //console.log(item);
            },
            afterShow: function () {
                if ($(item).closest(".modal").length)
                    $(".clockpicker-popover").eq(i).css("z-index", $(item).closest(".modal").css("z-index"))
            },
        });
    });

    $(this).on("change", ".onoffswitch-checkbox, [data-switchery='true'], :checkbox", function () {
        var checked = this.checked;
        checked ? $(this).attr("checked", true).prop("checked", true) : $(this).removeAttr("checked").prop("checked", false);
        if (this.value == "" || this.value == "true" || this.value == "false" || !$(this).attr("value")) {
            $(this).val(checked);
        }
    }).on("ifChanged", function (e) {
        var $target = $(e.target);
        var value = e.target.checked;
        $target.checked(value).trigger("change");
    });

    $(this).on("keyup", ".input_key", function (e) {
        var value = this.value;
        var allow_space = $(e.currentTarget).hasClass("allow-space");
        if (allow_space == false) {
            value = $.trim(value.replace(/ +?/g, ''));
        }
        $(this).val(value.toUpperCase());
    }).on("keypress", ".input_key", function (e) {

        var allow_space = $(e.currentTarget).hasClass("allow-space");
        if (allow_space == false && e.which === 32)
            return false;

        var regex = new RegExp("^[a-zA-Z0-9\\-\\s]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    }).on('focusout', ".input_key", function (e) {
        var $this = $(this);
        $this.val($this.val().replace(/<[^>]*>/g, ''));
    }).on('paste', ".input_key", function (e) {
        var $this = $(this);
        setTimeout(function () {
            $this.val($this.val().replace(/<[^>]*>/g, ''));
        }, 5);
    });

    $(".text_editor").ckeditor(function () {
        //console.log(".text_editor", $(this.element.$).attr("max"));
        $(this.element.$).parent().show()
    }, {
        height: 150
    });

    $(document).startChosen();

    $.validator.addMethod("daterange", function (value, element, params) {
        var $parent = $(element).closest('.input-daterange');
        var start = $parent.find(":text:first").val();
        var end = $parent.find(":text:last").val();
        if (start && end) {
            return start.toMoment().isSameOrBefore(end.toMoment());
        }
        return true;
    }, 'Please select a valid range of dates');

    $('[validate="true"]').each(function () {
        var _super = $(this);
        //console.log(_super);
        $(this).validate({
            lang: lang,
            rules: {
                chosen: "required",
                text_editor: "required",
                "[required]": "required"
            },
            errorPlacement: function (error, element) {
                if (element.hasClass("chosen-select")) {
                    var $divSelect = element.parent().find(".chosen-container");
                    $divSelect.addClass("error");
                    element.on("change", function () {
                        if (this.value) {
                            error.remove();
                            $divSelect.removeClass("error");
                        }
                    });
                    if ($divSelect.hasClass("error")) error.insertAfter(element.next());
                }
                else {
                    if (element.closest(".input-group").length) {
                        error.insertAfter(element.closest(".input-group"));
                    } else if (element.is(':radio') || element.is(':checkbox')) {
                        error.appendTo(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            },
            submitHandler: function (form, validator) {
                $(form).find(".form-control").each(function () {
                    var $container = $(this).closest(".tab-pane") ? $(this).closest(".tab-pane") : $(this).closest(".form-group");
                    if ($container.hasClass('tab-pane')) {
                        var $tab = $('[data-toggle="tab"][href="#' + $container.attr("id") + '"]');
                        $container.find(".error").length ? $tab.addClass('has-error') : $tab.removeClass('has-error');
                    }
                });

                if ($(form).valid() && $(form).attr("action")) {
                    console.log("valid", $(form).attr("action"));
                    form.submit();
                }
            }
        });

        $(this).find(".input-daterange :text").each(function () {
            $(this).rules('add', {
                daterange: true
            });
        });

        if (!$(this).find('[type="submit"]').length) {
            $(this).on("submit", false);
        }
    });

    $('.colorpicker').colorpicker().on('changeColor', function (e) {
        var $container = $(this).closest(".form-group");
        if ($container.length) $container.find(".fa-tint").css("color", e.color.toHex());
    }).on("create", function (e) {
        var $modal = $(this).closest(".modal");
        if ($modal.length) $(".colorpicker").css("z-index", $modal.css("z-index") + 100);
    });

    $.showSwitchery();

    var $full_height_scroll = $('.full-height-scroll');
    if ($full_height_scroll.length) {
        $full_height_scroll.slimscroll({
            height: '100%'
        });
    }

    $('.scroll_content').slimscroll();

    toastr.options = {
        closeButton: true,
        progressBar: false,
        preventDuplicates: true,
        positionClass: "toast-top-right"
    };

    $.runtoastr();

    $("[filterIntoTable]").each(function () {
        $(this).filterIntoTable($(this).attr("filterIntoTable"));
    });

    $('.num').num();
    $.fn.button.Constructor.DEFAULTS.loadingText = "<i class='fa fa-spinner fa-spin'></i>";

    $('.input-number').each(function () {
        var _super = $(this);

        $(this).inputFilter(function (value) {
            return FilterIntegerBoth.test(value);
        });
    });

    try {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
            increaseArea: '20%'
        });
    }
    catch (err) {
        console.error("The necessary library for iCheck needs to be imported.");
    }
});

jQuery.expr[':'].lowerContains = function (a, i, m) {
    return jQuery(a).text().toLowerCase().indexOf(m[3].toLowerCase()) >= 0;
};

String.prototype.toDate = function () {
    var _moment = this.toString().toMoment();
    return _moment.isValid() ? _moment.toDate() : null;
}

String.prototype.toMoment = function (dateFormatParameter, localeParameter) {
    return moment(this.toString(), (dateFormatParameter == undefined ? dateFormat : dateFormatParameter), (localeParameter == undefined ? moment.locale() : localeParameter));
}

String.prototype.toCamelCase = function () {
    return this.toString().replace(/(?:^|\s)\w/g, function (match) {
        return match.toUpperCase();
    });
};

String.prototype.isHTML = function () {
    var str = this.toString();
    var doc = new DOMParser().parseFromString(str, "text/html");
    return Array.from(doc.body.childNodes).some(node => node.nodeType === 1);
};

Number.prototype.restPercent = function (percent, fixed) {
    var total = parseFloat(this.toString());
    return ((percent / 100) * total).toFixed(fixed ? fixed : 2);
}

Array.prototype.sum = function () {
    var total = 0;
    $(this).each(function (i, value) {
        total += value ? parseFloat(value) : 0;
    });
    return total;
};

$.fn.num = function (decimalSeparator, decimalPlaces) {
    $(this).each(function () {
        if (!decimalSeparator) decimalSeparator = $(this).attr("decimal") ? $(this).attr("decimal") : CultureInfo.NumberFormat.CurrencyDecimalSeparator;
        if (!decimalPlaces) decimalPlaces = $(this).attr("decimalPlaces") ? parseInt($(this).attr("decimalPlaces")) : CultureInfo.NumberFormat.CurrencyDecimalDigits;
        $(this).numeric({
            decimal: decimalSeparator,
            decimalPlaces: decimalPlaces,
            negative: false
        });
    });
}

$.fn.getValues = function (str_find, str_replace) {
    var values = {};
    var array = [];
    $(this).find("select, textarea, input, :text, :checkbox, :hidden").each(function () {
        var $this = $(this);
        var name = this.name;
        var value = this.value;
        str_find ? name = String(name).replace(str_find, str_replace) : null;
        if (this.value != undefined && this.name && jQuery.type(this.name) != "undefined") {
            if ($(this).data('DateTimePicker')) {
                values[name] = $(this).data("DateTimePicker").date() != null ? $(this).data("DateTimePicker").date().format(dateTimeFormatDB) : null;
            } else if ($(this).data('ckeditorInstance')) {
                values[name] = $(this).data("ckeditorInstance").getData();
            } else if ($(this).hasClass("num")) {
                values[name] = accounting.unformat(value, CultureInfo.NumberFormat.CurrencyDecimalSeparator);
            } else if ($(this).is(":checkbox")) {
                if (name.includes("[]")) {
                    name = name.replace("[]", "");
                    if ($(this).is(':checked')) {
                        array.push(value);
                        values[name] = array;
                    }
                } else {
                    values[name] = (value.toLowerCase() == "true" ? true : false);
                    array = [];
                }
            }
            else {
                values[name] = value;
            }
        }
    });
    return values;
};

$.fn.setValue = function (value) {
    $(this).val(value);
    if ($(this).is("select")) {
        var IsChosen = $(this).data("chosen");
        if (IsChosen && IsChosen.is_multiple == false) {
            $(this).find('option[selected]').removeAttr("selected");
            $(this).find('option[value="' + value + '"]').attr("selected", true);
        }
    } else {
        $(this).trigger("change");
    }
};

$.fn.setValues = function (data, str_add) {
    var $this = this;
    $this.clear();
    $.each(data, function (key, value) {
        value = $.trim(value);
        if (key.length && key != "") {
            str_add != undefined ? key = (str_add + key) : null;
            var $input = $this.find('[name="' + key + '"]');
            $input = $input.length ? $input : $this.find("#" + key);
            if ($input.length) {
                if ($input.is("select")) {
                    if ($input.hasClass("chosen-select")) {
                        $input.setValue(value);
                        $input.trigger("change");
                    }
                    else if ($input.find('option[value="' + value + '"]').length) {
                        $input.find('option[value="' + value + '"]').attr("selected", true);
                        $input.setValue(value);
                    }
                } else if ($input.is(":input") || $input.is("textarea")) {
                    $input.is(":checkbox") ? $input.checked(value).trigger('change') : $input.val(value);
                    if ($input.hasClass("text_editor") && CKEDITOR) {
                        var ck = CKEDITOR.instances[key];
                        if (ck) { ck.setData(value); }
                    }
                } else if (value && !$input.hasClass("dropzone")) {
                    value.toString().isHTML() ? $input.html(value) : $input.text(value);
                } else if ($input.hasClass("dropzone")) {
                    var dropzone = $input.data("Dropzone");
                    var rootFolder = $input.attr("rootFolder") ? $input.attr("rootFolder") : "/";
                    if (dropzone.options.maxFiles != null && dropzone.options.maxFiles == 1) dropzone.findFile(rootFolder + value);
                }
            }
        }
    });
    return $this;
};

$.fn.clear = function (focus, except) {
    if (except == undefined) except = "";
    this.find('select').not(except).each(function () {
        var $first = $(this).find("option:first");
        $(this).find("option").removeAttr("selected");
        $first.prop("selected", true);
        $(this).val($first.val()).change();
    });
    this.find(':text, textarea, input[type="password"], input[type="number"], input[type="email"], input[type="tel"], input[type="hidden"]:not(.static)').not(except).val("");
    this.find(".datepicker").not(except).each(function () { $(this).trigger("dp.change"); });
    this.find('[default-value]').not(except).each(function () { $(this).val($(this).attr("default-value")).trigger("change"); });
    this.find(":checkbox").not(except).each(function () {
        var $parent = $(this).parent();
        if ($parent.is("label")) $parent.removeClass("active");
        $(this).val((this.value === "true" || this.value === "false" || this.value === true || this.value === false) ? false : this.value).prop("checked", false);
    });

    if (Switchery) $("[data-switchery]").not(except).each(function () { $(this).data("Switchery").setPosition(); });
    var $validator = $(this).attr("validate") ? $(this) : ($(this).find("[validate]").length ? $(this).find("[validate]") : null);
    if ($validator) {
        $validator = $validator.data("validator");
        if ($validator) {
            $validator.resetForm();
            $validator.currentForm.reset();
            $($validator.currentForm).find(".error").removeClass("error");
        }
    }
    this.find('.dropzone').not(except).each(function () {
        $(this).data("Dropzone").removeAllFiles();
    });
    if (focus) this.find('.form-control').first().focus();
    return this;
};

$.fn.filterText = function (filter) {
    var $rows = $(this).find("tbody > tr");
    if (filter == "") {
        $rows.show();
        return;
    }
    $rows.hide().find("td:lowerContains('" + filter.toLowerCase() + "')").closest("tr").show();
}

$.success = function (title, msn, reload, wait) {
    toastr.clear();
    toastr.success(msn, title ? title : "");
    if (!wait) wait = 1000;
    if (reload) $.reload(wait);
}

$.error = function (title, msn, reload, wait) {
    toastr.clear();
    toastr.error(msn, title ? title : "");
    if (!wait) wait = 1000;
    if (reload) $.reload(wait);
}

$.warning = function (title, msn, reload, wait) {
    toastr.clear();
    toastr.warning(msn, title ? title : "");
    if (!wait) wait = 1000;
    if (reload) $.reload(wait);
}

$.runtoastr = function () {
    $("[show-toastr='1']").each(function () {
        switch ($(this).attr("toastr-type")) {
            case "success":
                $.success($(this).attr("title"), $(this).attr("msn"));
                break;
            case "error":
                $.error($(this).attr("title"), $(this).attr("msn"));
                break;
        }
    });
}

$.redirect = function (url, wait) {
    if (!wait) {
        window.location.href = url;
    }
    else {
        setTimeout(function () {
            window.location.href = url;
        }, wait);
    }
}

$.reload = function (wait) {
    if (!wait) {
        window.location.reload();
    }
    else {
        setTimeout(function () {
            window.location.reload();
        }, wait);
    }
}

$.fn.addError = function (error) {
    if (error) $(this).showTooltip(error);
    $(this).addClass("error").delay(1500).queue(function () {
        $(this).removeClass("error").dequeue();
    });
    return this;
};

$.fn.showError = function (error) {
    $(this).text(error).show(0).delay(1500).hide(0);
    return this;
};

$.fn.showTooltip = function (error) {
    $(this).attr("data-original-title", error).on("focus", function () {
        $(this).tooltip('hide').removeAttr("data-original-title");
    }).tooltip({
        container: 'body',
        placement: 'bottom',
        template: '<div class="tooltip tooltip-error" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    });
}

$.showSwitchery = function () {
    var $switch_small = $('.js-switch');
    if ($switch_small.length) {
        $switch_small.each(function () {
            if (!$(this).data("Switchery")) {
                var switchery = new Switchery(this, { color: '#1BB394' });
                $(this).data('Switchery', switchery);
            }
        });
    }

    var $switch_small = $('.js-switch-small');
    if ($switch_small.length) {
        $switch_small.each(function () {
            if (!$(this).data("Switchery")) {
                var switchery = new Switchery(this, { size: 'small' });
                $(this).data('Switchery', switchery);
            }
        });
    }
};

$.fn.startChosen = function () {
    this.find(".chosen-select, .chosen-select-sm")
        .on("chosen:ready", function (e, target) {
            var $chosen = $(target.chosen.container);
            if ($(this).hasClass("chosen-inline")) $chosen.addClass("chosen-inline");
            $chosen.data("chosen", target.chosen);
        })
        .on("change", function () {
            if ($(this).data("chosen").is_multiple == false) {
                $(this).setValue(this.value);
            }
            $(this).trigger("chosen:updated");
        })
        .chosen({
            width: '100%',
            placeholder_text_multiple: 'ALL',
            placeholder_text_single: '',
            allow_single_deselect: true,
            inherit_select_classes: false,
            search_contains: true,
            disable_search_threshold: 10
        })
        .each(function () {
            var chosen = $(this).data("chosen");
            if (chosen) {
                $(chosen.selected_item).addClass("form-control selected_item");
                $(chosen.search_field).addClass("form-control search_field");

                if ($(this).hasClass("input-sm")) {
                    $(chosen.selected_item).addClass("input-sm");
                }
            }
        });
}

$.fn.checked = function (checked) {
    var valueTrueOrFalse = ($(this).attr("value") == "true" || $(this).attr("value") == "false");
    checked = (checked == true || checked === "true") ? true : false;
    if (valueTrueOrFalse) $(this).val(checked);
    checked ? $(this).attr('checked', checked) : $(this).removeAttr('checked');
    var $switchery = $(this).data('Switchery');
    if ($switchery) {
        $switchery.element.checked = checked;
        $switchery.setPosition();
    }
    if ($(this).data('iCheck')) {
        checked ? $(this).iCheck('check') : $(this).iCheck('uncheck');
        $(this).iCheck('update');
    }
    if ($(this).closest("div.btn-group") && $(this).closest("div.btn-group").attr("data-toggle") == "buttons") {
        if (valueTrueOrFalse) $(this).val(checked);
        if (checked) $(this).click();
    }
    return this;
}

$.fn.filterIntoTable = function (table) {
    $(this).on("keyup", function () {
        $(table).filterText(this.value);
    });
}

$.fn.isValid = function () {
    var valid = true;
    $(this).find(".form-control").removeClass("error").each(function () {
        if ($(this).attr("required") && !this.value) {
            $(this).addClass("error");
            valid = false;
        }
    });
    //console.log(this);
    return valid;
}

$.fn.searchClient = function (options) {
    var _options = {
        minLength: 2
    };
    $.extend(_options, options);
    var $autocomplete = $(this).autocomplete({
        minLength: _options.minLength,
        source: function (request, response) {
            var _this = this;
            var name = request.term;
            $.getJSON("/Functions/GetClientData/", { cliente: name }, function (data, status, xhr) {
                var dataResponse = $.map(data, function (item) {
                    var text = item.Customer_Name + " " + item.Customer_LastName;
                    return {
                        data: item,
                        value: text,
                        label: text,
                        id: item.Customer_Id
                    }
                });
                response(dataResponse);
                _this.data = data;
            });
        },
        select: _options.select
    });
    return $autocomplete.data("ui-autocomplete");
}

$.fn.searchSchool = function (options) {
    var _options = {
        minLength: 2
    };
    $.extend(_options, options);
    var $autocomplete = $(this).autocomplete({
        minLength: _options.minLength,
        source: function (request, response) {
            var _this = this;
            var name = request.term;
            $.getJSON("/Functions/GetSchoolData/", { school: name, city: $("#school-search").find("[name='Customer_City']").val() }, function (data, status, xhr) {
                var dataResponse = $.map(data, function (item) {
                    var text = item.Customer_Company_Name + " (" + item.Customer_City + ")";//item.Customer_Name + " " + item.Customer_LastName;
                    return {
                        data: item,
                        value: text,
                        label: text,
                        id: item.Customer_Id
                    }
                });
                response(dataResponse);
                _this.data = data;
            });
        },
        select: _options.select,
        search: _options.search
    });
    return $autocomplete.data("ui-autocomplete");
}

$.fn.keypressCtrlEnter = function () {
    var options = $.extend({ callback: function () { } }, arguments[0] || {});

    $(this).keydown(function (e) {
        if (e.ctrlKey && e.keyCode == 13) {
            options.callback.call(this);
        }
    });
}

String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
            ? args[number]
            : match
        ;
    });
};

String.prototype.findText = function (text) {
    return this.toString().indexOf(text) != -1 ? true : false;
}

var getDates = function (startDate, endDate) {
    var now = startDate, dates = [];
    while (now.isSameOrBefore(endDate)) {
        dates.push(now.format("L").toMoment());
        now.add(1, 'days');
    }
    return dates;
};

var getData = function (url, data, dataType, async) {
    if (async == undefined) async = false
    var res = null;
    $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: dataType ? dataType : "JSON",
        async: async,
        cache: false,
        success: function (dataResult) {
            console.log("SUCCESS");
            if (!dataType && dataType != "html") {
                if ($.type(dataResult) === "string") dataResult = JSON.parse(dataResult);
                if ($.type(dataResult) === "array") {
                    if (!dataResult.length) dataResult = null;
                    if (dataResult && data && data.object) dataResult = dataResult[0];
                }
            }
            res = dataResult;
            if (data && data.console) console.log(res);
        },
        error: function (xhr, error, res) {
            console.log("ERROR");
            console.log(url);
            console.log(data);
            console.log(dataType);
            console.log(xhr.responseText);
        },
        complete: function (xhr, status) {
            console.log("COMPLETE");
        }
    });
    return res;
};


function breakpoint() {
    var w = window.innerWidth;
    if (w <= 768) return "xs";
    else if (w > 768 && w < 960) return "sm";
    else if (w > 960 && w < 1152) return "md";
    else return "lg"
}

$.fn.tooltipAll = function () {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
}

$.fn.datepicker = function (optionsAdd) {
    var options = {
        //debug: true,
        allowInputToggle: false,
        inline: false,
        ignoreReadonly: true,
        showClose: true,
        showClear: true,
        showTodayButton: true,
        useCurrent: false,
        format: 'L',
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        }
    };

    if (optionsAdd != null || optionsAdd != undefined)
        options = Object.assign(options, optionsAdd);

    //console.log(options);
    this.find('.datepicker:not(.input-daterange)').attr("placeholder", dateFormat).datetimepicker(options);
    //.each(function () {
    //    delete options.defaultDate;
    //    var $this = $(this);
    //    var strDate = $(this).attr("value");
    //    var _defaultDate = (!strDate) ? false : strDate.toMoment();
    //    if (_defaultDate = false) options.defaultDate = _defaultDate;
    //    $this.datetimepicker(options);
    //    //console.log($this.attr("id"), strDate, _defaultDate, options.defaultDate)
    //})
    this.find('.datepicker :text').attr("placeholder", dateFormat);
    var $dateRange = $(this).find(".input-daterange");
    if ($dateRange.length) {
        function showDatePicker(e) {
            var $input = $(e.currentTarget);
            var $rangeContainer = $input.closest(".input-daterange");
            if ($rangeContainer.length) {
                var $start = $rangeContainer.find("input:text:first");
                var $end = $rangeContainer.find("input:text:last");
                if ($start.val() && $end.val()) {
                    var $dtp = $rangeContainer.find(".bootstrap-datetimepicker-widget");
                    var dates = getDates($start.val().toMoment(), $end.val().toMoment());
                    $.each(dates, function (i) {
                        $td = $dtp.find('.day[data-day="' + this.format("L") + '"]');
                        if (i == 0) $td.addClass("range-start");
                        else if (i == dates.length - 1) $td.addClass("range-end");
                        $td.addClass('range');
                    });
                }
            }
        }

        function changeDatePicker(e) {
            var $input = $(e.currentTarget);
            var $rangeContainer = $input.closest(".input-daterange");
            if ($rangeContainer.length) {
                var $start = $rangeContainer.find("input:text:first");
                var $end = $rangeContainer.find("input:text:last");
                if ($start.val() && $end.val()) {
                    var $dtp = $rangeContainer.find(".bootstrap-datetimepicker-widget");
                    var dates = getDates($start.val().toMoment(), $end.val().toMoment());
                    $rangeContainer.data("Dates", dates.map(p => p.format("DD/MM/YYYY")));
                }
            }
        }

        $dateRange.each(function () {
            var $start = $(this).closest(".input-daterange").find("input:text:first");
            if ($start.data("DateTimePicker") != undefined) $start.data("DateTimePicker").destroy();
            if ($start.length == 0) $start = $(this).closest(".input-daterange").find("input:first");
            var $end = $(this).closest(".input-daterange").find("input:text:last");
            if ($end.data("DateTimePicker") != undefined) $end.data("DateTimePicker").destroy();
            if ($end.length == 0) $end = $(this).closest(".input-daterange").find("input:last");

            if ($start.data("format")) options.format = $start.data("format");
            if ($end.data("format")) options.format = $end.data("format");

            if ($start.attr("min-date")) {
                options.minDate = $start.attr("min-date").toMoment();
                options.date = options.minDate;
            }
            $start.attr("placeholder", dateFormat).datetimepicker(options).on("dp.change", function (e) {
                if (!e.date && $start.val()) e.date = $start.val().toMoment();
                if (e.date) $end.attr("active", false).data("DateTimePicker").minDate(e.date);
                changeDatePicker(e);
            }).on("dp.show", showDatePicker);

            options.useCurrent = false;//Important!
            if ($end.attr("max-date")) options.maxDate = $end.attr("max-date").toMoment();
            $end.attr("placeholder", dateFormat).datetimepicker(options).on("dp.change", function (e) {
                if (!e.date && $end.val()) e.date = $end.val().toMoment();
                if (e.date) $start.attr("active", false).data("DateTimePicker").maxDate(e.date);
                changeDatePicker(e);
            }).on("dp.show", showDatePicker);

            if ($start.val()) $start.trigger("dp.change");
            if ($end.val()) $end.trigger("dp.change");
        });
    }
}

function initializeFooTable($selecto, _columns, _rows) {
    if (_rows != [])
        _rows = $.post(_rows["url"], _rows["data"])
    var ft = FooTable.init($selector, { columns: _columns, rows: _rows });
    return ft;
}

function extend(obj, src) {
    for (var key in src) {
        if (src.hasOwnProperty(key)) obj[key] = src[key];
    }
    return obj;
}

function getImageSizeInBytes(imgURL) {
    var request = new XMLHttpRequest();
    request.open("HEAD", imgURL, false);
    request.send(null);
    var headerText = request.getAllResponseHeaders();
    var re = /Content\-Length\s*:\s*(\d+)/i;
    re.exec(headerText);
    return parseInt(RegExp.$1);
}

function initializeDropzone($selector, _options) {
    if (!$selector) throw new Error("The selector is empty!");
    if (!_options) throw new Error("The options are empty!");
    var myDropzone = null, object = null;
    //console.log(_options);
    var optionsDefault = {
        url: _options.URL,
        paramName: _options.PARAMNAME,
        maxFiles: _options.MAXFILES,
        maxFilesize: _options.MAXFILESIZE,
        uploadMultiple: _options.UPLOADMULTIPLE,
        addRemoveLinks: true,
        parallelUploads: 1,
        acceptedFiles: _options.ACCEPTEDFILES,
        autoQueue: _options.AUTOQUEUE,
        autoProcessQueue: _options.AUTOPROCESSQUEUE,
        createImageThumbnails: true,
        maxThumbnailFilesize: 1,
        thumbnailWidth: _options.THUMBNAILWIDTH,
        thumbnailHeight: _options.THUMBNAILHEIGHT,
        thumbnailMethod: "contain",
        dictDefaultMessage: _options.DICTDEFAULTMESSAGE ? _options.DICTDEFAULTMESSAGE : Dropzone.prototype.defaultOptions.dictDefaultMessage,
        dictRemoveFile: _options.BTNCIRCLE ? "<i class=\"fa fa-times\"></i>" : Dropzone.prototype.defaultOptions.dictRemoveFile,
        init: function () {
            instance = this;
            $(instance.element).data("Dropzone", instance);
            $(instance.element).data("width", _options.WIDTH);
            $(instance.element).data("height", _options.HEIGHT);

            if (undefined !== _options.THUMBNAILURLS && _options.THUMBNAILURLS.length) {
                _options.THUMBNAILURLS.forEach(function (element, idx) {
                    if (typeof element == 'object') { instance.findFile(element.Url, element.Id, idx); }
                    if (typeof element == 'string') { instance.findFile(element); }
                });
            }

            this.on("maxfilesexceeded", function (file) {
                $.error("ERROR", "Se ha superado el limite de archivos para subir!");
                this.removeFile(file);
            });

            this.on('thumbnail', function (file) {
                var fileWidth = file.width;
                var fileHeight = file.height;
                var maxWidth = $($selector).data("width");
                var maxHeight = $($selector).data("height");
                var validWidth = (fileWidth == maxWidth);
                var validHeight = (fileHeight == maxHeight);
                if (file.accepted !== false && file.acceptDimensions) {
                    if (validWidth && validHeight) {
                        $($selector).find('.dz-image').last().find('img').attr({ width: _options.THUMBNAILWIDTH, height: _options.THUMBNAILHEIGHT });
                        file.acceptDimensions();
                        if (_options.BTNCIRCLE) $(file.previewElement).find(".dz-remove").addClass("btn-circle btn-circle-xs");

                    } else {
                        file.rejectDimensions();
                    }
                }
            });

            this.on("sending", function (file, xhr, formData) {
                object = _options.DATA();
                $.each(object, function () {
                    formData.append(this.name, this.value);
                });
            });

            this.on("success", function (file, response) {
                if (response != undefined) $.success("SUCCESS", "La imagen se subio correctamente");
            });
        },
        accept: function (file, done) {
            var _this = this;
            file.acceptDimensions = done;
            file.rejectDimensions = function () {
                var maxWidth = $($selector).data("width");
                var maxHeight = $($selector).data("height");
                if (maxWidth == 0 || maxHeight == 0) {
                    $.error("ERROR", 'Selecciona al menos un tipo de imagen.');
                    $("#ProductImage_ProductImage_IdImageType").focus();
                } else {
                    $.error("ERROR", `La dimensiones de la imagen deben ser ${maxWidth}px - ${maxHeight}px.`);
                }
                _this.removeAllFiles(true);
            };
        }
    };

    myDropzone = new Dropzone($selector, optionsDefault);
    return myDropzone;
}

Dropzone.prototype.findFile = function (url, id, idx) {
    if (url) {
        var dz = this;
        var image = new Image();
        var index = url.lastIndexOf("/") + 1;
        var filename = url.substr(index);
        var urlLast = url + '?' + new Date().getTime();
        var xhr = $.ajax({
            type: "HEAD",
            url: urlLast,
            cache: false,
            async: false,
            success: function () {
                image.src = urlLast;
                image.onload = function () {
                    var file = {
                        id: id,
                        idx: idx,
                        url: url,
                        name: filename,
                        width: this.width,
                        height: this.height,
                        type: xhr ? xhr.getResponseHeader('Content-Type') : null,
                        size: xhr ? parseFloat(xhr.getResponseHeader('Content-Length')) : null,
                        status: 'success'
                    };

                    //console.log("dz", file, dz);
                    //if (dz.filesToAdd) {
                    //dz.clear();
                    //dz.filesToAdd = null;

                    dz.emit("addedfile", file);
                    dz.emit("thumbnail", file, file.url);
                    dz.emit("success", file);
                    dz.files.push(file);
                    dz.emit("complete", file);
                    //}
                }
            },
            error: function (xhr, status, error) {
                console.log(error + ": " + url);
            }
        });
    }
}

Dropzone.prototype.validarDropzone = function () {
    var dropzone = this;
    var isValid = dropzone.files.length > 0;
    var $container = $(dropzone.element).closest(".tab-pane") ? $(dropzone.element).closest(".tab-pane") : $(dropzone.element).closest(".form-group");
    if ($container.hasClass('tab-pane')) {
        var $tab = $('[data-toggle="tab"][href="#' + $container.attr("id") + '"]');
        isValid ? $tab.removeClass('has-error') : $tab.addClass('has-error').click();
    }
    isValid ? $(dropzone.element).removeClass("has-error has-danger").find(".help-block").hide() : $(dropzone.element).addClass("has-error has-danger").find(".help-block").show();
    return isValid;
}

Dropzone.prototype.reorder = function () {
    var dz = this;
    if (dz.filesToAdd) {
        var files = dz.files;
        files.sort(function (a, b) {
            return a.idx > b.idx ? 1 : -1;
        });
        dz.clear();
        dz.filesToAdd = null;
        $.each(files, function (index, el) {
            dz.emit("addedfile", this);
            dz.emit("thumbnail", this, this.url);
            dz.emit("success", this);
            dz.files.push(this);
            dz.emit("complete", this);
        });
    }
}

Dropzone.prototype.clear = function () {
    var dz = this;
    $.each(dz.files, function (index, el) {
        this.clear = true;
        dz.removeFile(this);
        this.clear = false;
    });
};

$.fn.validarDropzones = function () {
    var isValid = true;
    $(this).each(function () {
        var dz = $(this).data("Dropzone");
        if ($(this).attr("required") && dz && !dz.validarDropzone()) {
            isValid = false;
        }
    });
    return isValid;
}


$.fn.sortableDB = function (data, callbacks) {
    if (callbacks == undefined) callbacks = function () { }
    var options = {
        cursor: "move",
        tolerance: "pointer",
        //containment: "parent",
        opacity: 0.7,
        revert: true,
        scroll: false,
        start: function (e, ui) {
            ui.placeholder.height(ui.item.height());
        },
        update: function (event, ui) {
            var $this = $(this);
            var sortedIDs = $this.sortable("toArray");
            var table = data && data.table ? data.table : $this.attr("data-table");
            var keyId = data && data.keyId ? data && data.keyId : $this.attr("data-keyId");
            var keyValue = data && data.keyValue ? data && data.keyValue : $this.attr("data-keyValue");
            console.log($this);
            if (table && keyId && keyValue) {
                $.post('/Functions/Sortable', {
                    table: table,
                    keyId: keyId,
                    keyValue: keyValue,
                    data: sortedIDs
                }, function (data, textStatus, xhr) {
                    if (data) {
                        var $items = $this.find('.dz-index');
                        if (!$items.length) $items = $this.find('.index');
                        $items.each(function (index, el) {
                            $(this).text(index + 1);
                        });
                        $.success("Elementos Ordenados", "Los elementos se reordenaron correctamente");
                        callbacks();
                    }
                    else $.error("Error", "surgio un error inesperado");
                });
            }
        }
    };
    if (data && data.items) {
        options.items = data.items;
    }
    $(this).sortable(options).disableSelection(options);
}

$.fn.dataTable.ext.order['dom-text'] = function (settings, col) {
    return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
        return $('input', td).val();
    });
}

$.fn.dataTable.ext.order['dom-text-numeric'] = function (settings, col) {
    return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
        return $('input', td).val() * 1;
    });
}

$.fn.dataTable.ext.order['dom-checkbox'] = function (settings, col) {
    return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
        return $('input', td).prop('checked') ? '1' : '0';
    });
}

$.fn.validValues = function () {
    var max = $(this).find("input[required], select[required]").length;
    var isValid = 0;
    $(this).find("input[required], select[required]").each(function () {
        var $this = $(this);
        if (!$this.val()) {
            if ($this.hasClass("chosen-select")) {
                $this.data("chosen").container.addClass("error");
            } else if ($this.is(":input")) {
                $this.addClass("error");
            }
        } else {
            isValid++;
            if ($this.hasClass("chosen-select")) {
                $this.data("chosen").container.removeClass("error");
            } else if ($this.is(":input")) {
                $this.removeClass("error");
            }
        }
    })
    return max == isValid;
}

$.fn.GenerateSwitch = function (valueParameter, idParameter, classParameter, nameParameter, attrParameter, urlParameter, rowDataParamater, msgParameter) {
    var div = document.createElement("div");
    div.className = "onoffswitch";
    div.style = "margin:auto;";

    var input = document.createElement("input");
    input.type = "checkbox";
    if (idParameter != null) {
        input.id = `${nameParameter}_${idParameter}`;
    }
    input.name = `${nameParameter}${(idParameter != null ? "_" + idParameter : "")}`;
    input.className = `onoffswitch-checkbox ${classParameter}`;
    if (idParameter != null) {
        input.setAttribute(attrParameter, idParameter);
    }
    input.checked = `${(valueParameter == 'true' || valueParameter == true) ? `checked` : ``}`;
    input.onchange = function () {
        var ID = this.getAttribute(attrParameter);
        var checked = this.checked;
        var row = $(this).closest("tr").data("__FooTableRow__");
        var values = rowDataParamater;
        $.post(urlParameter, {
            id: ID,
            status: checked,
            column: classParameter
        }, function (res) {
            if (res) {
                values[classParameter] = checked;
                if (row instanceof FooTable.Row) {
                    row.val(values);
                    $.success("SUCCESS", "Se " + (status ? "activo" : "desactivo") + " correctamente");
                }
            } else {
                $.error("ERROR", msgParameter);
            }
        }).fail(function (response, status, xhr) {
            console.log(response, status, xhr);
            $.error("ERROR", msgParameter);
        });
    };

    var label = document.createElement("label");
    label.className = "onoffswitch-label";
    label.setAttribute("for", `${classParameter}${(idParameter != null ? "_" + idParameter : "")}`);

    var spaninner = document.createElement("span");
    spaninner.className = "onoffswitch-inner";

    var spanswitch = document.createElement("span");
    spanswitch.className = "onoffswitch-switch";

    label.appendChild(spaninner);
    label.appendChild(spanswitch);
    div.appendChild(input);
    div.appendChild(label);

    return div;
}

//Integer values (both positive and negative):
const FilterIntegerBoth = /^-?\d*$/
//Integer values (positive only):
const FilterInteger = /^\d*$/
//Integer values (positive and up to a particular limit):
///^\d*$/.test(value) && (value === "" || parseInt(value) <= 500)
//Floating point values (allowing both . and , as decimal separator):
const FilterFloatingPoint = /^-?\d*[.,]?\d*$/
//Currency values (i.e. at most two decimal places):
const FilterCurrency = /^-?\d*[.,]?\d{0,2}$/
//Hexadecimal values:
const FilterHexadecimal = /^[0-9a-f]*$/i

$.fn.inputFilter = function (inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
        if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
    });
};