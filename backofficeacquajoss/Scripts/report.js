﻿function clipboard(_this) {
    var $this = $(_this);
    var $container = $this.closest("div.ibox");
    var $table = $container.find("table");
    var namefile = $this.data("namefile").toUpperCase() + '-' + moment().format("YYYY-MM-DD HH:mm:ss");

    var table_html = $table.prop('outerHTML');

    table_html = table_html.replace(new RegExp('€', "g"), '');

    exportToExcel(table_html, namefile);

}


function exportToExcel(htmls, name) {

    var uri = 'data:text/vnd.ms-excel;base64,';

    var base64 = function (s) {
        return window.btoa(unescape(encodeURIComponent(s)));
    };

    var dato = base64(htmls);
    var blob = b64toBlob(dato, uri);
    var blobUrl = URL.createObjectURL(blob);

    var link = document.createElement("a");
    link.download = name + ".xls";
    link.href = blobUrl;
    link.click();

}


function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
}