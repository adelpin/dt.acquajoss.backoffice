﻿using Core;
using Core.Db;
using Core.Properties;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BackOfficeAcquaJoss.Models
{
    public class ReservationModel
    {
        public Reservations reservation = null;
        public ReservationModel(int id)
        {
            reservation = Reservations.Instance().GetResarvation(id);
        }

        public InfoBancaSella infoBanck { get; set; } = null;
    }

    public class ReservationsModel : MainModel
    {
        private string Conexion = Core.Properties.Settings.Default.DT_BackOffice;

        private static ReservationsModel _istance = null;

        public new static ReservationsModel Instance()
        {
            if (_istance == null) _istance = new ReservationsModel();
            return _istance;
        }


        public Product GetTicketFixed(int Codice)
        {
            List<Product> Prod = new List<Product>();

            SqlParameter Id = new SqlParameter("IdProduct", Codice);

            string json = CRUD.ExecuteCommandQuery(Settings.Default.DT_BackOffice, "spBackOfficeGetTicketInfo", Id);
            Prod = string.IsNullOrEmpty(json) ? null : JsonConvert.DeserializeObject<List<Product>>(json);

            return Prod[0];
        }

        public List<Products> ProductsList
        {
            get
            {
                return Core.Products.Instance(Global.Lang).GetProducts();
            }
        }

        public List<ParkOperational> ParkOperationalList
        {
            get
            {
                return ParkOperational.Instance().GetParkOperationalList(new Hashtable() {
                    { "ParkOperational_Status", true }
                });
            }
        }

        public List<ParkCalendar> ParkCalendarList
        {
            get
            {
                return ParkCalendar.Instance().GetParkCaledarActives();
            }
        }

        public List<ParkSchedules> ParkSchedulesList
        {
            get
            {
                return Core.ParkSchedules.Instance().GetParkSchedules(new Hashtable() {
                    { "ParkSchedule_Status", true }
                });
            }
        }

        public List<DiscountCoupon> DiscountCouponList
        {
            get
            {
                return new DiscountCoupon().GetDiscounts(new Hashtable() {
                    { "DiscountCoupon_Status", true }
                });
            }
        }

        public List<DynamicPrice> ReadDynamicPrices(int anno, int mese)
        {
            List<DynamicPrice> Lista = new List<DynamicPrice>();

            List<SqlParameter> Parameters = new List<SqlParameter>();
            Parameters.Add(new SqlParameter("anno", anno));
            Parameters.Add(new SqlParameter("mese", mese));

            string json = CRUD.ExecuteCommandQuery(Settings.Default.DT_BackOffice, "spBackOfficeDynamicCalendar", Parameters);
            Lista = string.IsNullOrEmpty(json) ? null : JsonConvert.DeserializeObject<List<DynamicPrice>>(json);

            return Lista;
        }
    }
}