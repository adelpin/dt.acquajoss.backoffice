﻿using Core.Db;
using Core.Properties;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BackOfficeAcquaJoss.Models
{
    public class Product
    {
        public string Id { get; set; }
        public string IdFather { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string AdditionalInfo { get; set; }
        public string Image { get; set; }
        public string BasePrice { get; set; }
        public string Price { get; set; }
        public string Quantity { get; set; }
    }

  
    public class DynamicPriceModel
    {
        public Product GetTicketFixed(int Codice)
        {
            List<Product> Prod = new List<Product>();
            SqlParameter Id = new SqlParameter("IdProduct", Codice);
            string json = CRUD.ExecuteCommandQuery(Settings.Default.DT_BackOffice, "spBackOfficeGetTicketInfo", Id);
            Prod = string.IsNullOrEmpty(json) ? null : JsonConvert.DeserializeObject<List<Product>>(json);
            return Prod[0];
        }
              
    }
}