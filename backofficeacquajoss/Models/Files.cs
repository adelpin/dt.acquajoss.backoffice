﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace BackOfficeAcquaJoss.Models
{

    public class Files
    {
        public string Path { get; set; }
        public string Folder { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public long Size { get; set; }
    }

    public class FileUpload
    {
        public string Upload(HttpRequestBase request)
        {
            string fName = "";

            var message = new { Message = "Error in saving file", File = fName };

            try
            {
                foreach (string file in request.Files)
                {
                    var fileContent = request.Files[file];

                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        fName = fileContent.FileName;
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        var fileName = Path.GetFileName(file);
                        var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), fileContent.FileName);
                        using (var fileStream = new FileStream(path, FileMode.Create))//System.IO.File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }

                        //FileStream fileStream = File.Create(path, (int)fileContent.InputStream.Length);
                        //// Initialize the bytes array with the stream length and then fill it with data
                        //byte[] bytesInStream = new byte[fileContent.InputStream.Length];
                        //fileContent.InputStream.Read(bytesInStream, 0, bytesInStream.Length);
                        //// Use write method to write to the file specified above
                        //fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                    }
                    message = new { Message = "File uploaded successfully", File = fName };
                }
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(message);
            }

            return JsonConvert.SerializeObject(message);
        }

        public int Upload(HttpPostedFileBase file, Files data)
        {
            try
            {
                if (file != null && file.ContentLength > 0)
                {
                    // get a stream
                    var stream = file.InputStream;
                    // and optionally write the file to disk
                    var fileName = Path.GetFileName(string.IsNullOrEmpty(data.Name) ? file.FileName : data.Name);
                    var originalDirectory = new DirectoryInfo($"{HttpContext.Current.Server.MapPath(@"\")}{data.Path}\\");

                    string pathString = originalDirectory.ToString();
                    if (!string.IsNullOrEmpty(data.Folder))
                        pathString = Path.Combine(originalDirectory.ToString(), data.Folder + "/"); //subcarpeta para guardar la imagen
                   

                    bool isExists = Directory.Exists(pathString);

                    if (!isExists)
                        Directory.CreateDirectory(pathString);

                    var savepath = Path.Combine(pathString, fileName);

                    using (var fileStream = new FileStream(savepath, FileMode.Create))//System.IO.File.Create(path))
                    {
                        stream.CopyTo(fileStream);
                    }
                }
            }
            catch (Exception e)
            {
                var error = e.Message;
                return 0; //new { status = false, message = "Error in saving file", type = "error" }; //JsonConvert.SerializeObject(message);
            }

            return 1; //new { status = true, message = "File uploaded successfully", type = "success" }; //JsonConvert.SerializeObject(message);
        }

        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public List<Files> ProcessDirectory(DirectoryInfo targetDirectory)
        {
            List<Files> filesFound = new List<Files>();
            // Process the list of files found in the directory.
            var directories = targetDirectory.GetFiles("*"); // ,  SearchOption.AllDirectories

            foreach (FileInfo d in directories)
            {
                filesFound.Add(new Files { Name = d.Name, Path = $"/{d.Directory.Name}/{d.Name}", Size = d.Length });
            }

            return filesFound;
        }

        // Insert logic for processing found files here.
        public void ProcessFile(string path)
        {
            Console.WriteLine("Processed file '{0}'.", path);
        }
    }
}