﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BackOfficeAcquaJoss.Models
{
    public static class DecimalExtension
    {
        private static readonly Dictionary<string, CultureInfo> ISOCurrenciesToACultureMap = CultureInfo.GetCultures(CultureTypes.SpecificCultures)
           .Select(c => new { c, new RegionInfo(c.LCID).ISOCurrencySymbol })
           .GroupBy(x => x.ISOCurrencySymbol)
           .ToDictionary(g => g.Key, g => g.First().c, StringComparer.OrdinalIgnoreCase);

        public static string FormatCurrency(this decimal amount, string currencyCode)
        {
            CultureInfo culture;
            if (ISOCurrenciesToACultureMap.TryGetValue(currencyCode, out culture))
                return amount.ToString("C").ToString(culture);
            return amount.ToString("C");
        }
    }
}