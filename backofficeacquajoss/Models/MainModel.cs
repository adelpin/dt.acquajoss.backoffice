﻿using Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Models
{
    public class MainModel
    {
        private static MainModel _instance = null;
        public string Lang = Global.LangSelected;
        public MainModel()
        {
            _instance = this;
        }

        public static MainModel Instance()
        {
            if (_instance == null) _instance = new MainModel();
            return _instance;
        }

        public List<Languages> LanguagesList
        {
            get
            {
                return Languages.Instance().GetLanguages();
            }
        }

        public List<SalesSource> SalesSourceList
        {
            get
            {
                return SalesSource.Instance().GetSalesSource();
            }
        }
        public List<Affiliates> AffiliatesList
        {
            get
            {
                return Affiliates.Instance().GetAffiliates(new Hashtable() {
                    { "Affiliate_Status", 1 }
                });
            }
        }
        public List<Countries> CoutriesList
        {
            get
            {
                return Countries.Instance().GetCountries();
            }
        }

        #region DropDrownList

        #region Prices
        private List<Prices> _Prices { get; set; }
        //[Display(Name = "HotelRoomsTypes", ResourceType = typeof(Core.Resources.Models))]
        public List<Prices> Prices
        {
            get
            {
                return (_Prices ?? Core.Prices.Instance().GetProductsPrices());
            }
            set
            {
                _Prices = value;
            }
        }

        public int? PricesItems_Index { get; set; }
        public IEnumerable<SelectListItem> PricessItems
        {
            get
            {
                IEnumerable<SelectListItem> items = Prices.Select(i => new SelectListItem { Value = i.Price_Id.ToString(), Text = $"High: {i.Price_High.ToString(Global.CultureInfo)} - Low: {i.Price_Low.ToString(Global.CultureInfo)}", Selected = (PricesItems_Index.HasValue ? i.Price_Id.Equals(PricesItems_Index.Value) : false), Disabled = !i.Price_Status });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }
        #endregion
        /**/

        public string LanguagesItems_Index { get; set; }

        public IEnumerable<SelectListItem> LanguagesItems
        {
            get
            {
                IEnumerable<SelectListItem> allLanguagesTypes = LanguagesList.Select(lTypes => new SelectListItem { Value = lTypes.Language_Code.ToString(), Text = lTypes.Language_Name, Selected = (lTypes.Language_Code.Equals(LanguagesItems_Index)) });
                return LanguagesList.Count > 1 ? Enumerable.Repeat(new SelectListItem { Value = "", Text = Core.Resources.General.lblSelectLanguage }, count: 1).Concat(allLanguagesTypes) : allLanguagesTypes;
            }
        }

        private Languages _LanguageSelected = null;
        public Languages LanguageSelected
        {
            get
            {
                if (_LanguageSelected == null || Global.LangChange)
                {
                    _LanguageSelected = Languages.Instance().GetLanguage(Lang);
                }

                return _LanguageSelected;
            }
        }

        /**/

        public List<Sites> SitesList
        {
            get
            {
                return Sites.Instance().GetSites().FindAll(s => s.Site_Type.Equals(SitesTypes.PUBLIC) && s.Site_Status).OrderBy(s => s.Site_Url).ToList();
            }
        }

        public string SitesItems_Index { get; set; }
        public IEnumerable<SelectListItem> SitesItems
        {
            get
            {
                IEnumerable<SelectListItem> allSites = SitesList.Select(sites => new SelectListItem { Value = sites.Site_Id.ToString(), Text = sites.Site_Url, Selected = (sites.Site_Id.ToString().Equals(SitesItems_Index)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = Core.Resources.General.lblSelectSite }, count: 1).Concat(allSites);
            }
        }

        /**/

        public List<Sections> Sections
        {
            get
            {
                return Core.Sections.Instance().GetSections().OrderBy(s => s.Section_Name).ToList();
            }
        }

        public string SectionsItems_Index { get; set; }
        public IEnumerable<SelectListItem> SectionsItems
        {
            get
            {
                IEnumerable<SelectListItem> allSections = Sections.Select(section => new SelectListItem { Value = section.Section_Id.ToString(), Text = section.Section_Name, Selected = (section.Section_Id.ToString().Equals(SectionsItems_Index)), Disabled = !section.Section_Status });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = Core.Resources.Sliders.lblSelect }, count: 1).Concat(allSections);
            }
        }

        /**/

        [Display(Name = "ProductsTypes", ResourceType = typeof(Core.Resources.Models))]
        public List<ProductsTypes> ProductsTypes
        {
            get
            {
                return Core.ProductsTypes.Instance().GetProductsType();
            }
        }

        public string ProductsTypesItems_Index { get; set; }
        public IEnumerable<SelectListItem> ProductsTypesItems
        {
            get
            {
                IEnumerable<SelectListItem> items = ProductsTypes.Select(i => new SelectListItem { Value = i.ProductType_Identifier, Text = i.ProductType_Identifier.ToUpper(), Selected = (i.ProductType_Identifier.Equals(ProductsTypesItems_Index)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/
        private List<Products> _Products { get; set; }
        [Display(Name = "Products", ResourceType = typeof(Core.Resources.Models))]
        public List<Products> Products
        {
            get
            {
                return (_Products ?? Core.Products.Instance().GetProducts());
            }
            set
            {
                _Products = value;
            }
        }

        public int? ProductsItems_Index { get; set; }
        public IEnumerable<SelectListItem> ProductsItems
        {
            get
            {
                IEnumerable<SelectListItem> items = Products.Select(i => new SelectListItem { Value = i.Product_Id.ToString(), Text = i.Product_Key.ToUpper(), Selected = (i.Product_Id.Equals(ProductsItems_Index.HasValue ? ProductsItems_Index.Value : 0)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/
        //[Display(Name = "Products", ResourceType = typeof(Core.Resources.Models))]
        public List<Hotels> Hotels
        {
            get
            {
                return Core.Hotels.Instance().GetHotels();
            }
        }

        public int? HotelsItems_Index { get; set; }
        public IEnumerable<SelectListItem> HotelsItems
        {
            get
            {
                IEnumerable<SelectListItem> items = Hotels.Select(i => new SelectListItem { Value = i.Hotel_Id.ToString(), Text = (i.Product != null) ? i.Product.Product_Name.ToUpper() : i.Hotel_Url, Selected = (HotelsItems_Index.HasValue ? i.Hotel_Id.Equals(HotelsItems_Index.Value) : false), Disabled = !i.Hotel_Online });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/

        //[Display(Name = "HotelRoomsTypes", ResourceType = typeof(Core.Resources.Models))]
        public List<HotelRoomsTypes> HotelRoomsTypes
        {
            get
            {
                return Core.HotelRoomsTypes.Instance().GetHotelRoomTypes();
            }
        }

        public string HotelRoomsTypesItems_Index { get; set; }
        public IEnumerable<SelectListItem> HotelRoomsTypesItems
        {
            get
            {
                IEnumerable<SelectListItem> items = HotelRoomsTypes.Select(i => new SelectListItem { Value = i.HotelRoomType_Identifier.ToLower(), Text = i.HotelRoomType_Identifier.ToUpper(), Selected = (HotelRoomsTypesItems_Index.Equals(i.HotelRoomType_Id) || HotelRoomsTypesItems_Index.Equals(i.HotelRoomType_Identifier)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/

        private List<TmasterMap> _TMasters { get; set; }
        [Display(Name = "TMasters", ResourceType = typeof(Core.Resources.Models))]
        public List<TmasterMap> TMasters
        {
            get{
                return (_TMasters ?? TmasterMap.Instance().GetTmasterMapList());
            }
            set
            {
                _TMasters = value;
            }
        }

        public int? TMastersItems_Index { get; set; }
        public IEnumerable<SelectListItem> TMastersItems
        {
            get
            {
                int idParkOperational = Repository.GetCurrentParkOperational();

                TMasters = TMasters.Where(x => x.ParkOperational_Id == idParkOperational).ToList();

                IEnumerable<SelectListItem> items = TMasters.Select(i => new SelectListItem { Value = i.Id.ToString(), Text = i.Description.ToUpper(), Selected = (i.Id.Equals(TMastersItems_Index.HasValue ? TMastersItems_Index.Value : 0)) });
                items = items.OrderBy(i => i.Text);
                return Enumerable.Repeat(new SelectListItem { Value = "0", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/

        //[Display(Name = "TMasters", ResourceType = typeof(Core.Resources.Models))]
        public List<DiscountCoupon> DiscountsCupons
        {
            get
            {
                return DiscountCoupon.Instance().GetDiscounts();
            }
        }

        public string DiscountsCuponsItems_Index { get; set; }
        public IEnumerable<SelectListItem> DiscountsCuponsItems
        {
            get
            {
                IEnumerable<SelectListItem> items = DiscountsCupons.Select(i => new SelectListItem { Value = i.DiscountCoupon_Identifier, Text = i.DiscountCoupon_Identifier.ToUpper(), Selected = i.DiscountCoupon_Identifier.Equals(DiscountsCuponsItems_Index) });
                items = items.Distinct();
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/
        private List<Users> _Users { get; set; }
        public List<Users> Users
        {
            get
            {
                return (_Users ?? Core.Users.Instance().GetUsers()); 
            }
            set
            {
                _Users = value;
            }
        }

        public string UsersItems_Index { get; set; }
        public IEnumerable<SelectListItem> UsersItems
        {
            get
            {
                var items = Users.Select(i => new SelectListItem { Value = i.User_Id.ToString(), Text = i.User_FullName.ToUpper(), Selected = (i.User_Id.Equals(UsersItems_Index)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/

        [Display(Name = "Sellers", ResourceType = typeof(Core.Resources.Models))]
        public List<Users> Sellers
        {
            get
            {
                return Core.Users.Instance().GetUsers().FindAll(u => u.User_Type.Equals(UserType.OPERATOR));
            }
        }

        public int? SellersItems_Index { get; set; }
        public IEnumerable<SelectListItem> SellersItems
        {
            get
            {
                IEnumerable<SelectListItem> items = Sellers.Select(i => new SelectListItem { Value = i.User_Id.ToString(), Text = i.User_FullName.ToUpper(), Selected = (i.User_Id.Equals(SellersItems_Index)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/

        //[Display(Name = "Sellers", ResourceType = typeof(Core.Resources.Models))]
        public List<Users> OperatorsHotel
        {
            get
            {
                return Core.Users.Instance().GetUsers().FindAll(u => u.User_Type.Equals(UserType.OPERATORHOTEL));
            }
        }

        public int? OperatorsHotelItems_Index { get; set; }
        public IEnumerable<SelectListItem> OperatorsHotelItems
        {
            get
            {
                IEnumerable<SelectListItem> items = Sellers.Select(i => new SelectListItem { Value = i.User_Id.ToString(), Text = i.User_FullName.ToUpper(), Selected = (i.User_Id.Equals(SellersItems_Index)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/

        public List<ImagesTypes> ImagesTypes
        {
            get
            {
                return Core.ImagesTypes.Instance().GetImagesTypes().OrderBy(iTypes => iTypes.ImageType_Identifier).ToList();
            }
        }

        public string ImagesItems_Index { get; set; }
        public IEnumerable<SelectListItem> ImagesItems
        {
            get
            {
                var allImagesTypes = ImagesTypes.Select(iTypes => new SelectListItem { Value = iTypes.ImageType_Id.ToString(), Text = iTypes.ImageType_Identifier, Selected = (iTypes.ImageType_Id.ToString().Equals(ImagesItems_Index)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = Core.Resources.General.lblSelectImages }, count: 1).Concat(allImagesTypes);
            }
        }

        /**/

        public List<Provinces> Provinces
        {
            get
            {
                return Core.Provinces.Instance().GetProvinces().OrderBy(pCode => pCode.Province_Region).ToList();
            }
        }

        public string ProvincesItems_Index { get; set; }
        public IEnumerable<SelectListItem> ProvincesItems
        {
            get
            {
                IEnumerable<SelectListItem> allProvinces = Provinces.Select(iTypes => new SelectListItem { Value = iTypes.Province_Code.ToString(), Text = iTypes.Province_Name, Selected = (iTypes.Province_Code.Equals(ProvincesItems_Index)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = Core.Resources.General.lblSelectProvinces }, count: 1).Concat(allProvinces);
            }
        }

        /**/

        [Display(Name = "SalesChannel", ResourceType = typeof(Core.Resources.Models))]
        public List<SalesChannel> SalesChannel
        {
            get
            {
                return Core.SalesChannel.Instance().GetSalesChannel().OrderBy(c => c.SaleChannel_Name).ToList();
            }
        }

        public string SalesChannelItems_Index { get; set; }
        public IEnumerable<SelectListItem> SalesChannelItems
        {
            get
            {
                IEnumerable<SelectListItem> allChannels = SalesChannel.Select(c => new SelectListItem { Value = c.SaleChannel_Identifier, Text = c.SaleChannel_Name.ToUpper(), Selected = (c.SaleChannel_Identifier.Equals(SalesChannelItems_Index)) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(allChannels);
            }
        }

        /**/
        private List<ParkSchedules> _ParkSchedules { get; set; }
        //[Display(Name = "ParkSchedules", ResourceType = typeof(Core.Resources.Models))]
        public List<ParkSchedules> ParkSchedules
        {
            get
            {
                return (_ParkSchedules ?? Core.ParkSchedules.Instance().GetParkSchedules());
            }
            set
            {
                _ParkSchedules = value;
            }
        }

        public int? ParkSchedulesItems_Index { get; set; }
        public IEnumerable<SelectListItem> ParkSchedulesItems
        {
            get
            {
                IEnumerable<SelectListItem> items = ParkSchedules.Select(i => new SelectListItem { Value = i.ParkSchedule_Id.ToString(), Text = i.ParkSchedule_Description.ToUpper(), Selected = (i.ParkSchedule_Id.Equals(ParkSchedulesItems_Index)), Disabled = !i.ParkSchedule_Status });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/
        private List<ActivitySchedule> _ActivitySchedules { get; set; }
        //[Display(Name = "ActivitySchedules", ResourceType = typeof(Core.Resources.Models))]
        public List<ActivitySchedule> ActivitySchedules
        {
            get
            {
                return (_ActivitySchedules ?? ActivitySchedule.Instance().GetActivitySchedules());
            }
            set
            {
                _ActivitySchedules = value;
            }
        }

        public int? ActivityScheduleItems_Index { get; set; }
        public IEnumerable<SelectListItem> ActivitySchedulesItems
        {
            get
            {
                IEnumerable<SelectListItem> items = ActivitySchedules.Select(i => new SelectListItem { Value = i.Id.ToString(), Text = i.ScheduleTimeFormat.ToUpper(), Selected = (i.Id.Equals(ActivityScheduleItems_Index)), Disabled = (!i.Status) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }

        /**/

        //[Display(Name = "ParkOperationals", ResourceType = typeof(Core.Resources.Models))]
        public List<ParkOperational> ParkOperationals
        {
            get
            {
                return Core.ParkOperational.Instance().GetParkOperationalList();
            }
        }

        public int? ParkOperationalItems_Index { get; set; }
        public IEnumerable<SelectListItem> ParkOperationalItems
        {
            get
            {
                IEnumerable<SelectListItem> items = ParkOperationals.Select(i => new SelectListItem { Value = i.ParkOperational_Id.ToString(), Text = i.ParkOperational_Code.ToUpper(), Selected = (i.ParkOperational_Id.Equals(ParkOperationalItems_Index)), Disabled = (!i.ParkOperational_Status) });
                return Enumerable.Repeat(new SelectListItem { Value = "", Text = string.Empty }, count: 1).Concat(items);
            }
        }
        #endregion
    }
}