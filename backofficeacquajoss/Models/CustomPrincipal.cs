﻿using System.Security.Principal;
using Core;

namespace BackOfficeAcquaJoss.Models
{
    interface ICustomPrincipal : IPrincipal
    {
        Users UserProfile { get; set; }
    }

    public class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role) { return false; }

        public CustomPrincipal(string email)
        {
            this.Identity = new GenericIdentity(email);
        }
        
        public Users UserProfile { get; set; }
    }
}