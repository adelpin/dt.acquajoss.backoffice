﻿using Core;
using System.Collections.Generic;

namespace BackOfficeAcquaJoss.Models
{
    public class UsersModel
    {
        private static UsersModel instance = null;
        public UsersModel()
        {
            instance = this;
        }

        public static UsersModel Instance()
        {
            if (instance == null) instance = new UsersModel();
            return instance;
        }

        public List<UsersType> UsersTypes = UsersType.Instance().GetUsersTypes();
        public List<Users> Users = Core.Users.Instance().GetUsers();
        public UserAndLogin UserAndLogin = new UserAndLogin();
    }
}