﻿using Core;
using System;
using System.Collections.Generic;

namespace BackOfficeAcquaJoss.Models
{
    public class CheckoutReservation
    {
        public Customers customer { get; set; }
        public Reservations reservation { get; set; }
        public CheckoutReservations reservations { get; set; }
    }

    public class CheckoutReservations
    {
        public List<Admission> admissions { get; set; }
        public List<Dynamic> dynamics { get; set; }
        public List<School> schools { get; set; }
        public List<Extras> extras { get; set; }
        public List<Activities> activities { get; set; }
        public List<ActivitiesSchools> activitiesschools { get; set; }
        public List<Menu> menus { get; set; }
        public List<MenuSchools> menusschools { get; set; }
        public List<Gadget> gadgets { get; set; }
        public List<Hotel> hotels { get; set; }
    }

    public class Admission
    {
        public int idProduct { get; set; }
        public DateTime date { get; set; }
        public int quantity { get; set; }
        public int coupon { get; set; }
        public decimal price { get; set; }
        public string schedule { get; set; }
        public string currency_code { get; set; }
        public decimal discount { get; set; }
        public DiscountCoupon discountCoupon
        {
            get
            {
                return DiscountCoupon.Instance().GetDiscount(coupon);
            }
        }
        public decimal discount_value
        {
            get
            {
                if (discount > 0 && discountCoupon != null)
                {
                    if (discountCoupon.DiscountCouponTypeEnum == DiscountCouponType.Percent)
                    {
                        decimal discount = discountCoupon.DiscountCoupon_Value > 1 ? (discountCoupon.DiscountCoupon_Value / 100) : discountCoupon.DiscountCoupon_Value;
                        return Decimal.Round(subtotal * discount, 2);
                    }
                    return discountCoupon.DiscountCoupon_Value;
                }
                return 0;
            }
        }
        public decimal subtotal {
            get
            {
                return price; //* quantity;
            }
        }
        public decimal total
        {
            get
            {
                return (price * quantity); // - Decimal.Round(discount_value, 2);
            }
        }
    }

    public class School : Admission
    {
        
    }

    public class Dynamic
    {
        public int idProduct { get; set; }
        public DateTime date { get; set; }
        public int quantity { get; set; }
        public string coupon { get; set; }
        public decimal price { get; set; }
        public string schedule { get; set; }
        public string currency_code { get; set; }
        public decimal discount { get; set; }
        public DiscountCoupon discountCoupon
        {
            get
            {
                return DiscountCoupon.Instance().GetDiscount(coupon);
            }
        }
        public decimal subtotal
        {
            get
            {
                return price * quantity;
            }
        }
        public decimal total
        {
            get
            {
                return subtotal - discount;
            }
        }
    }

    public class Extras
    {
        public int idProduct { get; set; }
        public DateTime date { get; set; }
        public int quantity { get; set; }
        public string coupon { get; set; }
        public decimal price { get; set; }
        public string currency_code { get; set; }
        public decimal discount { get; set; }
        public decimal discount_value
        {
            get
            {
                if (discount > 0 && discountCoupon != null)
                {
                    if (discountCoupon.DiscountCouponTypeEnum == DiscountCouponType.Percent)
                    {
                        decimal discount = discountCoupon.DiscountCoupon_Value > 1 ? (discountCoupon.DiscountCoupon_Value / 100) : discountCoupon.DiscountCoupon_Value;
                        return subtotal * discount;
                    }
                    return discountCoupon.DiscountCoupon_Value;
                }
                return 0;
            }
        }
        public DiscountCoupon discountCoupon
        {
            get
            {
                return DiscountCoupon.Instance().GetDiscount(coupon);
            }
        }
        public decimal subtotal
        {
            get
            {
                return price * quantity;
            }
        }
        public decimal total
        {
            get
            {
                return (price * quantity) - discount_value;
            }
        }
    }

    public class Activities
    {
        public int idProduct { get; set; }
        public DateTime date { get; set; }
        public int quantity { get; set; }
        public string coupon { get; set; }
        public decimal price { get; set; }
        public string currency_code { get; set; }
        public string schedule { get; set; }
        public decimal discount { get; set; }
        public decimal discount_value
        {
            get
            {
                if (discount > 0 && discountCoupon != null)
                {
                    if (discountCoupon.DiscountCouponTypeEnum == DiscountCouponType.Percent)
                    {
                        decimal discount = discountCoupon.DiscountCoupon_Value > 1 ? (discountCoupon.DiscountCoupon_Value / 100) : discountCoupon.DiscountCoupon_Value;
                        return subtotal * discount;
                    }
                    return discountCoupon.DiscountCoupon_Value;
                }
                return 0;
            }
        }
        public DiscountCoupon discountCoupon
        {
            get
            {
                return DiscountCoupon.Instance().GetDiscount(coupon);
            }
        }
        public decimal subtotal
        {
            get
            {
                return price * quantity;
            }
        }
        public decimal total
        {
            get
            {
                return (price * quantity) - discount_value;
            }
        }
    }

    public class ActivitiesSchools : Activities
    {
    }

    public class Menu
    {
        public int idProduct { get; set; }
        public DateTime date { get; set; }
        public int quantity { get; set; }
        public string coupon { get; set; }
        public decimal price { get; set; }
        public string currency_code { get; set; }
        public decimal discount { get; set; }
        public decimal discount_value
        {
            get
            {
                if (discount > 0 && discountCoupon != null)
                {
                    if (discountCoupon.DiscountCouponTypeEnum == DiscountCouponType.Percent)
                    {
                        decimal discount = discountCoupon.DiscountCoupon_Value > 1 ? (discountCoupon.DiscountCoupon_Value / 100) : discountCoupon.DiscountCoupon_Value;
                        return subtotal * discount;
                    }
                    return discountCoupon.DiscountCoupon_Value;
                }
                return 0;
            }
        }
        public DiscountCoupon discountCoupon
        {
            get
            {
                return DiscountCoupon.Instance().GetDiscount(coupon);
            }
        }
        public decimal subtotal
        {
            get
            {
                return price * quantity;
            }
        }
        public decimal total
        {
            get
            {
                return (price * quantity) - discount_value;
            }
        }
    }

    public class MenuSchools : Menu
    {
    }

    public class Gadget
    {
        public int idProduct { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public string currency_code { get; set; }
        public string coupon { get; set; }
        public double discount { get; set; }
        public decimal discount_value
        {
            get
            {
                if (discount > 0 && discountCoupon != null)
                {
                    if (discountCoupon.DiscountCouponTypeEnum == DiscountCouponType.Percent)
                    {
                        decimal discount = discountCoupon.DiscountCoupon_Value > 1 ? (discountCoupon.DiscountCoupon_Value / 100) : discountCoupon.DiscountCoupon_Value;
                        return subtotal * discount;
                    }
                    return discountCoupon.DiscountCoupon_Value;
                }
                return 0;
            }
        }
        public DiscountCoupon discountCoupon
        {
            get
            {
                return DiscountCoupon.Instance().GetDiscount(coupon);
            }
        }
        public decimal subtotal
        {
            get
            {
                return price * quantity;
            }
        }
        public decimal total
        {
            get
            {
                return (price * quantity) - discount_value;
            }
        }
    }

    public class Hotel
    {
        public int idProduct { get; set; }
        public int adults { get; set; }
        public int kids { get; set; }
        public int infants { get; set; }
        public int rooms { get; set; }
        public int days { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public string currency_code { get; set; }

        public DateTime arrival { get; set; }
        public DateTime departure { get; set; }
        public int tickets { get; set; }//in the park
        public int nights { get; set; }//in the hotel
        public List<DateTime> datesOpened { get; set; }
        public List<DateTime> datesSelected { get; set; }
        public List<RoomType> roomsTypes { get; set; }
        public string coupon { get; set; }
        public double discount { get; set; }
        public decimal discount_value
        {
            get
            {
                if (discount > 0 && discountCoupon != null)
                {
                    if (discountCoupon.DiscountCouponTypeEnum == DiscountCouponType.Percent)
                    {
                        decimal discount = discountCoupon.DiscountCoupon_Value > 1 ? (discountCoupon.DiscountCoupon_Value / 100) : discountCoupon.DiscountCoupon_Value;
                        return subtotal * discount;
                    }
                    return discountCoupon.DiscountCoupon_Value;
                }
                return 0;
            }
        }
        public DiscountCoupon discountCoupon
        {
            get
            {
                return DiscountCoupon.Instance().GetDiscount(coupon);
            }
        }
        public decimal subtotal
        {
            get
            {
                return price * quantity;
            }
        }
        public decimal total
        {
            get
            {
                return (price * quantity) - discount_value;
            }
        }
    }

    public class RoomType {
        public int adults { get; set; }
        public int kids { get; set; }
        public int infants { get; set; }
        public string roomType
        {
            get
            {
                switch (adults + kids)
                {
                    case 1:
                        return "single";
                    case 2:
                        return "double";
                    case 3:
                        return "triple";
                    case 4:
                        return "quadruple";
                }
                return string.Empty;
            }
        }
    }
}