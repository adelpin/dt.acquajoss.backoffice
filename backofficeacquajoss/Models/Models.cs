﻿using Core;
using Core.Db;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace BackOfficeAcquaJoss.Models
{
    public class ProductsModel : MainModel
    {
        //private static ProductsModel _istance = null;
        public new List<Products> Products = null;
        public Products Product = null;

        //public new static ProductsModel Instance()
        //{
        //    if (_istance == null) _istance = new ProductsModel();
        //    return _istance;
        //}
    }

    public class PromotionsAddonsModel : MainModel
    {
        private static PromotionsAddonsModel _istance = null;
        public List<ParkOperational> ParksOperationals = null;
        public List<ParkCalendar> ParksCalendars = null;
        public List<ProductsCalendar> PromotionsAddons = null;

        //public ParkOperational ParkOperational = null;
        public ProductsCalendar PromotionsAddon = null;

        public new static PromotionsAddonsModel Instance()
        {
            if (_istance == null) _istance = new PromotionsAddonsModel();
            return _istance;
        }
    }

    public class ProductsImageModel : MainModel
    {
        //private static ProductsImageModel _istance = null;
        public List<ProductsImage> ProductsImage = null;
        public ProductsImage ProductImage = null;

        //public new static ProductsImageModel Instance()
        //{
        //    if (_istance == null) _istance = new ProductsImageModel();
        //    return _istance;
        //}
    }


    #region HOTELS
    public class HotelsModel : MainModel
    {
        //private static HotelsModel _istance = null;
        public List<Services> ServicesList
        {
            get
            {
                return Services.Instance().GetServices();
            }
        }
        public new List<Hotels> Hotels = null;
        public Hotels Hotel = null;
        public Products Product = null;

        //public new static HotelsModel Instance()
        //{
        //    if (_istance == null) _istance = new HotelsModel();
        //    return _istance;
        //}
    }

    public class HotelUsersModel : MainModel
    {
        public List<HotelUsers> HotelUsers {
            get
            {
                return Core.HotelUsers.Instance().GetAll();
            }
        }
    }

    public class HotelAllotmentModel : MainModel
    {
        public HotelAllotmentModel(FormCollection data = null)
        {
            if (data != null && data.Count > 0)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                foreach (var key in data.AllKeys)
                {
                    var value = data[key];
                    if (value != null && !string.IsNullOrEmpty(value))
                    {
                        if (key == "HotelAllotment_Date")
                        {
                            value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                        }
                        if (key == "HotelAllotment_CreationDate")
                        {
                            value = DateTime.Parse(value).ToString(CRUD.DATETIMEFORMAT);
                        }
                        parameters.Add(new SqlParameter(key, value));
                    }
                }

                HotelAllotments = Core.HotelAllotments.Instance().GetHotelAllotments(parameters);
            }
        }

        public List<ParkOperational> ParkOperationalList = null;
        public List<ParkCalendar> ParkCalendarList = null;
        public List<ParkSchedules> ParkSchedulesList = null;
        public List<HotelAllotments> HotelAllotments = null;

        public HotelAllotments HotelAllotment { get; set; }
        public Products Product = null;
        public Hotels Hotel = null;
    }

    public class HotelsMoldel
    {
        public List<Core.Hotel> Hotels = null;
        public HotelsMoldel(string lang, int rooms, int adults, int kids)
        {
            Hotels = Core.Hotel.GetHotels(lang, rooms, adults, kids);
        }
    }
    #endregion

    public class SeoModel
    {
        private static SeoModel _instance = null;
        public List<Seo> AllSeo
        {
            get
            {
                return Seo.Instance().GetListSeo();
            }
        }
        public List<Sites> SitesList
        {
            get
            {
                return Sites.Instance().GetSites();
            }
        }

        public static SeoModel Instance()
        {
            if (_instance == null) _instance = new SeoModel();
            return _instance;
        }
    }

    public class SeoEmailsModel : MainModel
    {
        public List<SchoolCategories> SchoolCategoriesList
        {
            get
            {
                return SchoolCategories.Instance().GetSchoolCategories();
            }
        }

        public List<string> NameOfSchools
        {
            get
            {
                return SchoolCategories.Instance().GetSchoolsNames();
            }
        }
    }

    public class ProductsInfoModel : MainModel
    {
        //private static ProductsInfoModel _instance = null;
        public List<ProductsInfo> ProductsInfo = null;
        public List<ProductsImage> ProductsImage = null;
        public List<ProductsAllotment> ProductsAllotments = null;
        public List<ProductsSchedules> ProductsSchedules = null;
        public Products Product = null;
        public new List<Products> Products = null;
        public ProductsInfo ProductInfo = null;
        public ProductsImage ProductImage = null;
        public ProductsAllotment ProductsAllotment = null;
        public ProductsSchedules ProductSchedule = null;
        public Hotels Hotel = null;

        //public new static ProductsInfoModel Instance()
        //{
        //    if (_instance == null) _instance = new ProductsInfoModel();
        //    return _instance;
        //}
    }

    public class AddonsModel : MainModel
    {
        //private static AddonsModel _istance = null;
        public List<Products> Addons = null;
        public Products Addon = null;

        //public new static AddonsModel Instance()
        //{
        //    if (_istance == null) _istance = new AddonsModel();
        //    return _istance;
        //}
    }

    public class SchedulesModel : MainModel
    {
        //private static SchedulesModel _istance = null;
        public List<Products> AllProducts = null;

        //public new static SchedulesModel Instance()
        //{
        //    if (_istance == null) _istance = new SchedulesModel();
        //    return _istance;
        //}
    }

    public class ParkCalendarModel
    {
        public string action = string.Empty;
        public ParkOperational ParkOperational = null;
        public List<ParkOperational> ParkOperationalList = null;
        public List<ParkCalendar> CalendarList = null;
        public List<ParkSchedules> SchedulesList
        {
            get
            {
                return ParkSchedules.Instance().GetParkSchedules();
            }
        }
    }

    public class ParkSeasonsModel
    {
        public ParkSeasons ParkSeason = null;
        public List<ParkSeasons> ParkSeasonsList = null;
        public ParkOperational ParkOperational = null;
        public List<ParkOperational> ParkOperationalList = null;
        public List<ParkCalendar> Calendar = null;
    }
    
    public class SlidersModel : MainModel
    {
        private static SlidersModel _istance = null;
        public List<Sliders> Sliders = null;
        public Sliders Slider = null;

        public new static SlidersModel Instance()
        {
            if (_istance == null) _istance = new SlidersModel();
            return _istance;
        }
    }

    public class PromotionsModel : MainModel
    {
        private static PromotionsModel _istance = null;
        public List<Promotions> Promotions = null;
        public Promotions Promotion = null;

        public new static PromotionsModel Instance()
        {
            if (_istance == null) _istance = new PromotionsModel();
            return _istance;
        }
    }

    public class ParkSchedulesModel// : MainModel
    {
        public ParkSchedules ParkSchedule = null;
        public List<ParkSchedules> ParkSchedules = null;
    }

    public class PricesModel
    {
        public List<ParkOperational> ParkOperationalList
        {
            get
            {
                return ParkOperational.Instance().GetParkOperationalList();
            }
        }
        public List<SalesChannel> SalesChannelList
        {
            get
            {
                return SalesChannel.Instance().GetSalesChannel();
            }
        }
        public List<PriceList> PriceList
        {
            get
            {
                return Core.PriceList.Instance().GetPriceList();
            }
        }
        public List<Products> ProductsList
        {
            get
            {
                return Products.Instance(Global.Lang).GetProducts();
            }
        }

        public PriceList PriceListSelected = null;
        public List<PriceListSalesChannel> PriceListSalesChannel = null;
    }

    public class ProductsAllotmentModel : MainModel
    {
        public List<ParkOperational> ParkOperationalList = null;
        public List<ParkCalendar> ParkCalendarList = null;
        public List<ParkSchedules> ParkSchedulesList = null;
        public List<ProductsAllotment> ProductsAllotments = null;

        public ProductsAllotment ProductsAllotment { get; set; }
        public Products Product = null;
    }

    public class DiscountCouponModel : MainModel
    {
        //private static DiscountCouponModel _istance = null;
        public List<DiscountCoupon> DiscountCoupons = null;
        public DiscountCoupon DiscountCoupon = null;

        //public static DiscountCouponModel Instance()
        //{
        //    if (_istance == null) _istance = new DiscountCouponModel();
        //    return _istance;
        //}
    }

    public class SearcReservationsModel : MainModel
    {
        public List<Reservations> reservations = null;
        public SearcReservationsModel(List<SqlParameter> parameters = null)
        {
            if (parameters != null)
            {
                reservations = Reservations.Instance().SeachReservations(parameters);
            }
        }
    }

    public class PartnersModel
    {
        public List<PartnersTypes> partnersTypes
        {
            get
            {
                return PartnersTypes.Instance().GetPartnersTypes();
            }
        }

        public List<Partners> partners
        {
            get
            {
                return Partners.Instance().GetPartners();
            }
        }
    }

    public class ActivitiesModel : MainModel
    {
        public List<ParkCalendar> CalendarList = ParkCalendar.Instance().GetParkCaledarActives();
        public List<Categories> categoriesList
        {
            get
            {
                return Categories.Instance().GetCategories();
            }
        }

        public List<Core.Activities> activitiesList
        {
            get
            {
                return Core.Activities.Instance().GetActivities();
            }
        }
    }

    public class AquaHomeModel : MainModel
    {
       public AquaHome home = null;
        public AquaHomeModel( int id) {

            //home = Core.AquaHome.Instance().GetHome(id);
            home = Repository.GetAquaHome(id);

        }
    }
    public class ConvenzioniModel : MainModel
    {
        public Convenzion home = null;
        public ConvenzioniModel(int id)
        {
            home = Core.Convenzion.Instance().GetConvenzioni(id);
        }
    }
    public class ActivityScheduleModel : MainModel
    {
        public ActivitySchedule ActivitySchedule = null;
    }

    public class TmasterMapModel
    {
        public int? ParkOperational_Id;

        public TmasterMapModel(int? id)
        {
            this.ParkOperational_Id = id;
        }
        
        public List<ParkOperational> parkOperationalList
        {
            get
            {
                return ParkOperational.Instance().GetParkOperationalList();
            }
        }

        public List<TmasterMap> TmasterMapList
        {
            get
            {
                return TmasterMap.Instance().GetTmasterMapList();
            }
        }
    }

    public class TMasterMapeoModel : MainModel
    {
        public int? ParkOperational_Id;

        public ProductsTickets ProductsTicket { get; set; }

        public TMasterMapeoModel(int? id) {
            this.ParkOperational_Id = id;
        }

        public List<ParkOperational> ParkOperationalList
        {
            get
            {
                return ParkOperational.Instance().GetParkOperationalList();
            }
        }

        public List<TmasterMap> TmasterMaps
        {
            get
            {
                return TmasterMap.Instance().GetTmasterMapList(this.ParkOperational_Id);
            }
        }

        public List<ProductsTickets> ProductsTickets
        {
            get
            {
                return Core.ProductsTickets.Instance().GetProductTickets();
            }
        }
    }

    public class EventsModel : MainModel
    {
        public Events Event { get; set; }
        public List<Events> Events
        {
            get
            {
                return Core.Events.Instance().GetEvents();
            }
        }
        public EventsFaqs EventFaq { get; set; }
        public List<EventsFaqs> EventsFaqs {
            get {
                return Core.EventsFaqs.Instance().GetEventsFaqs(Event.Event_Id);
            }
        }
    }

    public class NewsModel : MainModel
    {
        
        public News New { get; set; }
        public List<News> News {
            get {
                return Core.News.Instance().GetNews();
            }
        }
    }

    public class FaqsModel : MainModel
    {

        private static FaqsModel _istance = null;
        public List<Faqs> faqs = null;
        //public List<Faqs> faqs
        //{
        //    get
        //    {
        //        return Faqs.Instance().GetFaqs();
        //    }
        //}
        public Faqs Faqs = null;

        public new static FaqsModel Instance()
        {
            if (_istance == null) _istance = new FaqsModel();
            return _istance;
        }
    }

    #region
    public class ReportsModel
    {
        public class ReportsSales : MainModel
        {
            public ReportsSales(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {

                            if ((key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }
                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        ReservationItems = Core.ReservationItems.Instance().SearchReportSales(parameters);
                }
            }

            public List<Core.ReservationItemsReport> ReservationItems = null;
        }

        public class ReportsSalesAddons : MainModel
        {
            public ReportsSalesAddons(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {

                            if ((key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }
                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        ReservationItemsAddons = Core.ReservationItemsReportAddons.Instance().SearchReportSalesAddons(parameters);
                }
            }

            public List<Core.ReservationItemsReportAddons> ReservationItemsAddons = null;
        }

        public class ReservationSalesCustomers : MainModel
        {
            public ReservationSalesCustomers(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {
                            if ((key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }

                            if (key == "Product_Id" && value == "0")
                            {
                                value = null;
                            }

                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        ReservationSales = Core.ReservationSalesReportCustomers.Instance().SearchReportSalesCustomers(parameters);
                }
            }

            public List<Core.ReservationSalesReportCustomers> ReservationSales = null;
        }


        public class ReservationSalesDetailsZM : MainModel
        {
            public ReservationSalesDetailsZM(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {
                            if ((key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }

                            if (key == "Product_Id" && value == "0")
                            {
                                value = null;
                            }

                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        ReservationSales = Core.ReservationSalesReportDetailsZM.Instance().SearchReportSalesDetailsZM(parameters);
                }
            }

            public List<Core.ReservationSalesReportDetailsZM> ReservationSales = null;
        }


        public class ReservationSalesPostalCodes : MainModel
        {
            public ReservationSalesPostalCodes(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {
                            if ((key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }

                            if (key == "Product_Id" && value == "0")
                            {
                                value = null;
                            }

                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        ReservationSales = Core.ReservationSalesReportPostalCodes.Instance().SearchReportSalesPostalCodes(parameters);
                }
            }

            public List<Core.ReservationSalesReportPostalCodes> ReservationSales = null;
        }

        public class ReportSalesSubscriptionNames : MainModel
        {
            public ReportSalesSubscriptionNames(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {
                            if ((key == "Reservation_Ini" || key == "Reservation_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }

                            if (key == "Product_Id" && value == "0")
                            {
                                value = null;
                            }

                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        SubscriptionNames = Core.ReservationSalesReportSubscriptionNames.Instance().SearchReportSalesSubscriptionNames(parameters);
                }
            }

            public List<Core.ReservationSalesReportSubscriptionNames> SubscriptionNames = null;
        }

        public class ReservationSalesSellers : MainModel
        {
            public ReservationSalesSellers(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {
                            if ((key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }

                            if (key == "Product_Id" && value == "0")
                            {
                                value = null;
                            }

                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        ReservationSales = Core.ReservationSalesReportSellers.Instance().SearchReportSalesSellers(parameters);
                }
            }

            public List<Core.ReservationSalesReportSellers> ReservationSales = null;
        }

        public class ReportsSalesHotels : MainModel
        {
            public ReportsSalesHotels(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {
                            if ((key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }
                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        ReservationItemsHotels = Core.ReservationItemsReportHotels.Instance().SearchReportSalesHotels(parameters);
                }
            }

            public List<Core.ReservationItemsReportHotels> ReservationItemsHotels = null;
        }

        public class ReportsSalesDealers : MainModel
        {
            public ReportsSalesDealers(FormCollection data = null)
            {
                if (data != null && data.Count > 0)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    foreach (var key in data.AllKeys.Where(k => k != "Only_Admission").ToList())
                    {
                        var value = data[key];
                        if (value != null && !string.IsNullOrEmpty(value))
                        {
                            if ((key == "Reservation_Ini" || key == "Reservation_Fin" || key == "Admission_Ini" || key == "Admission_Fin") && value != null)
                            {
                                value = DateTime.Parse(value).ToString(CRUD.DATEFORMAT);
                            }
                            parameters.Add(new SqlParameter(key, value));
                        }
                    }
                    if (parameters != null)
                        ReservationItemsDealers = Core.ReservationItemsReportDealers.Instance().SearchReportSalesDealers(parameters);
                }
            }

            public List<Core.ReservationItemsReportDealers> ReservationItemsDealers = null;
        }
    }
    #endregion

    #region Model Customer
    public class CustomersModel : MainModel
    {
        public Customers Customer { get; set; }

        public List<Customers> Customers { get; set; } //Core.Customers.GetClients();

        public List<Customers> Schools { get; set; } //Core.Customers.GetSchools();
    }
    #endregion

    #region Model Telemarketing
    public class TelemarketingModel// : MainModel
    {
        public TelemarketingModel(List<SqlParameter> parameters = null)
        {
            if (parameters != null)
            {
                Telemarketings = Telemarketing.Instance().GetTelemarketingBySchool(parameters);
            }
        }

        public Users Agent { get; set; }

        public List<Telemarketing> Telemarketings { get; set; }
        public List<Users> Agents { get; set; }
        public List<Customers> Schools { get; set; }
    }
    #endregion
}