﻿using BackOfficeAcquaJoss.App_Start;
using BackOfficeAcquaJoss.Models;
using BackOfficeAcquaJoss.Models.Binds;
using Core;
using Core.Db;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace BackOfficeAcquaJoss
{
    public class Global : HttpApplication
    {
        public static string Lang;
        public static bool LangChange = false;
        public static string LangSelected {
            get {
                if (HttpContext.Current != null)
                {
                    LangChange = false;
                    string _langSelected = HttpContext.Current.Request["LangSelected"] != null ? HttpContext.Current.Request["LangSelected"].ToString() : string.Empty;
                    if (!string.IsNullOrEmpty(_langSelected))
                    {
                        HttpContext.Current.Session["LangSaved"] = _langSelected;
                        LangChange = true;
                    }
                    if(HttpContext.Current.Session["LangSaved"] != null)
                        return HttpContext.Current.Session["LangSaved"].ToString();
                }
                return Lang;
            }
        } 

        public static Users UserProfile
        {
            get
            {
                if (HttpContext.Current.User != null)
                {
                    CustomPrincipal user = HttpContext.Current.User as CustomPrincipal;
                    return user == null ? new Users() : user.UserProfile;
                }
                return new Users();
            }
        }

        public static bool IsLogged
        {
            get
            {
                return UserProfile != null && HttpContext.Current.Session["logged"] != null && (bool)HttpContext.Current.Session["logged"];
            }
        }

        public static CultureInfo CultureInfo;
        public static RegionInfo RegionInfo;
        public static bool IsMobile
        {
            get
            {
                return HttpContext.Current.Request.Browser.IsMobileDevice;
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            Impostazioni.TmTest = "off";
            Impostazioni.TmServer = ConfigurationManager.AppSettings["TmServer"];
            Impostazioni.TmEndPoint = Impostazioni.TmServer + Impostazioni.TmPercorso;

            Impostazioni.DynamicIds = Repository.GetDynamicIds();

            Impostazioni.BasePrice = Repository.GetBasePrice();
            Impostazioni.BaseReducePrice = Repository.GetBaseReducePrice();
            Impostazioni.BaseHotelPrice = Repository.GetBaseHotelPrice();

            Impostazioni.Steps = Repository.GetStepInfo();

            int Company = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultCompany"]);
            Repository.GetInvoiceInfo(Company);

            ////////////////////////
            //banca sella check bank
            string BsTest = "";
            if (ConfigurationManager.AppSettings["BsTest"] == "on")
            {
                BsTest = "Test";
                Impostazioni.BsTest = BsTest;
            }

            Impostazioni.BsShopLogin = ConfigurationManager.AppSettings["BsShopLogin" + BsTest];
            Impostazioni.BsCheckEndPoint = ConfigurationManager.AppSettings["BsCheckEndPoint" + BsTest];
            Impostazioni.BsCheckDaysBefore = ConfigurationManager.AppSettings["BsCheckDaysBefore"];

        }

        protected void Application_BeginRequest()
        {
            CultureInfo = new CultureInfo(GlobalSettings.UserLang); //Get user lang by browser lang
            Lang = CultureInfo.TwoLetterISOLanguageName; //Define lang
            if (!Lang.Contains("it")) Lang = "en"; //If diferent to it other lang is equals to en
            CultureInfo = new CultureInfo(Lang); //Important redefine the culture info
            Thread.CurrentThread.CurrentCulture = CultureInfo;
            Thread.CurrentThread.CurrentUICulture = CultureInfo;
            RegionInfo = new RegionInfo((Lang.Contains("it") ? "it-IT" : "en-US"));
        }
        
        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                Users userProfile = new JavaScriptSerializer().Deserialize<Users>(authTicket.UserData);
                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.UserProfile = userProfile;
                HttpContext.Current.User = newUser;
            }
        }
    }
}
